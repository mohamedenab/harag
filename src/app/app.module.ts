import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from './modules/shared/shared.module';
import {MainModule} from './modules/main/main.module';
import {AdminModule} from './modules/admin/admin.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {UserModule} from './modules/user/user.module';
import {AuthenticatedService} from './services/auth-gaurd/authenticated.service';
import {HaragGuard} from './services/auth-gaurd/harag.guard';
import {ReactiveFormsModule} from '@angular/forms';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {SuperAdminModule} from './modules/super-admin/super-admin.module';
import {GoogleTagManagerModule} from 'angular-google-tag-manager';
import {SortableModule} from 'ngx-bootstrap/sortable';
import {PaymentComponent} from './modules/payment/payment.component';

@NgModule({
  declarations: [
    AppComponent,
    PaymentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    MainModule,
    AdminModule,
    UserModule,
    SuperAdminModule,
    ReactiveFormsModule,
    GoogleTagManagerModule.forRoot({
      id: 'GTM-56PMRV2',
    })
  ],
  providers: [AuthenticatedService, HaragGuard,
    {provide: LOCALE_ID, useValue: 'en-SA'}, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
