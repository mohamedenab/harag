// ********* LOGIN PART *********** //
import {Brand} from '../Interfaces/Model';

export interface Login {
  phone?: string;
  password?: string;
}

// ********* REGISTER PART *********** //

export interface Register {
  email?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  birthDate?: string;
  country?: string;
  gender?: string;
}


// ********* HOME PRODUCTS PART *********** //

export interface HomeProducts {
  categoryID?: string;
  categoryName?: string;
  products?: Products[];
}

export interface Products {
  _id?: string;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  cover?: string;
}

// *********  PRODUCTS DETAILS PART  *********** //

export interface ProductDetailsParent {
  product?: ProductDetails;
  similarProducts?: SimilarProducts[];
}

export interface SimilarProducts {
  _id?: string;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  createdAt?: string;
  image?: string;
}

export interface ProductDetails {
  _id?: string;
  views?: number;
  images?: Images[];
  title?: string;
  text?: string;
  city?: string;
  price?: number;
  contactNumber?: string;
  warrantyStatus?: string;
  installment?: boolean;
  carPlace?: string;
  driver?: boolean;
  forSell?: boolean;
  runningKilos?: number;
  brand?: object;
  model?: object;
  style?: string;
  manufacturingYear?: number;
  typeOfSpecifications?: string;
  agent?: string;
  movementController?: string;
  speedCount?: string;
  color?: string;
  innerColor?: string;
  cylinderCount?: number;
  fuelType?: string;
  supplier?: Supplier;
  createdAt?: string;
  comments?: Comments[];
  reviews?: Reviews[];
  fullRate?: number;
  isFavorited?: boolean;
  typeOfAd?: string;
  manufacturingYears?: number[];

}

export interface Comments {
  owner?: CommentsOwner;
  body?: string;
  createdAt?: string;
}

export interface Supplier {
  _id?: string;
  profilePicture?: string;
  fullName?: string;
}

export interface Reviews {
  owner?: ReviewsOwner;
  rate?: number;
  comment?: string;
}

export interface CommentsOwner {
  _id?: string;
  profilePicture?: Images;
  fullName?: string;
}

export interface ReviewsOwner {
  profilePicture?: Images;
}

export interface Images {
  path?: string;
}

export interface AddComment {
  body?: string;
}

// *********  SEARCH PART  *********** //

export interface Search {
  _id?: string;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  manufacturingYear?: number;
  cylinderCount?: number;
  supplier?: SearchSupplier;
  createdAt?: string;
  image?: string;
  cover?: string;
  brand?: object;
  engine;

}

export interface SearchBody {
  sort?: string;
  title?: string;
  categories?: string[];
  maxPrice?: number;
  minPrice?: number;
  startYear?: number;
  finishYear?: number;
  model?: string;
  brand?: string;
  style?: string;
  place?: string;
  minRunningKilos?: number;
  maxRunningKilos?: number;
  sortBy?: string;
  sortOrder?: string;
  installment?: boolean;
}

export interface SearchSupplier {
  _id?: string;
  fullName?: string;
}

export interface FilterModel {
  _id?: string;
  name?: string;
  selected?: boolean;
}

export interface City {
  name?: string;
}

export interface SubCategoryList {
  _id?: string;
  name?: string;
  subcategories?: Brand[];
}

export interface BrandID {
  brandID?: string;
}


// ********* ADVERTISING MANAGEMENT PART  *********** //


export interface UserFavorites {
  _id?: string;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  supplier?: Supplier;
  createdAt?: string;
  updatedAt?: string;
  fullRate?: string;
  image?: string;
}

export interface UserAdvertisments {
  _id?: string;
  active?: boolean;
  visible?: boolean;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  createdAt?: string;
  updatedAt?: string;
  image?: string;
  show?: boolean;
}


// ********* STATISTICS PART  *********** //

export interface Statistics {
  activeProducts?: number;
  hiddenProducts?: number;
  allProducts?: number;
  views24h?: number;
  viewsLastWeek?: number;
  viewsLastMonth?: number;
  viewsAllTime?: number;
  viewsSearch?: ViewsSearch[];
  viewsSearchCount?: number;
  mostViewed?: MostViewed[];
}

export interface ViewsSearch {
  _id?: string;
  count?: number;
}

export interface MostViewed {
  _id?: string;
  active?: boolean;
  visible?: boolean;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  supplier?: Supplier;
  image?: string;
}


// ********* NOTIFICATIONS PART  *********** //

export interface Notifications {
  count?: number;
  notifications?: NotificationsData[];
}

export interface NotificationsData {
  _id?: string;
  read?: boolean;
  from?: From;
  message?: string;
  link?: string;
  createdAt?: string;
  updatedAt?: string;
  showAt?: string;
}

export interface From {
  _id?: string;
  fullName?: string;
  profilePicture?: Images;
}


// ********* PROFILE DATA PART  *********** //

export interface Profile {
  user?: User;
  products?: ProfileProducts[];
}

export interface User {
  _id?: string;
  email?: string;
  birthDate?: string;
  phone?: string;
  firstName?: string;
  lastName?: string;
  gender?: string;
  profilePicture?: string;
  fullName?: string;
  city?: string;
  subscribed?: boolean;
  subscription?: Subscription;
  userRate?: number;
  owner?: boolean;
  createdAt: string;
  role?: string;
  activate?: boolean;
}

export interface Subscription {
  _id?: string;
  type?: string;
  price?: number;
  name?: string;
  period?: number;
  limit?: number;
  createdAt?: string;
  updatedAt?: string;
}

export interface ProfileProducts {
  _id?: string;
  active?: boolean;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  createdAt?: string;
  updatedAt?: string;
  visible?: boolean;
  fullRate?: number;
  image?: string;
}


export interface EditProfile {
  email?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  city?: string;
}


// ********* SUPER ADMIN PART *********** //


export interface ProductList {
  count?: number;
  products?: AdminProductList[];
}

export interface AdminProductList {
  _id?: string;
  image?: string;
  title?: string;
  city?: string;
  price?: number;
  runningKilos?: number;
  supplier?: Supplier;
  createdAt?: string;
}

export interface AdminImages {
  path?: string;
}

export interface ListComments {
  _id?: string;
  owner?: Owner;
  product?: ProductTitle;
  body?: string;
  createdAt?: string;
}

export interface Owner {
  _id?: string;
  fullName?: string;
}

export interface ProductTitle {
  title?: string;
}

export interface DeleteComment {
  commentID?: string;
  reason?: string;
}


export interface Statistics {
  usersCount?: number;
  subUsersCount?: number;
  productsCount?: number;
}

// ************ CHATS PART ************** //

export interface ChatList {
  _id?: string;
  lastMessage?: string;
  lastMessageDate?: string;
  userOne?: ChatUser;
  userTwo?: ChatUser;
  lastMessageRead?: boolean;
}

export interface ChatUser {
  _id?: string;
  fullName?: string;
  profilePicture?: string;
}


export interface GetChatMessages {
  _id?: string;
  messages?: Message[];
  userOne?: WhoSendMessage;
  userTwo?: WhoSendMessage;
}

export interface Message {
  _id?: string;
  from?: WhoSendMessage;
  to?: WhoSendMessage;
  text?: string;
  createdAt?: string;
}

export interface WhoSendMessage {
  _id?: string;
  fullName?: string;
  profilePicture?: string;
}

// blog

export interface Article {
  name?: string;
  body?: string;
  type?: string;
  image?: string;
  newTags?: string[];
}

export interface ArticleData {
  _id?: string;
  approved?: string;
  name?: string;
  body?: string;
  type?: {
    _id?: string;
    name?: string;
  };
  writer?: {
    _id?: string;
    fullName?: string
  };
  image?: {
    _id?: string;
    path?: string;
  };
  likesCount?: number;
  tags?: Tags[];
}


// *********** BLOGS PART ************* //

export interface BlogDetails {
  blog: Blog;
  relatedBlogs: RelatedBlogs[];
}


export interface RelatedBlogs {
  _id?: string;
  name?: string;
  body?: string;
  type?: BlogType;
  image?: BlogImage;
  writer?: BlogWriter;
  createdAt?: string;
  likesCount?: number;
}

export interface Blog {
  _id?: string;
  name?: string;
  body?: string;
  type?: BlogType;
  image?: BlogImage;
  writer?: BlogWriter;
  createdAt?: string;
  comments?: BlogComments[];
  likesCount?: number;
  isLiked?: boolean;
}

export interface BlogComments {
  _id?: string;
  body?: string;
  createdAt?: string;
  owner?: BlogOwner;
}

export interface BlogOwner {
  _id?: string;
  fullName?: string;
  profilePicture?: string;
}

export interface BlogIsRemoved {
  value?: boolean;
  reason?: string;
}


export interface BlogWriter {
  _id?: string;
  fullName?: string;
}
export interface BlogImage {
  _id?: string;
  path?: string;
}
export interface BlogType {
  _id?: string;
  name?: string;
}
// ************** HOME OF BLOGS **************** //

export interface HomeOfBlogs {
  homeBlogs?: HomeBlogs[];
  latestBlogs?: LatestBlogs[];
  gallery?: Gallery[];
}

export interface HomeBlogs {
  _id?: string;
  tags?: [];
  name?: string;
  body?: string;
  type?: BlogType;
  image?: BlogImage;
  writer?: BlogWriter;
  createdAt?: string;
  likesCount?: number;
}

export interface LatestBlogs {
  _id?: string;
  tags?: [];
  name?: string;
  body?: string;
  type?: BlogType;
  image?: BlogImage;
  writer?: BlogWriter;
  createdAt?: string;
  likesCount?: number;
}

export interface Gallery {
  _id?: string;
  image?: string;
  body?: string;
  name?: string;
}


// ************** ADD COMMENT ************* //

export interface BlogCommentBody {
  body?: string;
}

// ************* BLOG CONTACT *************** //

export interface BlogContact {
  name?: string;
  email?: string;
  address?: string;
  message?: string;
}


// ************* LIST CONTACTS ************** //

export interface ListContacts {
  _id?: string;
  name?: string;
  email?: string;
  address?: string;
  message?: string;
  createdAt?: string;
}


// ************* LIST CONTACTS ************** //

export interface Tags {
  _id?: string;
  name?: string;
  createdAt?: string;
  selected?: boolean;
}
