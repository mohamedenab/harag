import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AdminStatisticsService {
  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) { }

  getStatistics(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/statistics`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }
}
