import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import Swal from 'sweetalert2';
declare const $: any;
@Injectable({
  providedIn: 'root'
})
export class AdvertisingService {
  // variables
  token = localStorage.getItem('harag-token');
  // array
  MONTH_NAMES = [
    'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيه',
    'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'
  ];
  // sweet alert
  Toast = Swal.mixin({
    toast: true,
    position: 'top-left',
    showConfirmButton: false,
    timer: 2500,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });

  constructor(private http: HttpClient) { }

  // *********** GET MY FAVOURITES LIST *********** //

  getMyFavourites(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/favorites`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *********** GET ADVERTISING MANAGEMENT LIST *********** //
  getAdvMangeList(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/products`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *********** SHOW ADVERTISEMENT *********** //
  showAdvertisment(advId): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/products/${advId}/show`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *********** HIDE ADVERTISEMENT *********** //
  hideAdvertisment(advId): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/products/${advId}/hide`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *********** ACTIVE ADVERTISEMENT *********** //
  activeAdvertising(advId): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/products/${advId}/active`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *********** DELETE ADVERTISEMENT *********** //
  deleteProduct(id): Observable<any> {
    return this.http.delete(`${environment.apiWithUrl}/products/${id}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // ************************** PROFILE PART ************************** //

  getProfile(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/me`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *********** ADMIN TIME MANAGEMENT & LOADERS *********** //
  getFormattedDate(date, prefomattedDate, hideYear = false) {
    const day = date.getDate();
    const month = this.MONTH_NAMES[date.getMonth()];
    const year = date.getFullYear();
    const hours = date.getHours();
    let minutes = date.getMinutes();

    if (minutes < 10) {
      // Adding leading zero to minutes
      minutes = `0${minutes}`;
    }

    if (prefomattedDate) {
      // Today at 10:20
      // Yesterday at 10:20
      return `${prefomattedDate} عند ${hours}:${minutes}`;
    }

    if (hideYear) {
      // 10. January at 10:20
      return `${day} ${month} عند ${hours}:${minutes}`;
    }

    // 10. January 2017. at 10:20
    return `${day} ${month} ${year} عند ${hours}:${minutes}`;
  }

  timeAgo(dateParam) {
    if (!dateParam) {
      return null;
    }

    const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
    const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
    const today = new Date();
    const yesterday = new Date(+today - DAY_IN_MS);
    const seconds = Math.round((+today - date) / 1000);
    const minutes = Math.round(seconds / 60);
    const isToday = today.toDateString() === date.toDateString();
    const isYesterday = yesterday.toDateString() === date.toDateString();
    const isThisYear = today.getFullYear() === date.getFullYear();


    if (seconds < 5) {
      return 'الان';
    } else if (seconds < 60) {
      return `منذ ${seconds} ثوان`;
    } else if (seconds < 90) {
      return 'منذ دقيقة تقريبا';
    } else if (minutes < 60) {
      return `منذ ${minutes} دقيقة`;
    } else if (isToday) {
      return this.getFormattedDate(date, 'اليوم'); // Today at 10:20
    } else if (isYesterday) {
      return this.getFormattedDate(date, 'امس'); // Yesterday at 10:20
    } else if (isThisYear) {
      return this.getFormattedDate(date, false, true); // 10. January at 10:20
    }

    return this.getFormattedDate(date, false, false); // 10. January 2017. at 10:20
  }

  showLoader(): void {
    $('.dash-preloader').fadeIn();
  }

  hideLoader(): void {
    $('.dash-preloader').fadeOut();
  }

  alertSuccess(message: string) {
    this.Toast.fire({
      icon: 'success',
      title: message
    });
  }

  alertDanger(message: string) {
    this.Toast.fire({
      icon: 'error',
      title: message
    });
  }
}
