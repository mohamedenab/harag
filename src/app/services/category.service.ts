import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) {
  }

  getCategoryList(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/categories`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  updateSubCategory(obj, id) {
    return this.http.put(`${environment.apiWithUrl}/admin/categories/subcategory/${id}`, obj, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  addSubCategory(obj) {
    return this.http.post(`${environment.apiWithUrl}/admin/categories/subcategory`, obj, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  getSubCategoryList(parentId): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/categories/sub-by-category`, {
      'parentID': parentId
    }, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }
}
