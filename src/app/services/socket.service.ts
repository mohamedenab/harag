import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';
import * as io from 'socket.io-client';

declare const $: any;

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket: any;
  readonly url: string = 'https://harag-ecommerce.herokuapp.com/';
  token = localStorage.getItem('harag-token');
  chatRoomId: string;
  notificationList = [];

  constructor(private http: HttpClient) {
    this.socket = io(this.url);
    this.listeners();
  }

//  ********* GET CHAT LIST *********** //
  getChatList(userId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/chat/user/${userId}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

//  ********* GET CHAT MESSAGES *********** //
  getChatMessages(roomId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/chat/${roomId}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  checkNotification(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/notifications/check`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  checkChat(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/chat/check`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  listenToListNotification(): Observable<any> {
    return this.listen('listNotifications');
  }

  emitToNotification() {
    this.emit('listNotifications', {
      userToken: this.token
    });
  }

  listenToAllNotifications(): Observable<any> {
    return this.listen('newNotification');
  }

  readNotification(): Observable<any> {
    return this.listen('readNotification');
  }

  emitChats() {
    this.emit('listChats', {
      userToken: this.token
    });
  }

  listenToChatNotifications(): Observable<any> {
    return this.listen('newMessageNotification');
  }

  listenChats(): Observable<any> {
    return this.listen('listChats');
  }

  listenToNewMessage(): Observable<any> {
    return this.listen('newMessage');
  }

  // listenToChatErrors(): void {
  //   this.listen('sendMessage').subscribe(res => {
  //     console.log(res);
  //   });
  // }

  setReadNotification() {
    this.emit('setReadNotification', {
      userToken: this.token
    });
  }

  // ********** emit socket when connect *********** //

  emitSocketWhenConnect(): void {
    this.emit('setSocketId', {
      userToken: this.token
    });
    console.log('emit socket id');
  }

  leaveRoom(chatId: string) {
    this.emit('leaveRoom', {
      chatId
    });
    console.log('room ha been left');
  }

  listeners(): void {
    // this.emitSocketWhenConnect();
    // this.listenToAllNotifications();
  }

  // *************** SOCKET EVENTS ******************* //
  listen(eventName: string): Observable<any> {
    return new Observable((subscriber) => {
      this.socket.on(eventName, (data) => {
        subscriber.next(data);
      });
    });
  }

  emit(eventName: string, data: any): void {
    this.socket.emit(eventName, data);
  }

  trackByFn(index, item) {
    return index; // or item.id
  }

  getDownWhenEnter(): void {
    const scrollDiv = $('.middle-box');
    scrollDiv.animate({scrollTop: scrollDiv.prop('scrollHeight')}, 0);
  }
}
