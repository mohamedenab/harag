import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AdminNewModelStyleService {
  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) { }

  addNewBrand(brand: {name: string}): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/admin/brands`, brand, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  addNewModel(model: {name: string, brandID: string}): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/admin/models`, model, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  addNewStyle(style: {name: string, modelID: string}): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/admin/styles`, style, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }
}
