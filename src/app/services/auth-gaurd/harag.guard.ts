import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthenticatedService} from './authenticated.service';

@Injectable({
  providedIn: 'root'
})
export class HaragGuard implements CanActivate {
  constructor(private auth: AuthenticatedService, private router: Router) {}

  canActivate(): boolean {
    if (this.auth.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }

}
