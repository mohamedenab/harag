import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ProfileService} from '../profile/profile.service';
import {AuthenticatedService} from './authenticated.service';

@Injectable({
  providedIn: 'root'
})
export class AdminModeratorGuard implements CanActivate {
  constructor(private profile: ProfileService, private router: Router, private auth: AuthenticatedService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if ((this.profile?.isModerator || this.profile?.isAdmin) && this.auth?.isLoggedIn()) {
      return true;
    } else {
      this.router.navigateByUrl('/');
      return false;
    }
  }

}
