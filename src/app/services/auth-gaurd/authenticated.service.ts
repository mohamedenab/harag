import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedService {

  constructor(private router: Router) {
    console.log(this.isLoggedIn());
  }

  saveToken(token) {
    localStorage.setItem('harag-token', token);
  }

  isLoggedIn() {
    return !!localStorage.getItem('harag-token');
  }

  logOut(): void {
    localStorage.removeItem('harag-token');
    localStorage.removeItem('h-u-id');
    localStorage.removeItem('h-u-n');
    window.location.href = '/';
  }

}
