import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';
import Swal from "sweetalert2";

@Injectable({
  providedIn: 'root'
})
export class AdminProductsService {
  // variables
  token = localStorage.getItem('harag-token');
  // sweet alert
  Toast = Swal.mixin({
    toast: true,
    position: 'top-left',
    showConfirmButton: false,
    timer: 2500,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });
  constructor(private http: HttpClient) { }

  // ********** GET ADMIN PRODUCT LIST ********** //
  getProductList(page: number, limit: number): Observable<any> {
      return this.http.get(`${environment.apiWithUrl}/admin/products?page=${page}&limit=${limit}`, {
        headers: {
          'x-auth-token': `${this.token}`
        }
      });
  }

  // ********** APPROVE PRODUCT ********** //
  approveProduct(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/products/approve/${id}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // ********** DELETE PRODUCT ********** //
  deleteProduct(id): Observable<any> {
    return this.http.delete(`${environment.apiWithUrl}/admin/products/${id}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // ************************ DELETE PRODUCT ************************* //


  // ********** COMMENTS LIST ********** //
  getCommentsList(page: number, limit: number): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/comments?page=${page}&limit=${limit}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // ********** DELETE COMMENT ********** //
  deleteComment(data: object): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/admin/comments/delete`, data, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // ********** APPROVE COMMENT ********** //
  approveComment(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/comments/approve/${id}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  alertSuccess(message: string) {
    this.Toast.fire({
      icon: 'success',
      title: message
    });
  }

  alertInfo(message: string) {
    this.Toast.fire({
      icon: 'info',
      title: message
    });
  }

  alertDanger(message: string) {
    this.Toast.fire({
      icon: 'error',
      title: message
    });
  }

  search(query: string, page: number, limit: number): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/products?page=${page}&limit=${limit}&title=${query}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  trackByFn(index, item) {
    return index;
  }
}
