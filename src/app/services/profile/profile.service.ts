import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {Profile} from '../../models/model';
import {AuthenticatedService} from '../auth-gaurd/authenticated.service';

declare const $: any;

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  // variables
  isAdmin = false;
  isModerator = false;
  token = localStorage.getItem('harag-token');
  profileContainer: Profile = {};

  constructor(private http: HttpClient, private auth: AuthenticatedService) {
    this.getProfile();
  }

  // ************** GET USER'S PROFILE DATA ************** //

  getUserProfile(token): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/me`, {
      headers: {
        'x-auth-token': `${token}`
      }
    });
  }

  // ************** GET USER'S PROFILE DATA BY ID ************** //

  getProfileById(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/${id}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // ************** UPDATE PROFILE DATA ************** //

  updateProfileData(data): Observable<any> {
    return this.http.put(`${environment.apiWithUrl}/user`, data, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  uploadPicture(images: FormData): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/user/upload-pic?folder=profile-pictures`, images, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  changePic(id) {
    return this.http.put(`${environment.apiWithUrl}/user/change-pic`, id, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  getProfile(passtoken?) {
    if (this.token || passtoken) {
      this.getUserProfile(this.token ? this.token : passtoken).subscribe(res => {
        this.profileContainer = res.data;
        this.isAdmin = res.data.user.role === 'admin';
        this.isModerator = res.data.user.role === 'moderator';
        console.log(this.isModerator, 'mod');
        console.log(res);
      }, err => {
        // if (err.status === 401) {
        //   this.auth.logOut();
        // }
        console.error(err);
      });
    }
  }

  showLoader(): void {
    $('.preloader').fadeIn();
  }

  hideLoader(): void {
    $('.preloader').fadeOut();
  }
}
