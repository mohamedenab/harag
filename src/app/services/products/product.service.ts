import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';

declare const $: any;

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  // variables
  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) {
  }

  // *************** GET HOME PRODUCTS DATA *************** //

  getHomeProcutsData(limit, page, data): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/products/search-by-category?limit=${limit}&page=${page}`, data, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *************** GET PRODUCTS DETAILS *************** //

  getProdcutDetials(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/products/${id}?update=true`, { ///
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  editProduct(id, data): Observable<any> {
    return this.http.put(`${environment.apiWithUrl}/products/${id}`, data, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *************** TOGGLE FAVORITE *************** //

  giveLikeToAdd(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/products/${id}/favorite`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  commentsList(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/products/${id}/comments`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *************** ADD COMMENT *************** //
  addComment(id, data): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/products/${id}/comments`, data, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }


  showNumber(id) {
    return this.http.get(`${environment.apiWithUrl}/products/${id}/show-number`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  showWhatsapp(id) {
    return this.http.get(`${environment.apiWithUrl}/products/${id}/show-whatsapp`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  checkout() {
    return this.http.post(`${environment.apiWithUrl}/payment/checkout`, {
      'amount': 5.00,
      'street': 'new street',
      'state': 'Riyad',
      'city': 'Riyad',
      'country': 'SA',
      'postCode': '15616'
    }, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  showLoader(): void {
    $('.preloader').fadeIn();
  }

  hideLoader(): void {
    $('.preloader').fadeOut();
  }
}
