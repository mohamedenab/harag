import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment.prod';
import {Observable} from 'rxjs';
import {Advertisement, GivenAdvertisement} from '../Interfaces/Model';

@Injectable({
  providedIn: 'root'
})
export class NewAddService {

  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) {
  }

  getListCategories(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/categories`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  getSubCategories(parentId: string): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/categories/sub-by-category`, {parentID: parentId}, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *************** GET BRAND LIST DATA *************** //
  getListBrands(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/brands`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *************** GET MODEL LIST DATA *************** //
  getModelList(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/models`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *************** GET STYLE LIST DATA *************** //
  getStyleList(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/styles`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  uploadPictures(images: FormData): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/products/upload-pic?folder=products`, images, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  postNewAddv(newAdd: Advertisement): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/products`, newAdd, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  editProduct(product: Advertisement, id: string): Observable<any> {
    return this.http.put(`${environment.apiWithUrl}/products/${id}`, product, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }
}
