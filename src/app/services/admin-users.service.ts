import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AdminUsersService {
  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) {
  }

  getListUsers(page: number, limit: number): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/users?page=${page}&limit=${limit}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  getAdminList(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  addUser(data): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/admin/users`, data, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  getModeratorList(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/moderators`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  deleteUser(userId: string): Observable<any> {
    return this.http.delete(`${environment.apiWithUrl}/admin/users/${userId}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  suspendUser(userId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/users/suspend/${userId}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  unsuspendUser(userId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/users/unsuspend/${userId}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  makeAdmin(userId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/${userId}/make-admin`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  makeModerator(userId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/${userId}/make-moderator`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  removeAdmin(userId): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/${userId}/remove-admin`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  removeModerator(userId): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/${userId}/remove-moderator`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  search(query: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/users?name=${query}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }
}
