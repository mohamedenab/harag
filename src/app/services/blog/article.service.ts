import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {Article} from '../../models/model';
import {SubCategory, SubCategoryResponse} from '../../Interfaces/Model';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) {
  }

  addNewArticle(article: Article): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/blogs`, article, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  uploadImage(image: FormData): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/blogs/upload-pic?folder=blogs`, image, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  getListSubCategory(): Observable<SubCategory[]> {
    return this.http.get(`${environment.apiWithUrl}/categories/subcategory`, {
      headers: {
        'x-auth-token': this.token
      }
    }).pipe(
      map((it: SubCategoryResponse) => it.data)
    );
  }

  updateArticle(id: string, article: Article): Observable<any> {
    return this.http.put(`${environment.apiWithUrl}/blogs/${id}`, article, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  getArticle(articleId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs/${articleId}`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  getArticlesByType(typeId: string): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs?type=${typeId}`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  getListSubCategoryFromCategory(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/categories/subcategories-for-category`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }


  // ******************* GET BLOG BY ID *********************** //

  getBlogById(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs/${id}`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  // ******************* GET HOME BLOGS *********************** //

  getHomeblogs(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs/home`);
  }

  // ******************* ADD BLOG COMMENT *********************** //

  addBlogComment(data, id): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/blogs/${id}/comments`, data, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  // ******************* TOGGLE BLOG LIKE *********************** //

  toggleBlogLike(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs/${id}/toggle-like`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  // ******************* CONTACT BLOG *********************** //

  contactFromBlog(data: object): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/blogs/contact`, data, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  // ******************* LIST OF BLOGS *********************** //

  blogList(page, limit): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/blogs?page=${page}&limit=${limit}`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }


  // ******************* APPROVE BLOGS *********************** //
  approveMyBlogs(blogId): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/admin/blogs/approve/${blogId}`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  // ******************* DIS - APPROVE BLOGS *********************** //

  rejectThisBlog(blogId): Observable<any> {
    return this.http.delete(`${environment.apiWithUrl}/admin/blogs/${blogId}`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  // ******************* GET LIST CONTACTS *********************** //

  getListContact(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs/contacts`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }

  // ******************* GET TAGS *********************** //

  getTags(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs/tags`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }
}
