import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class MyArticlesService {
  token = localStorage.getItem('harag-token');

  constructor(private http: HttpClient) { }

  getMyArticles(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/blogs/my`, {
      headers: {
        'x-auth-token': this.token
      }
    });
  }
}
