import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environment.prod';
declare const $: any;
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  // variables
  token = localStorage.getItem('harag-token');
  // array
  MONTH_NAMES = [
    'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيه',
    'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'
  ];
  constructor(private http: HttpClient) { }

  // ************* GET STATISTICS DATA *************** //
  getStatisticsData(start, end): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/statistics?start=${start}&end=${end}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }


  // ************* GET NOTIFICATIONS DATA *************** //
  getNotificationsData(): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/notifications`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }


  // ************* READ NOTIFICATION *************** //
  readNotification(id): Observable<any> {
    return this.http.get(`${environment.apiWithUrl}/user/notifications/seen/${id}`, {
      headers: {
        'x-auth-token': `${this.token}`
      }
    });
  }

  // *********** ADMIN TIME MANAGEMENT & LOADERS *********** //
  getFormattedDate(date, prefomattedDate, hideYear = false) {
    const day = date.getDate();
    const month = this.MONTH_NAMES[date.getMonth()];
    const year = date.getFullYear();
    const hours = date.getHours();
    let minutes = date.getMinutes();

    if (minutes < 10) {
      // Adding leading zero to minutes
      minutes = `0${minutes}`;
    }

    if (prefomattedDate) {
      // Today at 10:20
      // Yesterday at 10:20
      return `${prefomattedDate} عند ${hours}:${minutes}`;
    }

    if (hideYear) {
      // 10. January at 10:20
      return `${day} ${month} عند ${hours}:${minutes}`;
    }

    // 10. January 2017. at 10:20
    return `${day} ${month} ${year} عند ${hours}:${minutes}`;
  }

  timeAgo(dateParam) {
    if (!dateParam) {
      return null;
    }

    const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
    const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
    const today = new Date();
    const yesterday = new Date(+today - DAY_IN_MS);
    const seconds = Math.round((+today - date) / 1000);
    const minutes = Math.round(seconds / 60);
    const isToday = today.toDateString() === date.toDateString();
    const isYesterday = yesterday.toDateString() === date.toDateString();
    const isThisYear = today.getFullYear() === date.getFullYear();


    if (seconds < 5) {
      return 'الان';
    } else if (seconds < 60) {
      return `منذ ${seconds} ثوان`;
    } else if (seconds < 90) {
      return 'منذ دقيقة تقريبا';
    } else if (minutes < 60) {
      return `منذ ${minutes} دقيقة`;
    } else if (isToday) {
      return this.getFormattedDate(date, 'اليوم'); // Today at 10:20
    } else if (isYesterday) {
      return this.getFormattedDate(date, 'امس'); // Yesterday at 10:20
    } else if (isThisYear) {
      return this.getFormattedDate(date, false, true); // 10. January at 10:20
    }

    return this.getFormattedDate(date, false, false); // 10. January 2017. at 10:20
  }

  showLoader(): void {
    $('.dash-preloader').fadeIn();
  }

  hideLoader(): void {
    $('.dash-preloader').fadeOut();
  }
}
