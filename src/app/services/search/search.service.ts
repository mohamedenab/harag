import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';
import {Search} from '../../models/model';

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    // variables
    currentPage = 1;
    searchContainer: Search[] = [];
    lastPage = 1;
    token = localStorage.getItem('harag-token');
    advCount = 0;
    advanced = false;
    sortOrder = 'desc';
    filterdata = {};

    constructor(private http: HttpClient) {
    }

    // *************** GET SEARCH DATA *************** //


    getSearchData(page, limit, body): Observable<any> {
        return this.http.post(`${environment.apiWithUrl}/products/search?page=${page}&limit=${limit}`, body, {
            headers: {
                'x-auth-token': `${this.token}`
            }
        });
    }

    searchByCategory(page: number, limit: number, category: { categories: string[] }): Observable<any> {
        return this.http.post(`${environment.apiWithUrl}/products/search-by-category?page=${page}&limit=${limit}`, category, {
            headers: {
                'x-auth-token': `${this.token}`
            }
        });
    }

    // *************** GET BRAND LIST DATA *************** //
    getListBrands(): Observable<any> {
        return this.http.get(`${environment.apiWithUrl}/brands`, {
            headers: {
                'x-auth-token': `${this.token}`
            }
        });
    }

    // *************** GET BRAND LIST DATA *************** //
    getListModels(): Observable<any> {
        return this.http.get(`${environment.apiWithUrl}/models`, {
            headers: {
                'x-auth-token': `${this.token}`
            }
        });
    }

    // *************** GET CATEGORY LIST DATA *************** //
    getCategoryBrands(): Observable<any> {
        return this.http.get(`${environment.apiWithUrl}/categories`, {
            headers: {
                'x-auth-token': `${this.token}`
            }
        });
    }

    // *************** GET LIST MODELS BY BRANDS DATA *************** //

    getModelsByBrands(data): Observable<any> {
        return this.http.post(`${environment.apiWithUrl}/models/model-by-brand`, data, {
            headers: {
                'x-auth-token': `${this.token}`
            }
        });
    }

    // *************** LIST STYLES BY MODELS DATA *************** //

    getStylesByModels(data): Observable<any> {
        return this.http.post(`${environment.apiWithUrl}/styles/style-by-model`, data);
    }

    getListSubCategory(): Observable<any> {
        return this.http.get(`${environment.apiWithUrl}/categories/subcategories-for-category`);
    }

    getRecentAds(page): Observable<any> {
        return this.http.get(`${environment.apiWithUrl}/products/recent?limit=10&page=${page}`);
    }

    getCities(): Observable<any> {
        return this.http.get(`${environment.apiWithUrl}/products/cities`);
    }

    calcPages() {
        if (this.advCount === 0) {
            this.lastPage = 1;
            return;
        }
        this.lastPage = Math.ceil(this.advCount / 10);
    }
}
