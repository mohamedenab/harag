import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class SignUpInService {
  proxy = 'https://cors-anywhere.herokuapp.com/';

  constructor(private http: HttpClient) {
  }

  resetpassword(phoneNum) {
    return this.http.post(`${environment.apiWithUrl}/user/reset`, {
      phone: phoneNum
    });

  }

  sendCode() {
    const token = localStorage.getItem('harag-token');

    return this.http.get(`${environment.apiWithUrl}/user/send/code`, {
      headers: {
        'x-auth-token': `${token}`
      }
    });
  }

  confirmresetpassword(userPassword,
                       token) {
    return this.http.post(`${environment.apiWithUrl}/user/reset-password`, {
      password: userPassword,
      resetToken: token
    });

  }

  // *********** LOGIN PART ************ //

  loginUser(data): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/user/login`, data);
  }

  // *********** REGISTER PART ************ //

  registerUser(data): Observable<any> {
    return this.http.post(`${environment.apiWithUrl}/user/register`, data);
  }

  activate(token) {
    return this.http.get(`${environment.apiWithUrl}/user/activate?token=${token}`);

  }
}
