import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoutingModule} from './user-routing.module';
import {UserComponent} from './user.component';
import {ProfileComponent} from './routes/profile/profile.component';
import {SharedModule} from '../shared/shared.module';
import {FreeAdvertismentManagementComponent} from '../admin/routes/free-advertisment-management/free-advertisment-management.component';
import {FollowingListComponent} from '../admin/routes/following-list/following-list.component';
import {PaidAdvertismentManagementComponent} from '../admin/routes/paid-advertisment-management/paid-advertisment-management.component';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    UserComponent,
    ProfileComponent,
    FreeAdvertismentManagementComponent,
    FollowingListComponent,
    PaidAdvertismentManagementComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    FormsModule,
    CommonModule
  ]
})
export class UserModule {
}
