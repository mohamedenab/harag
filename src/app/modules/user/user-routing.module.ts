import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserComponent} from './user.component';
import {ProfileComponent} from './routes/profile/profile.component';
import {HaragGuard} from '../../services/auth-gaurd/harag.guard';

const routes: Routes = [
    {
        path: 'user',
        component: UserComponent,
        // canActivate: [HaragGuard],
        children: [
            {
                path: ':id',
                component: ProfileComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {
}
