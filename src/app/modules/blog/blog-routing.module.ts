import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {BlogComponent} from './blog.component';
import {BlogHomeComponent} from './routes/blog-home/blog-home.component';
import {BlogArticleApprovalComponent} from './routes/blog-article-approval/blog-article-approval.component';
import {BlogArticleDetailsComponent} from './routes/blog-article-details/blog-article-details.component';
import {BlogContactComponent} from './routes/blog-contact/blog-contact.component';
import {BlogContactMessagesComponent} from './routes/blog-contact-messages/blog-contact-messages.component';
import {BlogNewArticleComponent} from './routes/blog-new-article/blog-new-article.component';
import {BlogEditArticleComponent} from './routes/blog-edit-article/blog-edit-article.component';
import {BlogCommentsApprovalComponent} from './routes/blog-comments-approval/blog-comments-approval.component';
import {MyArticlesComponent} from './routes/my-articles/my-articles.component';
import {BlogsByTypeComponent} from './routes/blogs-by-type/blogs-by-type.component';
import {HaragGuard} from '../../services/auth-gaurd/harag.guard';

const routes: Routes = [
  {
    path: '',
    component: BlogComponent,
    children: [
      {
        path: '',
        component: BlogHomeComponent
      },
      {
        path: 'comments-approval',
        component: BlogArticleApprovalComponent
      },
      {
        path: 'article-approval',
        component: BlogCommentsApprovalComponent
      },
      {
        path: 'articles/:id',
        component: BlogArticleDetailsComponent
      },
      {
        path: 'contact',
        component: BlogContactComponent
      },
      {
        path: 'received-messages',
        component: BlogContactMessagesComponent
      },
      {
        path: 'new-article',
        component: BlogNewArticleComponent
      },
      {
        path: 'edit-article',
        component: BlogEditArticleComponent
      },
      {
        path: 'my-articles',
        component: MyArticlesComponent
      },
      {
        path: 'categories/:id',
        component: BlogsByTypeComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule {
}
