import {Component, Input, OnInit} from '@angular/core';
import {RelatedBlogs} from '../../../../models/model';

@Component({
  selector: 'app-latest-articles',
  templateUrl: './latest-articles.component.html',
  styleUrls: ['./latest-articles.component.scss']
})
export class LatestArticlesComponent implements OnInit {
  @Input() relatedContent: RelatedBlogs[];
  constructor() { }

  ngOnInit(): void {
  }

}
