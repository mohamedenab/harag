import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../../../../services/blog/article.service';
import {SubCategoryList} from '../../../../models/model';
import {ProfileService} from '../../../../services/profile/profile.service';
import {AuthenticatedService} from '../../../../services/auth-gaurd/authenticated.service';

@Component({
  selector: 'app-blog-navbar',
  templateUrl: './blog-navbar.component.html',
  styleUrls: ['./blog-navbar.component.scss']
})
export class BlogNavbarComponent implements OnInit {

  categoryList: SubCategoryList[] = [];

  constructor(private article: ArticleService, public profileService: ProfileService , public authService: AuthenticatedService) { }

  ngOnInit(): void {
    this.getListSubCategoryFromCategory();
  }

  getListSubCategoryFromCategory(): void {
    this.article.getListSubCategoryFromCategory().subscribe(res => {
      this.categoryList = res.data;
      console.log(res.data, 'navbar data');
    }, err => {
      console.log(err);
    });
  }

}
