import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { BlogNavbarComponent } from './components/blog-navbar/blog-navbar.component';
import { BlogHomeComponent } from './routes/blog-home/blog-home.component';
import { BlogFooterComponent } from './components/blog-footer/blog-footer.component';
import { BlogNewArticleComponent } from './routes/blog-new-article/blog-new-article.component';
import { BlogArticleDetailsComponent } from './routes/blog-article-details/blog-article-details.component';
import { BlogContactComponent } from './routes/blog-contact/blog-contact.component';
import { BlogArticleApprovalComponent } from './routes/blog-article-approval/blog-article-approval.component';
import { BlogCommentsApprovalComponent } from './routes/blog-comments-approval/blog-comments-approval.component';
import { BlogContactMessagesComponent } from './routes/blog-contact-messages/blog-contact-messages.component';
import { BlogEditArticleComponent } from './routes/blog-edit-article/blog-edit-article.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LatestArticlesComponent } from './components/latest-articles/latest-articles.component';
import {MyArticlesComponent} from './routes/my-articles/my-articles.component';
import {BlogsByTypeComponent} from './routes/blogs-by-type/blogs-by-type.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [
    BlogComponent,
    BlogNavbarComponent,
    BlogHomeComponent,
    BlogFooterComponent,
    BlogNewArticleComponent,
    BlogArticleDetailsComponent,
    BlogContactComponent,
    BlogArticleApprovalComponent,
    BlogCommentsApprovalComponent,
    BlogContactMessagesComponent,
    BlogEditArticleComponent,
    LatestArticlesComponent,
    MyArticlesComponent,
    BlogsByTypeComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class BlogModule {
}
