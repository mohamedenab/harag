import {Component, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {ArticleService} from '../../../../services/blog/article.service';
import {AdvertisingService} from '../../../../services/Advertising/advertising.service';
import {Blog, ListContacts} from '../../../../models/model';

declare const $: any;
@Component({
  selector: 'app-blog-contact-messages',
  templateUrl: './blog-contact-messages.component.html',
  styleUrls: ['./blog-contact-messages.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.8)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class BlogContactMessagesComponent implements OnInit {
  // variables
  currentPage = 1;
  sendRequest = false;
  lastPage = 1;
  productsCount = 0;
  isLoading = false;
  // containers

  contactsContainer: ListContacts[] = [];

  constructor(private article: ArticleService, private adv: AdvertisingService) {
  }


  ngOnInit(): void {
    this.getContacts();
  }


  getContacts(): void {
    this.article.getListContact().subscribe(res => {
      this.contactsContainer = res.data;
      console.log(this.contactsContainer, 'AAAAAAAAAAAAAAAAAAA');
    });
  }

  previous() {
    if (this.currentPage > 1) {
      this.currentPage--;
      // this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.lastPage) {
      this.currentPage++;
      // this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  first() {
    if (this.currentPage > 1) {
      this.currentPage = 1;
      // this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.lastPage) {
      this.currentPage = this.lastPage;
      // this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  calcPages() {
    if (this.productsCount === 0) {
      this.lastPage = 1;
      return;
    }
    this.lastPage = Math.ceil(this.productsCount / 5);
  }
}
