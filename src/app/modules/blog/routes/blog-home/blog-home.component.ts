import {Component, OnDestroy, OnInit} from '@angular/core';
import {ArticleService} from '../../../../services/blog/article.service';
import {Blog, BlogDetails, Gallery, HomeOfBlogs} from '../../../../models/model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-blog-home',
  templateUrl: './blog-home.component.html',
  styleUrls: ['./blog-home.component.scss']
})
export class BlogHomeComponent implements OnInit, OnDestroy {

  popularArr = [
    {img: 'https://images.pexels.com/photos/2882234/pexels-photo-2882234.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/3156482/pexels-photo-3156482.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/3221165/pexels-photo-3221165.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/457418/pexels-photo-457418.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
  ];

  weeklyArr = [
    {img: 'https://images.pexels.com/photos/1571783/pexels-photo-1571783.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/1149831/pexels-photo-1149831.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/2127037/pexels-photo-2127037.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/544542/pexels-photo-544542.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/2922140/pexels-photo-2922140.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
    {img: 'https://images.pexels.com/photos/3536271/pexels-photo-3536271.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'},
  ];

  // variables
  blogSub: Subscription;
  // containers
  blogsContainer: HomeOfBlogs;
  twoContainer?: Gallery[];
  threeContainer?: Gallery[];
  constructor(private article: ArticleService) {
  }

  ngOnInit(): void {
    this.getBlogs();
  }

  ngOnDestroy(): void {
    this.blogSub.unsubscribe();
  }

  getBlogs(): void {
    this.blogSub = this.article.getHomeblogs().subscribe(res => {
      // this.blogContainer = res.data;
      // this.twoContainer = res.data.gallery.splice(0, 2);
      // this.threeContainer = res.data.gallery.splice(2, 3);
      this.blogsContainer = res.data;
      console.log(res, 'home');
    });
  }
}
