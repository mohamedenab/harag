import {Component, OnInit} from '@angular/core';
import {Article, ArticleData} from '../../../../models/model';
import {ArticleService} from '../../../../services/blog/article.service';
import {ActivatedRoute} from '@angular/router';
import {animate, group, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-blogs-by-type',
  templateUrl: './blogs-by-type.component.html',
  styleUrls: ['./blogs-by-type.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.96)'
        }), group([
          animate(100, style({
            opacity: 1
          })), animate(100, style({
            transform: 'scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class BlogsByTypeComponent implements OnInit {
  loading = false;
  articles: ArticleData[] = [];
  typeId = '';
  typeName = '';

  constructor(private articleService: ArticleService, private activatedRoute: ActivatedRoute) {
    activatedRoute.params.subscribe(it => {
      this.typeId = it.id;
      this.getArticlesByType();
    });
  }

  ngOnInit(): void {
  }

  getArticlesByType(): void {
    this.loading = true;
    this.articleService.getArticlesByType(this.typeId).subscribe(res => {
      this.articles = res.data.blogs;
      this.typeName = res.data.type.name;
      this.loading = false;
      console.log(res, 'ay7aga');
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

}
