import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import validate = WebAssembly.validate;
import {BlogContact, HomeOfBlogs} from '../../../../models/model';
import {ArticleService} from '../../../../services/blog/article.service';
import {AdvertisingService} from "../../../../services/Advertising/advertising.service";

@Component({
  selector: 'app-blog-contact',
  templateUrl: './blog-contact.component.html',
  styleUrls: ['./blog-contact.component.scss']
})
export class BlogContactComponent implements OnInit {
  // variables
  sendRequest = false;
  // containers
  blogsContainer: HomeOfBlogs;
  // Interfaces
  contactInterface: BlogContact = {
    address: '',
    email: '',
    message: '',
    name: ''
  };

  // Reactive Forms
  contactForm = new FormGroup({
    userName: new FormControl('', [Validators.required]),
    userMail: new FormControl('', [Validators.required, Validators.email]),
    userAddress: new FormControl('', [Validators.required, Validators.minLength(2)]),
    userMessage: new FormControl('', [Validators.required, Validators.minLength(2)])
  });
  constructor(private article: ArticleService, private adv: AdvertisingService) { }

  ngOnInit(): void {
    this.getLatestBlogs();
  }

  getLatestBlogs() {
    this.article.getHomeblogs().subscribe(res => {
      this.blogsContainer = res.data;
    });
  }

  submitContactForm() {
    this.contactInterface.email = this.contactForm.controls.userMail.value;
    this.contactInterface.message = this.contactForm.controls.userMessage.value;
    this.contactInterface.address = this.contactForm.controls.userAddress.value;
    this.contactInterface.name = this.contactForm.controls.userName.value;
    console.log(this.contactInterface);
    this.sendRequest = true;
    this.article.contactFromBlog(this.contactInterface).subscribe(res => {
      console.log(res);
      this.adv.alertSuccess('تم ارسال رسالتك بنجاح');
      this.contactForm.reset();
      this.sendRequest = false;
    }, err => {
      this.adv.alertDanger('هناك خطأ ما');
      this.sendRequest = false;
    });
  }

}
