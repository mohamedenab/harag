import {Component, OnInit} from '@angular/core';
import {Article, SubCategoryList, Tags} from '../../../../models/model';
import {ArticleService} from '../../../../services/blog/article.service';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {SubCategory, UploadedImage} from '../../../../Interfaces/Model';
import {forkJoin, Observable, of} from 'rxjs';
import {NgForm} from '@angular/forms';
import {Tag} from '@angular/compiler/src/i18n/serializers/xml_helper';

@Component({
  selector: 'app-blog-new-article',
  templateUrl: './blog-new-article.component.html',
  styleUrls: ['./blog-new-article.component.scss']
})
export class BlogNewArticleComponent implements OnInit {
  loading = false;
  submitting = false;
  newArticle: Article = {
    body: '',
    image: '',
    name: '',
    type: '',
    newTags: []
  };
  subCategoryList: Observable<SubCategory[]>;
  uploadedImage: UploadedImage = {
    _id: '',
    name: '',
    path: ''
  };
  tagInput;
  tagsContainer: Tags[] = [];

  constructor(private articleService: ArticleService, private router: Router) {
  }

  ngOnInit(): void {
    this.getBlogTags();
    this.getSubCategoryList();
  }


  getBlogTags(): void {
    this.articleService.getTags().subscribe(res => {
      this.tagsContainer = res.data;
    });
  }

  getSubCategoryList(): void {
    this.subCategoryList = this.articleService.getListSubCategory();
  }

  addTag() {
    this.tagsContainer.push({name: this.tagInput});
    this.tagInput = '';
  }

  selectTag(index): void {
    if (this.tagsContainer[index].selected) {
      this.tagsContainer[index].selected = false;
      const tagIndex = this.newArticle.newTags.indexOf(this.tagsContainer[index].name);
      this.newArticle.newTags.splice(tagIndex, 1);
    } else {
      this.tagsContainer[index].selected = true;
      this.newArticle.newTags.push(this.tagsContainer[index].name);
    }
  }

  addNewArticle(form: NgForm): void {
    this.submitting = true;
    this.articleService.addNewArticle(this.newArticle).subscribe(it => {
      this.submitting = false;
      form.resetForm();
      this.resetImage();
      Swal.fire({
        title: ' تم إضافة المقالة بنجاح ',
        text: 'هل تريد إضافة مقالة اخري؟',
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا شكرا'
      }).then((result) => {
        if (result.value) {
        } else {
          this.router.navigate(['/blog']);
        }
      });
    }, err => {
      console.log(err);
      this.submitting = false;
      Swal.fire({
        title: err.message,
        text: 'هل تريد المحاولة مره اخري؟',
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا شكرا'
      }).then((result) => {
        if (result.value) {
        } else {
          this.router.navigate(['/blog']);
        }
      });
    });
  }

  uploadImage(evt): void {
    this.loading = true;
    if (evt.target.files && evt.target.files.length > 0) {
      const img = evt.target.files[0];
      const image = new FormData();
      image.append('image', img, img.name);
      this.articleService.uploadImage(image).subscribe((res) => {
        this.newArticle.image = res.data._id;
        this.uploadedImage = res.data;
        this.loading = false;
      }, err => {
        console.log(err);
        this.loading = false;
      });
    }
  }

  deleteImage(): void {
    this.resetImage();
    this.newArticle.image = '';
  }

  resetImage(): void {
    this.uploadedImage = {
      path: '',
      name: '',
      _id: ''
    };
  }
}
