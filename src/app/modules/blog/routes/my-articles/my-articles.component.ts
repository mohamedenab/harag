import { Component, OnInit } from '@angular/core';
import {MyArticlesService} from '../../../../services/blog/my-articles.service';
import {ActivatedRoute} from '@angular/router';
import {ArticleData} from '../../../../models/model';
import {animate, group, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-my-articles',
  templateUrl: './my-articles.component.html',
  styleUrls: ['./my-articles.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.96)'
        }), group([
          animate(100, style({
            opacity: 1
          })), animate(100, style({
            transform: 'scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class MyArticlesComponent implements OnInit {

  articles: ArticleData[] = [];
  loading = false;

  constructor(private myArticlesService: MyArticlesService) {
  }

  ngOnInit(): void {
    this.getMyArticles();
  }

  getMyArticles(): void {
    this.loading = true;
    this.myArticlesService.getMyArticles().subscribe(res => {
      this.articles = res.data;
      this.loading = false;
      console.log(this.articles);
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

}
