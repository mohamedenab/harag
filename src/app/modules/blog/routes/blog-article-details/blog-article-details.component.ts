import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../../../services/blog/article.service';
import { BlogCommentBody, BlogDetails } from '../../../../models/model';
import { ActivatedRoute } from '@angular/router';
import { AdvertisingService } from '../../../../services/Advertising/advertising.service';
import { AdminService } from '../../../../services/admin/admin.service';

@Component({
  selector: 'app-blog-article-details',
  templateUrl: './blog-article-details.component.html',
  styleUrls: ['./blog-article-details.component.scss']
})
export class BlogArticleDetailsComponent implements OnInit {

  newArr: any = [
    { img: 'https://images.pexels.com/photos/457418/pexels-photo-457418.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/2356071/pexels-photo-2356071.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/3342697/pexels-photo-3342697.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/3354648/pexels-photo-3354648.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/2920064/pexels-photo-2920064.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/132657/pexels-photo-132657.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/120049/pexels-photo-120049.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/244206/pexels-photo-244206.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/3156482/pexels-photo-3156482.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/831475/pexels-photo-831475.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
  ]

  popularArr = [
    { img: 'https://images.pexels.com/photos/2882234/pexels-photo-2882234.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/3221165/pexels-photo-3221165.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
    { img: 'https://images.pexels.com/photos/457418/pexels-photo-457418.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' },
  ]

  // variables
  blogId: string;
  loadMe = false;
  isLikedVar: boolean;
  // interfaces
  blogDetailsContainer: BlogDetails;
  blogCommentBody: BlogCommentBody = {
    body: ''
  };

  constructor(private article: ArticleService, private Activated: ActivatedRoute, private adv: AdvertisingService, public admin: AdminService) {
    this.getBlogId();
  }

  ngOnInit(): void {
  }


  getBlogDetail(): void {
    this.article.getBlogById(this.blogId).subscribe(res => {
      this.blogDetailsContainer = res.data;
      this.isLikedVar = res.data.blog.isLiked;
      console.log(this.blogDetailsContainer, this.isLikedVar);
    });
  }

  getBlogId() {
    this.Activated.params.subscribe(res => {
      this.blogId = res.id;
      this.getBlogDetail();
    });
  }

  addBlogComments(form): void {
    this.loadMe = true;
    this.article.addBlogComment(this.blogCommentBody, this.blogId).subscribe(res => {
      console.log(res);
      this.loadMe = false;
      form.reset();
      this.adv.alertSuccess('تم اضافه تعلقيك بنجاح, انتظر قبول ادمن الصفحه علي هذا التعليق');
    }, err => {
      console.log(err);
      this.adv.alertDanger('هناك خطأ ما');
      this.loadMe = false;
    });
  }


  toggleLike(): void {
    if (this.blogDetailsContainer.blog.isLiked) {
      this.toggleApi();
      this.blogDetailsContainer.blog.isLiked = false;
      this.blogDetailsContainer.blog.likesCount--;
    } else {
      this.toggleApi();
      this.blogDetailsContainer.blog.isLiked = true;
      this.blogDetailsContainer.blog.likesCount++;
    }
  }

  toggleApi(): void {
    this.article.toggleBlogLike(this.blogId).subscribe(res => {
      console.log(res);
    });
  }
}
