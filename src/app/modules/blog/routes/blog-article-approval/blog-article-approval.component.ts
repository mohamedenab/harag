import { Component, OnInit } from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
declare const $: any;
@Component({
  selector: 'app-blog-article-approval',
  templateUrl: './blog-article-approval.component.html',
  styleUrls: ['./blog-article-approval.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.8)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class BlogArticleApprovalComponent implements OnInit {

  // variables
  currentPage = 1;
  sendRequest = false;
  lastPage = 1;
  productsCount = 0;
  isLoading = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  previous() {
    if (this.currentPage > 1) {
      this.currentPage--;
      // this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.lastPage) {
      this.currentPage++;
      // this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');

    }
  }

  first() {
    if (this.currentPage > 1) {
      this.currentPage = 1;
      // this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.lastPage) {
      this.currentPage = this.lastPage;
      // this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  calcPages() {
    if (this.productsCount === 0) {
      this.lastPage = 1;
      return;
    }
    this.lastPage = Math.ceil(this.productsCount / 5);
  }

}
