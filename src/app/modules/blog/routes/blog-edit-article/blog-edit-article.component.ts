import { Component, OnInit } from '@angular/core';
import {Article, ArticleData, Tags} from '../../../../models/model';
import {Observable} from 'rxjs';
import {SubCategory, UploadedImage} from '../../../../Interfaces/Model';
import {ArticleService} from '../../../../services/blog/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-blog-edit-article',
  templateUrl: './blog-edit-article.component.html',
  styleUrls: ['./blog-edit-article.component.scss']
})
export class BlogEditArticleComponent implements OnInit {

  loading = false;
  submitting = false;
  dataLoading = false;
  newArticle: Article = {
    body: '',
    image: '',
    name: '',
    type: '',
    newTags: []
  };
  editArticle: ArticleData = {
    _id: '',
    name: '',
    body: '',
    image: {
      _id: '',
      path: ''
    },
    type: {
      name: '',
      _id: ''
    },
    tags: []
  };

  subCategoryList: Observable<SubCategory[]>;
  uploadedImage: UploadedImage = {
    _id: '',
    name: '',
    path: ''
  };
  articleId = '';
  tagsContainer: Tags[] = [];


  constructor(private articleService: ArticleService, private router: Router, private activatedRoute: ActivatedRoute) {
    activatedRoute.queryParams.subscribe(res => {
      this.articleId = res.id;
    });
  }

  ngOnInit(): void {
    this.getArticle();
    this.getSubCategoryList();
    this.getBlogTags();
  }


  getSubCategoryList(): void {
    this.subCategoryList = this.articleService.getListSubCategory();
  }

  updateArticle(form: NgForm): void {
    this.submitting = true;
    this.articleService.updateArticle(this.articleId, this.newArticle).subscribe(it => {
      // form.resetForm();
      this.submitting = false;
      // this.resetImage();
      Swal.fire({
        title: ' تم تعديل المقالة بنجاح ',
        text: 'هل تريد التعديل مره اخري؟',
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا شكرا'
      }).then((result) => {
        if (result.value) {
        } else {
          this.router.navigate(['/blog']);
        }
      });
    }, err => {
      console.log(err);
      this.submitting = false;
      Swal.fire({
        title: err.message,
        text: 'هل تريد المحاولة مره اخري؟',
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا شكرا'
      }).then((result) => {
        if (result.value) {
        } else {
          this.router.navigate(['/blog']);
        }
      });
    });
  }

  uploadImage(evt): void {
    this.loading = true;
    if (evt.target.files && evt.target.files.length > 0) {
      const img = evt.target.files[0];
      const image = new FormData();
      image.append('image', img, img.name);
      this.articleService.uploadImage(image).subscribe((res: { data: UploadedImage }) => {
        this.newArticle.image = res.data._id;
        this.uploadedImage = res.data;
        this.loading = false;
      }, err => {
        console.log(err);
        this.loading = false;
      });
    }
  }

  getBlogTags(): void {
    this.articleService.getTags().subscribe(res => {
      this.tagsContainer = res.data;
      console.log(this.tagsContainer, 'taaaaaaaaags');
      this.checkSelectedTags();
    });
  }

  getArticle(): void {
    this.dataLoading = true;
    this.articleService.getArticle(this.articleId).subscribe(res => {
      this.editArticle = res.data.blog;
      this.newArticle = {
        type: this.editArticle.type._id,
        name: this.editArticle.name,
        body: this.editArticle.body,
        image: this.editArticle.image._id,
        newTags: this.editArticle.tags.map(tag => tag.name)
      };
      console.log(this.newArticle.newTags, 'tags');
      this.uploadedImage = this.editArticle.image;
      this.dataLoading = false;
      console.log(res);
    }, err => {
      console.log(err);
      this.dataLoading = false;
    });
  }

  deleteImage(): void {
    this.resetImage();
    this.newArticle.image = '';
  }

  resetImage(): void {
    this.uploadedImage = {
      path: '',
      name: '',
      _id: ''
    };
  }


  selectTag(index): void {
    if (this.tagsContainer[index].selected) {
      this.tagsContainer[index].selected = false;
      const tagIndex = this.newArticle.newTags.indexOf(this.tagsContainer[index].name);
      this.newArticle.newTags.splice(tagIndex, 1);
    } else {
      this.tagsContainer[index].selected = true;
      this.newArticle.newTags.push(this.tagsContainer[index].name);
    }
  }

  checkSelectedTags(): void {
    if (this.newArticle.newTags.length > 0) {
      this.tagsContainer.forEach(tag => {
        if (this.editArticle.tags.includes(tag)) {
          tag.selected = true;
        }
      });
    }
  }
}
