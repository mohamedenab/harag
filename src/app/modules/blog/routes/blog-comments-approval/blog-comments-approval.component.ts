import {Component, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {ArticleService} from '../../../../services/blog/article.service';
import {Blog} from '../../../../models/model';
import {AdvertisingService} from "../../../../services/Advertising/advertising.service";
declare const $: any;

@Component({
  selector: 'app-blog-comments-approval',
  templateUrl: './blog-comments-approval.component.html',
  styleUrls: ['./blog-comments-approval.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.8)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class BlogCommentsApprovalComponent implements OnInit {
  // variables
  currentPage = 1;
  sendRequest = false;
  lastPage = 1;
  productsCount = 0;
  isLoading = false;
  // containers
  blogListContainer: Blog[] = [];
  constructor(private article: ArticleService, private adv: AdvertisingService) {
  }

  ngOnInit(): void {
    this.getListOfBlogs();
  }


  getListOfBlogs(): void {
    this.article.blogList(this.currentPage, '5').subscribe(res => {
      this.blogListContainer = res.data;
      console.log(this.blogListContainer);
    });
  }

  approveMyBlog(id): void {
    this.sendRequest = true;
    this.article.approveMyBlogs(id).subscribe(res => {
      this.sendRequest = false;
      console.log(res);
      this.optimisticRemove(id);
      this.adv.alertSuccess('تم قبول هذا المقال بنجاح');
    }, err => {
      this.sendRequest = false;
      this.adv.alertDanger('هناك خطأ ما');
    });
  }

  disApproveBlog(id): void {
    this.sendRequest = true;
    this.article.rejectThisBlog(id).subscribe(res => {
      this.optimisticRemove(id);
      console.log(res);
      this.sendRequest = false;
      this.adv.alertSuccess('تم ازاله هذا التعليق بنجاح');
    }, err => {
      this.sendRequest = false;
      this.adv.alertDanger('هناك خطأ ما');
    });
  }

  optimisticRemove(id): void {
    this.blogListContainer.forEach((blog, index) => {
      if (id === blog._id) {
        this.blogListContainer.splice(index, 1);
      }
    });
  }

  previous() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.lastPage) {
      this.currentPage++;
      this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  first() {
    if (this.currentPage > 1) {
      this.currentPage = 1;
      this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.lastPage) {
      this.currentPage = this.lastPage;
      this.getListOfBlogs();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  calcPages() {
    if (this.productsCount === 0) {
      this.lastPage = 1;
      return;
    }
    this.lastPage = Math.ceil(this.productsCount / 5);
  }

}
