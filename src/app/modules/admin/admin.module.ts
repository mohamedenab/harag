import { NgModule } from '@angular/core';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import {SharedModule} from '../shared/shared.module';
import { NotificationComponent } from './routes/notification/notification.component';
import {CommonModule} from '@angular/common';
import { FreeSettingsComponent } from './routes/free-settings/free-settings.component';
import { PremiumSettingsComponent } from './routes/premium-settings/premium-settings.component';
import { StatisticsComponent } from './routes/statistics/statistics.component';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewAdvertisementComponent } from './routes/new-advertisement/new-advertisement.component';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { EditAdvertisementComponent } from './routes/edit-advertisement/edit-advertisement.component';
import { MyChatsComponent } from './routes/my-chats/my-chats.component';
import { PrivateRoomComponent } from './routes/private-room/private-room.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatRadioModule } from '@angular/material/radio';
import { NewModelComponent } from './routes/new-model/new-model.component';
import { AdvertisementApprovalComponent } from './routes/advertisement-approval/advertisement-approval.component';
import { UsersComponent } from './routes/users/users.component';
import { CommentsApprovalComponent } from './routes/comments-approval/comments-approval.component';
import { EditSubcategoryComponent } from './routes/edit-subcategory/edit-subcategory.component';
import { ProfileComponent } from './routes/profile/profile.component';

@NgModule({
  declarations: [
    AdminComponent,
    NotificationComponent,
    FreeSettingsComponent,
    PremiumSettingsComponent,
    StatisticsComponent,
    NewAdvertisementComponent,
    EditAdvertisementComponent,
    MyChatsComponent,
    PrivateRoomComponent,
    NewModelComponent,
    AdvertisementApprovalComponent,
    UsersComponent,
    CommentsApprovalComponent,
    EditSubcategoryComponent,
    ProfileComponent,
  ],
  imports: [
    AdminRoutingModule,
    SharedModule,
    CommonModule,
    ChartsModule,
    FormsModule,
    NgxUsefulSwiperModule,
    CommonModule,
    DragDropModule,
    MatRadioModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
