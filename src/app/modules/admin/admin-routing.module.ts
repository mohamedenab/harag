import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AdminComponent} from './admin.component';
import {NotificationComponent} from './routes/notification/notification.component';
import {FreeSettingsComponent} from './routes/free-settings/free-settings.component';
import {PremiumSettingsComponent} from './routes/premium-settings/premium-settings.component';
import {StatisticsComponent} from './routes/statistics/statistics.component';
import {HaragGuard} from '../../services/auth-gaurd/harag.guard';
import {FollowingListComponent} from './routes/following-list/following-list.component';
import {PaidAdvertismentManagementComponent} from './routes/paid-advertisment-management/paid-advertisment-management.component';
import {FreeAdvertismentManagementComponent} from './routes/free-advertisment-management/free-advertisment-management.component';
import {NewAdvertisementComponent} from './routes/new-advertisement/new-advertisement.component';
import {EditAdvertisementComponent} from './routes/edit-advertisement/edit-advertisement.component';
import {MyChatsComponent} from './routes/my-chats/my-chats.component';
import {PrivateRoomComponent} from './routes/private-room/private-room.component';
import {NewModelComponent} from './routes/new-model/new-model.component';
import {AdvertisementApprovalComponent} from './routes/advertisement-approval/advertisement-approval.component';
import {UsersComponent} from './routes/users/users.component';
import {CommentsApprovalComponent} from './routes/comments-approval/comments-approval.component';
import {EditSubcategoryComponent} from './routes/edit-subcategory/edit-subcategory.component';
import { ProfileComponent } from './routes/profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [HaragGuard],
    children: [
      {
        path: 'notifications',
        component: NotificationComponent
      },
      {
        path: 'freeSettings',
        component: FreeSettingsComponent
      },
      {
        path: 'settings',
        component: PremiumSettingsComponent
      },
      {
        path: 'statistics',
        component: StatisticsComponent
      },
      {
        path: 'following-list',
        component: FollowingListComponent
      },
      {
        path: 'advertisement-management',
        component: PaidAdvertismentManagementComponent
      },
      {
        path: 'freeAdvManagement',
        component: FreeAdvertismentManagementComponent
      },
      {
        path: 'new-advertisement',
        component: NewAdvertisementComponent
      },
      {
        path: 'edit-advertisement',
        component: EditAdvertisementComponent
      },
      {
        path: 'my-chats',
        component: MyChatsComponent
      },
      {
        path: 'my-chats/:id',
        component: PrivateRoomComponent
      },
      {
        path: 'new-model-style',
        component: NewModelComponent
      },
      {
        path: 'advertisements',
        component: AdvertisementApprovalComponent
      },
      {
        path: 'user/:id',
        component: ProfileComponent
      },
      {
        path: 'comments',
        component: CommentsApprovalComponent
      },
      {
        path: 'edit-subCategory',
        component: EditSubcategoryComponent
      },

      {
        path: 'users',
        component: UsersComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
