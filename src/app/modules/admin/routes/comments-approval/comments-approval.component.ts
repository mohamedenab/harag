import { Component, OnInit } from '@angular/core';
import { animate, group, style, transition, trigger } from '@angular/animations';
import { DeleteComment, ListComments } from '../../../../models/model';
import { AdminProductsService } from '../../../../services/admin-products.service';
import Swal from 'sweetalert2';
import { SocketService } from '../../../../services/socket.service';

declare const $: any;

@Component({
  selector: 'app-comments-approval',
  templateUrl: './comments-approval.component.html',
  styleUrls: ['./comments-approval.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.8)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class CommentsApprovalComponent implements OnInit {

  // variables
  sendRequest = false;
  currentPage = 1;
  lastPage = 1;
  commentsCount = 0;
  isLoading = false;

  // array
  commentsContainer: ListComments[] = [];

  // interfaces
  deleteCommentInterface: DeleteComment = {
    commentID: '',
    reason: ''
  };

  constructor(public admin: AdminProductsService, private socket: SocketService) {
  }

  ngOnInit(): void {
    this.getCommentsList();
  }

  getCommentsList() {
    this.isLoading = true;
    this.admin.getCommentsList(this.currentPage, 10).subscribe(res => {
      this.commentsContainer = res.data.comments;
      this.commentsCount = res.data.count;
      this.calcPages();
      this.isLoading = false;
      console.log(res);
    }, err => {
      this.isLoading = false;
    });
  }

  approveComment(id) {
    this.isLoading = true;
    this.sendRequest = true;
    this.admin.approveComment(id).subscribe(res => {
      this.getCommentsList();
      this.sendRequest = false;
      this.isLoading = false;
      this.admin.alertSuccess('تم قبول هذا التعليق');
      this.sendNotification(id);
    }, err => {
      this.isLoading = false;
      this.sendRequest = false;
      this.admin.alertDanger('لم يتم قبول هذا التعليق');
    });
  }

  deleteComment() {
    Swal.fire({
      title: 'هل انت متاكد ؟',
      text: 'لن يكون هذا التعليق متاح!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم متأكد',
      cancelButtonText: 'لست متأكد'
    }).then((result) => {
      if (result.value) {
        this.sendRequest = true;
        this.admin.deleteComment(this.deleteCommentInterface).subscribe(res => {
          this.getCommentsList();
          this.sendRequest = false;
          $('#rejectComments').modal('hide');
          this.deleteCommentInterface = {};
          Swal.fire(
            'تم حذف هذا التعليق',
            'هذا التعليق لم يعد متاح',
            'success'
          );
        });
      }
    });
  }

  passCommentId(id) {
    this.deleteCommentInterface.commentID = id;
  }

  previous() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getCommentsList();
      $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.lastPage) {
      this.currentPage++;
      this.getCommentsList();
      $('html, body').animate({ scrollTop: 0 }, 'fast');

    }
  }

  first() {
    if (this.currentPage > 1) {
      this.currentPage = 1;
      this.getCommentsList();
      $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.lastPage) {
      this.currentPage = this.lastPage;
      this.getCommentsList();
      $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
  }

  calcPages() {
    if (this.commentsCount === 0) {
      this.lastPage = 1;
      return;
    }
    this.lastPage = Math.ceil(this.commentsCount / 10);
  }

  sendNotification(id) {
    this.socket.emit('newNotification', {
      id
    });
  }

}
