import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {ChatList} from '../../../../models/model';
import {SocketService} from '../../../../services/socket.service';
import {ProfileService} from '../../../../services/profile/profile.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-my-chats',
  templateUrl: './my-chats.component.html',
  styleUrls: ['./my-chats.component.scss'],
  animations: [
    trigger('scaleIn', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-50%)'
        }), group([
          animate(250, style({
            opacity: 1
          })), animate(250, style({
            transform: 'translateX(0)'
          }))
        ])
      ])
    ]),
    trigger('scaleUp', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateY(-50%)'
        }), group([
          animate(250, style({
            opacity: 1
          })), animate(250, style({
            transform: 'translateY(0)'
          }))
        ])
      ])
    ])
  ]
})
export class MyChatsComponent implements OnInit, OnDestroy {
  // variables
  chatSub: Subscription;
  // interfaces
  chatListContainer: ChatList[] = [];
  token = localStorage.getItem('harag-token');
  loading: boolean;
  constructor(public socket: SocketService, public profile: ProfileService) {}

  ngOnInit(): void {
    // this.getChatList();
    this.socket.emitChats();
    this.getAllChats();
  }

  ngOnDestroy(): void {
    // this.chatSub.unsubscribe();
  }

  openNewChat(id): void {
    this.socket.emit('openChat', {
      userToken: this.token,
      userTwo: id
    });
  }

  getChatList(): void {
    this.loading = true;
    this.chatSub = this.socket.getChatList(this.profile?.profileContainer?.user?._id).subscribe(res => {
      this.chatListContainer = res.data;
      this.chatListContainer.filter(x => x.lastMessage);
      console.log(this.chatListContainer);
      this.loading = false;
    }, err => {
      this.getChatList();
    });
  }

  getAllChats() {
    this.loading = true;
    this.socket.listenChats().subscribe(res => {
      this.chatListContainer = res;
      this.loading = false;
      console.log(res, 'chats');
    }, err => {
      console.log(err);
    });
  }
}
