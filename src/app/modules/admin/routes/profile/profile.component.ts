import {Component, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {ProfileService} from '../../../../services/profile/profile.service';
import {AdvertisingService} from '../../../../services/Advertising/advertising.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Profile} from '../../../../models/model';
import {SocketService} from '../../../../services/socket.service';
import {AuthenticatedService} from '../../../../services/auth-gaurd/authenticated.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-100px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, -28vh)'
          }))
        ])
      ])
    ])
  ]
})
export class ProfileComponent implements OnInit {

  view = 'grid';
  // variables
  userId;
  openRoom = false;
  token = localStorage.getItem('harag-token');
  // arraies
  userProfileContainer: Profile = {};

  constructor(public profile: ProfileService, public adv: AdvertisingService,
    private activated: ActivatedRoute, private router: Router, public socket: SocketService) {
    this.activated.params.subscribe(res => {
      this.userId = res.id;
      this.getUserProfileById();
    });
    this.listenToOpeningChat();
  }

  ngOnInit(): void {

  }

  openNewChat(): void {
    this.openRoom = true;
    this.socket.emit('openChat', {
      userToken: this.token,
      userTwo: this.userProfileContainer?.user?._id,
    });
  }

  listenToOpeningChat(): void {
    this.socket.listen('openChat').subscribe(res => {
      this.router.navigateByUrl(`/admin/my-chats/${res}`);
    }, err => this.openRoom = false);
  }

  getUserProfileById() {
    this.profile.showLoader();
    this.profile.getProfileById(this.userId).subscribe(res => {
      this.userProfileContainer = res.data;
      console.log(this.userProfileContainer);
      this.profile.hideLoader();
    });
  }
}

