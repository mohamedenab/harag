import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChartDataSets, ChartType} from 'chart.js';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {AdminService} from "../../../../services/admin/admin.service";
import {Statistics} from "../../../../models/model";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-60px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, 0)'
          }))
        ])
      ])
    ])
  ]
})
export class StatisticsComponent implements OnInit, OnDestroy {
  // variables
  showForm = false;
  statSub: Subscription;

  dateSelection =  {
    startDate: '',
    endDate: '',
  };

  public chartoptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public chartlables = [];
  public chartlegend = true;
  public charttype: ChartType = 'line';
  public data = [
    {
      label: 'عدد الزيارات',
      data: [],
      backgroundColor: 'rgba(52, 152, 219, 0.4)',
    }
  ];

  statisticsContainer: Statistics = {};
  constructor(private admin: AdminService) { }

  ngOnInit(): void {
    this.getStatisticsData();
  }

  ngOnDestroy(): void {
    this.statSub.unsubscribe();
  }

  getStatisticsData() {
    this.admin.showLoader();
    this.statSub = this.admin.getStatisticsData(this.dateSelection.startDate, this.dateSelection.endDate).subscribe(res => {
      this.statisticsContainer = res.data;
      this.chartlables = this.statisticsContainer.viewsSearch.reverse().map(data => data._id);
      this.data[0].data = this.statisticsContainer.viewsSearch.reverse().map(data => data.count);
      this.admin.hideLoader();
    });
  }

  searchByDate() {
    console.log(this.dateSelection);
    this.getStatisticsData();
  }

  toggleForms() {
    this.showForm = !this.showForm;
  }
}
