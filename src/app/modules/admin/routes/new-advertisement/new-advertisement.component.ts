import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {NewAddService} from '../../../../services/new-add.service';
import {Advertisement, Brand, Inputs, SubCategory} from '../../../../Interfaces/Model';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {forkJoin, Subscription} from 'rxjs';
import {SearchService} from '../../../../services/search/search.service';
import {SwiperOptions} from 'swiper';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {flatMap} from 'rxjs/operators';
import {City} from '../../../../models/model';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {AdminProductsService} from '../../../../services/admin-products.service';

declare const $: any;

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-new-advertisement',
  templateUrl: './new-advertisement.component.html',
  styleUrls: ['./new-advertisement.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('* => void', [
        style({
          opacity: 0,
          transform: 'translateX(50px)'
        }), group([
          animate(100, style({
            opacity: 1
          })), animate(100, style({
            transform: 'translateX(0)'
          }))
        ])
      ])
    ])
  ]
})
export class NewAdvertisementComponent implements OnInit, OnDestroy, AfterViewInit {

  /* todo
    - next step will be to loop through subCategoryInputs in third screen and get the selection option.
  */

  // subscription
  subCategorySub: Subscription;
  brandSub: Subscription;
  modelSub: Subscription;
  styleSub: Subscription;

  // screens variables
  firstScreen = false;
  secondScreen = true;
  thirdScreen = false;
  fourthScreen = false;
  fifthScreen = false;
  sixScreen = false;
  sevenScreen = false;
  eightScreen = false;

  // data variables
  newAddData: Advertisement = {
    cover: '',
    category: '',
    agent: '',
    brand: '',
    carPlace: '',
    city: '',
    color: '',
    carBody: '',
    contactNumber: '',
    cylinderCount: null,
    driver: null,
    forSell: null,
    fuelType: '',
    images: [],
    innerColor: '',
    installment: null,
    manufacturingYear: '',
    model: '',
    movementController: '',
    price: null,
    runningKilos: null,
    speedCount: '',
    style: '',
    text: '',
    title: '',
    typeOfAd: 'سيارات',
    typeOfSpecifications: '',
    warrantyStatus: '',
    brands: [],
    models: [],
    styles: [],
    manufacturingYears: []
  };
  categoryList: Brand[] = [];
  category: Brand = {
    name: '',
    _id: ''
  };
  subCategoryList: SubCategory[] = [];
  subCategoryInputs: Inputs = {
    options: [],
    field: '',
    name: ''
  };
  brandList: Brand[] = [];
  brandValue: Brand = {
    name: '',
    _id: ''
  };
  modelList: Brand[] = [];
  modelValue: Brand = {
    name: '',
    _id: ''
  };
  styleList: Brand[] = [];
  styleValue: Brand = {
    _id: '',
    name: ''
  };
  imageList: {
    path: '',
    name: '',
    id: ''
  }[] = [];
  cities: City[] = [];
  isUploading = false;
  subCatName: string;
  brandName: string;
  modelName: string;
  styleName: string;
  postingAdd = false;
  // spare parts
  spareBrandList: Brand[] = [];
  spareModelList: Brand[] = [];
  spareStyleList: Brand[] = [];
  spareYears: string[] = [];
  spareYearValue = '';

  config: SwiperOptions = {
    pagination: {el: '.swiper-pagination', clickable: true},
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 30,
    lazy: {
      loadPrevNext: true,
    },
  };
  bodyShpae = [{img: 'sedan', title: 'سيدان'}, {img: 'hatchback', title: 'هاتشباك'}, {img: 'foorbyfoor', title: 'دفع رباعي'}, {
    img: 'crossover',
    title: 'كروس اوفر'
  }, {
    img: 'sport',
    title: 'رياضية'
  }, {img: 'CONVERTIBLE', title: 'مكشوفه'}, {img: 'classic', title: 'كلاسيك'}, {img: 'pikup', title: 'بيك اب'}];

  years(startYear = 1980) {
    const currentYear = new Date().getFullYear();
    const years = [];
    startYear = startYear || 1980;
    while (startYear <= currentYear) {
      years.unshift(startYear++);
    }
    return years;
  }

  constructor(private newAdd: NewAddService,
              private search: SearchService,
              private cd: ChangeDetectorRef,
              private router: Router,
              private admin: AdminProductsService,
              private location: Location) {
  }

  ngOnInit(): void {
    // this.getSubCategories();
    // this.testing();
    this.getListCategories();
    this.getBrandList();
    this.getCities();

  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    // this.subCategorySub.unsubscribe();
  }

  testing() {
    this.category.name = 'قطع غيار';
    this.closeAllScreens();
    this.openFifthScreen();
  }

  // ==== get methods ==== //

  getListCategories() {
    this.playLoader();
    this.newAdd.getListCategories().subscribe(res => {
      console.log(res);
      this.categoryList = res.data;
      this.category = this.categoryList[0];
      this.getSubCategories();
      this.stopLoader();
    }, err => {
      this.stopLoader();
      console.log(err);
    });
  }

  getCities() {
    this.search.getCities().subscribe(res => {
      this.cities = res.data;
    }, err => {
      console.log(err);
    });
  }

  getSubCategories() {
    this.playLoader();
    this.subCategorySub = this.newAdd.getSubCategories(this.category._id).subscribe(res => {
      this.subCategoryList = res.data;
      console.log(this.subCategoryList, 'sub category list');
      this.stopLoader();
    }, err => {
      this.stopLoader();
      console.log(err);
    });
  }

  getBrandList() {
    if (this.brandList.length === 0) {
      this.brandSub = this.newAdd.getListBrands().subscribe(res => {
        this.brandList = res.data;
      }, err => {
        console.log(err);
      });
    }
  }

  getBrandAndModelAndStyleList() {
    if (this.spareYears.length > 0) {

    } else {
      this.playLoader();
      forkJoin([this.newAdd.getListBrands(), this.newAdd.getModelList(), this.newAdd.getStyleList()]).subscribe(res => {
        this.brandList = res[0].data;
        this.modelList = res[1].data;
        this.styleList = res[2].data;
        this.spareYears = this.years();
        this.stopLoader();
      }, err => {
        this.stopLoader();
      });
    }
  }

  getModelsByBrand(evt) {
    this.brandName = evt.name;
    this.newAddData.brand = evt._id;
    this.playLoader();
    this.modelSub = this.search.getModelsByBrands({brandID: evt._id}).subscribe(res => {
      this.modelList = res.data;
      this.stopLoader();
    }, err => {
      this.stopLoader();
      console.log(err);
    });

  }

  getStylesByModel(evt) {
    this.newAddData.model = evt._id;
    this.modelName = evt.name;
    this.playLoader();
    this.styleSub = this.search.getStylesByModels({modelID: evt._id}).subscribe(res => {
      this.stopLoader();
      console.log(res);
      this.styleList = res.data;
    }, err => {
      this.stopLoader();
      console.log(err);
    });
  }

  addToBrands() {
    const index = this.brandList.indexOf(this.brandValue);
    if (!this.spareBrandList.includes(this.brandValue)) {
      this.spareBrandList.push(this.brandValue);
      this.brandList.splice(index, 1);
    }
    this.brandValue = {
      name: '',
      _id: ''
    };
  }

  addToModels() {
    const index = this.modelList.indexOf(this.modelValue);
    if (!this.spareModelList.includes(this.modelValue)) {
      this.spareModelList.push(this.modelValue);
      this.modelList.splice(index, 1);
    }
    this.modelValue = {
      name: '',
      _id: ''
    };
  }

  addToStyles() {
    const index = this.styleList.indexOf(this.styleValue);
    if (!this.spareStyleList.includes(this.styleValue)) {
      this.spareStyleList.push(this.styleValue);
      this.styleList.splice(index, 1);
    }
    this.styleValue = {
      name: '',
      _id: ''
    };
  }

  addToYears() {
    const index = this.spareYears.indexOf(this.spareYearValue);
    if (!this.newAddData.manufacturingYears.includes(this.spareYearValue)) {
      this.newAddData.manufacturingYears.push(this.spareYearValue);
      this.spareYears.splice(index, 1);
    }
    this.spareYearValue = '';
  }

  removeFromYears(index) {
    this.spareYearValue = '';
    this.spareYears.push(this.newAddData.manufacturingYears[index]);
    this.spareYears.sort().reverse();
    this.newAddData.manufacturingYears.splice(index, 1);
  }

  removeFromStyles(index) {
    this.styleValue = {
      name: '',
      _id: ''
    };
    this.styleList.push(this.spareStyleList[index]);
    this.spareStyleList.splice(index, 1);
  }

  removeFromModel(index) {
    this.modelValue = {
      name: '',
      _id: ''
    };
    this.modelList.push(this.spareModelList[index]);
    this.spareModelList.splice(index, 1);
  }

  removeFromBrand(index) {
    this.brandValue = {
      name: '',
      _id: ''
    };
    this.brandList.push(this.spareBrandList[index]);
    this.spareBrandList.splice(index, 1);
  }

  styleChange(evt) {
    this.newAddData.style = evt._id;
    this.styleName = evt.name;
  }

  categoryChange(evt) {
    this.newAddData.typeOfAd = evt.name;
  }

  uploadImage(evt) {
    this.isUploading = true;
    if (evt.target.files.length && evt.target.files) {
      const data = new FormData();
      for (const img of evt.target.files) {
        data.append('image', img, img.name);
      }
      this.newAdd.uploadPictures(data).subscribe(res => {
        console.log(res);
        if (this.imageList.length === 0) {
          this.newAddData.cover = res.data[0]._id;
        }
        // this.newAddData.images.push(...res.data.map(item => item._id));
        this.imageList.push(...res.data.map(item => ({path: item.path, name: item.name, id: item._id})));
        this.cd.detectChanges();
        console.log(this.imageList);
        this.isUploading = false;
      }, err => {
        console.log(err);
        this.admin.alertDanger('يوجد خطا');

        this.isUploading = false;
      });
    }
  }

  log(data) {
    console.log(data);
    console.log(this.newAddData.cover);

  }

  deleteImage(index) {
    this.newAddData.images.splice(index, 1);
    this.imageList.splice(index, 1);
  }

  subCategoryChange(subCategory) {
    const subCateg = this.subCategoryList.find(sub => sub._id === subCategory);
    this.subCatName = subCateg.name;
    this.subCategoryInputs = subCateg?.inputs[0];
    console.log(this.subCategoryInputs, 'subCategoryInputs');
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.imageList, event.previousIndex, event.currentIndex);
  }

  inputsChange(input) {
    console.log(this.newAddData);
  }

  addNewAddv() {
    this.playLoader();
    this.postingAdd = true;
    this.newAddData.cylinderCount = +this.newAddData.cylinderCount;
    this.newAddData.runningKilos = +this.newAddData.runningKilos;
    this.newAddData.price = +this.newAddData.price;
    if (this.category.name === 'قطع غيار') {
      delete this.newAddData.brand;
      delete this.newAddData.model;
      delete this.newAddData.style;
    }
    this.imageList.forEach((img, index) => {
      this.newAddData.images.push({
        order: index,
        id: img.id
      });
    });
    console.log(this.newAddData);
    this.newAdd.postNewAddv(this.newAddData).subscribe(res => {
      this.postingAdd = false;
      this.stopLoader();

      Swal.fire({
        title: 'تم إضافة الإعلان بنجاح',
        text: 'هل تريد إضافة إعلان آخر؟',
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا شكرا'
      }).then((result) => {
        if (result.value) {
          this.location.back();
          setTimeout(() => {
            this.location.forward();
          }, 10);
        } else {
          this.router.navigate(['/']);
        }
      });

      console.log(res);
    }, err => {
      this.stopLoader();
      this.admin.alertDanger('يوجد خطا بالاضافة برجاء ملئ كل البيانات');

      console.log(err);
      this.postingAdd = false;
    });
  }

  // ==== screens control ==== //
  openFirstScreen() {
    this.resetAll();
    this.closeAllScreens();
    this.firstScreen = true;
  }

  openSecondScreen() {
    this.resetAddFields(); // if go back
    this.newAddData.typeOfAd = this.category?.name;
    this.getSubCategories();
    this.closeAllScreens();
    this.secondScreen = true;
  }

  openThirdScreen() {
    this.closeAllScreens();
    this.thirdScreen = true;
  }

  openFourthScreen() {
    if (this.category.name === 'سيارات') {
      this.getBrandList();
      this.closeAllScreens();
      this.fourthScreen = true;
    } else if (this.category.name === 'قطع غيار') {
      this.openThirdScreen();
    }
  }

  openFifthScreen() {
    if (this.category.name === 'قطع غيار') {
      this.getBrandAndModelAndStyleList();
    }
    this.closeAllScreens();
    this.fifthScreen = true;
  }

  openSixScreen() {
    if (this.category.name === 'قطع غيار') {
      this.submitSpare();
    }
    this.closeAllScreens();
    this.sixScreen = true;
  }

  submitSpare() {
    this.newAddData.brands = this.spareBrandList.map(brand => brand._id);
    this.newAddData.models = this.spareModelList.map(model => model._id);
    this.newAddData.styles = this.spareStyleList.map(style => style._id);
    console.log(this.newAddData);
  }

  openSevenScreen() {
    console.log(this.newAddData);
    this.closeAllScreens();
    this.sevenScreen = true;
  }

  openEightScreen() {
    this.closeAllScreens();
    this.eightScreen = true;
  }

  playLoader() {
    $('.loader').fadeIn();
  }

  stopLoader() {
    $('.loader').fadeOut();
  }

  // ==== resetting ====
  closeAllScreens() {
    this.firstScreen = false;
    this.secondScreen = false;
    this.thirdScreen = false;
    this.fourthScreen = false;
    this.fifthScreen = false;
    this.sixScreen = false;
    this.sevenScreen = false;
    this.eightScreen = false;
  }

  resetAddFields() {
    this.newAddData.carPlace = '';
    this.newAddData.driver = null;
    this.newAddData.installment = null;
    this.newAddData.forSell = null;

  }

  resetAll() {
    this.newAddData = {
      category: '',
      agent: '',
      brand: '',
      carPlace: '',
      city: '',
      color: '',
      contactNumber: '',
      cylinderCount: null,
      driver: null,
      forSell: null,
      fuelType: '',
      images: [],
      innerColor: '',
      installment: null,
      manufacturingYear: '',
      model: '',
      movementController: '',
      price: null,
      runningKilos: null,
      speedCount: '',
      style: '',
      text: '',
      title: '',
      typeOfAd: '',
      typeOfSpecifications: '',
      warrantyStatus: '',
      brands: [],
      models: [],
      styles: [],
      manufacturingYears: []
    };
  }
}
