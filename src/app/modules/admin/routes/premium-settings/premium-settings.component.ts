import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {ProfileService} from '../../../../services/profile/profile.service';
import {EditProfile, Profile} from '../../../../models/model';
import {AdvertisingService} from '../../../../services/Advertising/advertising.service';
import {AdminProductsService} from '../../../../services/admin-products.service';

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-premium-settings',
  templateUrl: './premium-settings.component.html',
  styleUrls: ['./premium-settings.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-60px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, 0)'
          }))
        ])
      ])
    ])
  ]
})
export class PremiumSettingsComponent implements OnInit, AfterViewInit {
  // variables
  sendRequest = false;
  isAdmin = false;
  isLoading = false;
  // interfaces
  profileData: EditProfile = {
    firstName: '',
    lastName: '',
    phone: ''
  };
  profileContainer: Profile = {};
  isUploading = false;

  constructor(public profile: ProfileService, private advertisment: AdvertisingService, private cd: ChangeDetectorRef,
              private admin: AdminProductsService,
  ) {
  }

  ngOnInit(): void {
    this.getProfile();
  }

  ngAfterViewInit(): void {
    this.cd.detectChanges();
  }

  filterProfileData() {
    const { firstName, lastName, phone} = {...this.profileContainer?.user};
    this.profileData = { firstName, lastName, phone};
    console.log(this.profileData, 'profile setting');
  }

  uploadImage(evt) {
    this.isUploading = true;

    if (evt.target.files.length && evt.target.files) {
      const data = new FormData();
      for (const img of evt.target.files) {
        data.append('image', img, img.name);
      }
      this.profile.uploadPicture(data).subscribe(res => {
        console.log(res);
        this.profileContainer.user.profilePicture = res.data.path;

        this.profile.changePic({userImage: res.data._id}).subscribe((_res) => {
          this.isUploading = false;
        });
      }, err => {
        console.log(err);
        this.admin.alertDanger(err);
        this.isUploading = false;
      });
    }
  }

  getProfile() {
    const token = localStorage.getItem('harag-token');
    this.isLoading = true;
    this.profile.getUserProfile(token).subscribe(res => {
      this.profileContainer = res.data;
      this.isAdmin = res.data.user.role === 'admin';
      this.isLoading = false;
      console.log(res, 'pp');
      this.filterProfileData();
    }, err => {
      this.isLoading = false;
      // console.error(err);
    });
  }

  updateProfileSubmition() {
    this.sendRequest = true;
    this.profile.updateProfileData(this.profileData).subscribe(res => {
      this.getDataAgain();
      this.sendRequest = false;
      this.advertisment.alertSuccess(`${res.message}`);
    }, err => {
      this.advertisment.alertDanger(`${err.error.error}`);
      this.sendRequest = false;
    });
  }

  getDataAgain() {
    const token = localStorage.getItem('harag-token');
    this.profile.getUserProfile(token).subscribe(res => {
      this.profile.profileContainer = res.data;
    });
  }
}
