import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../../services/products/product.service';
import {forkJoin, Subscription} from 'rxjs';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {Advertisement, Brand, GivenAdvertisement, Inputs, SubCategory} from '../../../../Interfaces/Model';
import {SwiperOptions} from 'swiper';
import {NewAddService} from '../../../../services/new-add.service';
import {SearchService} from '../../../../services/search/search.service';
import {Location} from '@angular/common';
import Swal from 'sweetalert2';
import {ProfileService} from '../../../../services/profile/profile.service';
import {City} from '../../../../models/model';

declare const $: any;

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-edit-advertisement',
  templateUrl: './edit-advertisement.component.html',
  styleUrls: ['./edit-advertisement.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-60px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, 0)'
          }))
        ])
      ])
    ])
  ]
})
export class EditAdvertisementComponent implements OnInit, AfterViewInit, OnDestroy {

  productId: string;
  productSub: Subscription;

  // subscription
  subCategorySub: Subscription;
  brandSub: Subscription;
  modelSub: Subscription;
  styleSub: Subscription;

  // screens variables
  firstScreen = true;
  secondScreen = false;
  thirdScreen = false;
  fourthScreen = false;
  fifthScreen = false;
  sixScreen = false;
  sevenScreen = false;
  eightScreen = false;

  // data variables
  newAddData: GivenAdvertisement = {
    category: {
      _id: '',
      name: '',
      parent: ''
    },
    agent: '',
    brand: {
      name: '',
      _id: ''
    },
    carPlace: '',
    city: '',
    color: '',
    contactNumber: '',
    cylinderCount: null,
    driver: null,
    forSell: null,
    fuelType: '',
    images: [],
    innerColor: '',
    installment: null,
    manufacturingYear: '',
    model: {
      _id: '',
      name: ''
    },
    movementController: '',
    price: null,
    runningKilos: null,
    speedCount: '',
    style: {
      name: '',
      _id: ''
    },
    text: '',
    title: '',
    typeOfAd: '',
    typeOfSpecifications: '',
    warrantyStatus: '',
    brands: [],
    models: [],
    styles: [],
    manufacturingYears: []
  };

  editedAdd: Advertisement = {
    category: '',
    agent: '',
    brand: '',
    carPlace: '',
    city: '',
    color: '',
    contactNumber: '',
    cylinderCount: null,
    driver: null,
    forSell: null,
    fuelType: '',
    images: [],
    innerColor: '',
    installment: null,
    manufacturingYear: '',
    model: '',
    movementController: '',
    price: null,
    runningKilos: null,
    speedCount: '',
    style: '',
    text: '',
    title: '',
    typeOfAd: '',
    typeOfSpecifications: '',
    warrantyStatus: '',
    brands: [],
    models: [],
    styles: [],
    manufacturingYears: []
  };

  categoryList: Brand[] = [];
  category: Brand = {
    name: '',
    _id: ''
  };
  subCategoryList: SubCategory[] = [];
  subCategoryInputs: Inputs = {
    options: [],
    field: '',
    name: ''
  };
  brandList: Brand[] = [];
  brandValue: Brand = {
    name: '',
    _id: ''
  };
  modelList: Brand[] = [];
  modelValue: Brand = {
    name: '',
    _id: ''
  };
  styleList: Brand[] = [];
  styleValue: Brand = {
    _id: '',
    name: ''
  };
  imageList: {
    path: '',
    name: ''
    _id: ''
  }[] = [];
  cities: City[] = [];
  isUploading = false;
  subCatName: string;
  brandName: string;
  modelName: string;
  styleName: string;
  postingAdd = false;
  // spare parts
  spareBrandList: Brand[] = [];
  spareModelList: Brand[] = [];
  spareStyleList: Brand[] = [];
  spareYears: string[] = [];
  spareYearValue = '';

  config: SwiperOptions = {
    pagination: {el: '.swiper-pagination', clickable: true},
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 30,
    lazy: {
      loadPrevNext: true,
    },
  };

  years(startYear = 1980) {
    const currentYear = new Date().getFullYear();
    const years = [];
    startYear = startYear || 1980;
    while (startYear <= currentYear) {
      years.unshift(startYear++);
    }
    return years;
  }

  constructor(private newAdd: NewAddService,
              private search: SearchService,
              private cd: ChangeDetectorRef,
              private router: Router,
              private route: ActivatedRoute,
              private product: ProductService,
              private profile: ProfileService,
              private location: Location) {
    this.route.queryParams.subscribe(res => {
      this.productId = res.id;
    });
  }

  ngOnInit(): void {
    // this.getSubCategories();
    // this.testing();
    // this.getListCategories();
    this.getProductDetailsAndCategoryList();

  }

  checkProduct() {
    if (this.profile?.profileContainer?.products.filter(x => x._id === this.productId).length === 0) {
      this.router.navigate(['/cars']);
    }
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
    // this.checkProduct();
  }

  ngOnDestroy(): void {
    // this.subCategorySub.unsubscribe();
  }

  testing() {
    this.category.name = 'قطع غيار';
    this.closeAllScreens();
    this.openFifthScreen();
  }

  // ==== get methods ==== //

  getListCategories() {
    this.playLoader();
    this.newAdd.getListCategories().subscribe(res => {
      console.log(res);
      this.categoryList = res.data;
      this.stopLoader();
    }, err => {
      this.stopLoader();
      console.log(err);
    });
  }

  getSubCategories() {
    this.playLoader();
    this.subCategorySub = this.newAdd.getSubCategories(this.category._id).subscribe(res => {
      console.log(res.data, 'res sub cat');
      this.subCategoryList = res.data;
      const subcat = res.data.filter(x => x._id === this.newAddData.category._id)[0];
      this.subCategoryChange(this.subCategoryList[this.subCategoryList.indexOf(subcat)]);
      console.log(this.subCategoryList, 'sub category list');
      this.stopLoader();
    }, err => {
      this.stopLoader();
      console.log(err);
    });
  }


  getCities() {
    this.search.getCities().subscribe(res => {
      this.cities = res.data;
    }, err => {
      console.log(err);
    });
  }

  getBrandList() {
    if (this.brandList.length === 0) {
      this.playLoader();
      this.brandSub = this.newAdd.getListBrands().subscribe(res => {
        this.brandList = res.data;
        console.log(this.brandList, 'brandList');
        this.stopLoader();
      }, err => {
        this.stopLoader();
        console.log(err);
      });
    }
  }

  getBrandAndModelAndStyleList() {
    if (this.spareYears.length > 0) {

    } else {
      this.playLoader();
      forkJoin([this.newAdd.getListBrands(), this.newAdd.getModelList(), this.newAdd.getStyleList()]).subscribe(res => {
        this.brandList = res[0].data;
        this.modelList = res[1].data;
        this.styleList = res[2].data;
        this.spareYears = this.years();

        // remove the existing values from years
        for (let i = 0; i <= this.newAddData.manufacturingYears.length; i++) {
          if (this.spareYears.indexOf(this.newAddData.manufacturingYears[i]) > 0) {
            const index = this.spareYears.indexOf(this.newAddData.manufacturingYears[i]);
            this.spareYears.splice(index, 1);
          }
        }

        // for (let i = 0; i <= this.newAddData.brands.length; i++) {
        //   if (this.brandList.indexOf(this.newAddData.brands[i])) {
        //     const index = this.brandList.indexOf(this.newAddData.brands[i]);
        //     this.brandList.splice(index, 1);
        //   }
        // }

        this.stopLoader();
      }, err => {
        this.stopLoader();
      });
    }
  }

  getModelsByBrand() {
    this.newAddData.brand.name = this.brandList.filter(x => x._id === this.newAddData.brand._id)[0]?.name;
    this.brandName = this.newAddData.brand.name;
    console.log(this.newAddData.brand, 'brand in add');
    this.playLoader();
    this.modelSub = this.search.getModelsByBrands({brandID: this.newAddData.brand._id}).subscribe(res => {
      this.modelList = res.data;
      this.stopLoader();
    }, err => {
      this.stopLoader();
      console.log(err);
    });

  }

  getStylesByModel(evt) {
    this.newAddData.model.name = this.modelList.filter(x => x._id === this.newAddData.model._id)[0]?.name;
    this.modelName = this.newAddData.model.name;
    this.playLoader();
    this.styleSub = this.search.getStylesByModels({modelID: this.newAddData.model._id}).subscribe(res => {
      this.stopLoader();
      console.log(res);
      this.styleList = res.data;
    }, err => {
      this.stopLoader();
      console.log(err);
    });
  }

  addToBrands() {
    const index = this.brandList.indexOf(this.brandValue);
    if (!this.spareBrandList.includes(this.brandValue)) {
      this.spareBrandList.push(this.brandValue);
      this.brandList.splice(index, 1);
    }
    this.brandValue = {
      name: '',
      _id: ''
    };
  }

  addToModels() {
    const index = this.modelList.indexOf(this.modelValue);
    if (!this.spareModelList.includes(this.modelValue)) {
      this.spareModelList.push(this.modelValue);
      this.modelList.splice(index, 1);
    }
    this.modelValue = {
      name: '',
      _id: ''
    };
  }

  addToStyles() {
    const index = this.styleList.indexOf(this.styleValue);
    if (!this.spareStyleList.includes(this.styleValue)) {
      this.spareStyleList.push(this.styleValue);
      this.styleList.splice(index, 1);
    }
    this.styleValue = {
      name: '',
      _id: ''
    };
  }

  addToYears() {
    const index = this.spareYears.indexOf(this.spareYearValue);
    if (!this.newAddData.manufacturingYears.includes(this.spareYearValue)) {
      this.newAddData.manufacturingYears.push(this.spareYearValue);
      this.spareYears.splice(index, 1);
    }
    this.spareYearValue = '';
  }

  removeFromYears(index) {
    this.spareYearValue = '';
    this.spareYears.push(this.newAddData.manufacturingYears[index]);
    this.spareYears.sort().reverse();
    this.newAddData.manufacturingYears.splice(index, 1);
  }

  removeFromStyles(index) {
    this.styleValue = {
      name: '',
      _id: ''
    };
    this.styleList.push(this.spareStyleList[index]);
    this.spareStyleList.splice(index, 1);
  }

  removeFromModel(index) {
    this.modelValue = {
      name: '',
      _id: ''
    };
    this.modelList.push(this.spareModelList[index]);
    this.spareModelList.splice(index, 1);
  }

  removeFromBrand(index) {
    this.brandValue = {
      name: '',
      _id: ''
    };
    this.brandList.push(this.spareBrandList[index]);
    this.spareBrandList.splice(index, 1);
  }

  styleChange(evt) {
    this.newAddData.style.name = this.styleList.filter(x => x._id === this.newAddData.style._id)[0]?.name;
    this.styleName = this.newAddData.style.name;
  }

  categoryChange(evt) {
    this.newAddData.typeOfAd = evt.name;
  }

  uploadImage(evt) {
    this.isUploading = true;
    if (evt.target.files.length && evt.target.files) {
      const data = new FormData();
      for (const img of evt.target.files) {
        data.append('image', img, img.name);
      }
      this.newAdd.uploadPictures(data).subscribe(res => {
        console.log(res);

        this.editedAdd.images.push(...res.data.map(item => item._id));
        this.imageList.push(...res.data.map(item => ({path: item.path, name: item.name}))); // [{path: '', name: ''}]
        console.log(this.editedAdd.images);
        this.isUploading = false;
      }, err => {
        console.log(err);
        this.isUploading = false;
      });
    }
  }

  deleteImage(index) {
    this.editedAdd.images.splice(index, 1);
    this.imageList.splice(index, 1);
    this.cd.detectChanges();
  }

  subCategoryChange(subCategory) {
    console.log(subCategory);
    this.subCatName = subCategory.name;
    this.subCategoryInputs = subCategory?.inputs[0];
    console.log(this.subCategoryInputs, 'subCategoryInputs');
  }

  inputsChange(input) {
    console.log(this.newAddData);
  }

  addNewAddv() {
    this.postingAdd = true;
    this.newAddData.cylinderCount = +this.newAddData.cylinderCount;
    this.newAddData.runningKilos = +this.newAddData.runningKilos;
    this.newAddData.price = +this.newAddData.price;
    const images = this.editedAdd.images;
    const {
      agent,
      carPlace,
      city,
      color,
      contactNumber,
      cylinderCount,
      driver,
      forSell,
      fuelType,
      innerColor,
      installment,
      manufacturingYear,
      movementController,
      price,
      runningKilos,
      speedCount,
      text,
      title,
      typeOfAd,
      typeOfSpecifications,
      warrantyStatus,
      manufacturingYears
    } = this.newAddData;

    this.editedAdd = {
      agent,
      carPlace,
      city,
      color,
      contactNumber,
      cylinderCount,
      driver,
      forSell,
      fuelType,
      innerColor,
      installment,
      manufacturingYear,
      movementController,
      price,
      runningKilos,
      speedCount,
      text,
      title,
      typeOfAd,
      typeOfSpecifications,
      warrantyStatus,
      manufacturingYears
    };
    this.editedAdd.images = images;
    this.editedAdd.category = this.newAddData.category._id;
    console.log(this.editedAdd, 'before update');
    console.log(this.editedAdd.images, 'images before update');
    if (this.category.name === 'قطع غيار') {

      // add brand, styles, models
      this.editedAdd.brands = this.newAddData.brands.map(x => x._id);
      this.editedAdd.models = this.newAddData.models.map(x => x._id);
      this.editedAdd.styles = this.newAddData.styles.map(x => x._id);

      delete this.editedAdd.brand;
      delete this.editedAdd.model;
      delete this.editedAdd.style;
      delete this.editedAdd.manufacturingYear;

    } else if (this.category.name === 'سيارات') {

      this.editedAdd.brand = this.newAddData.brand._id;
      this.editedAdd.model = this.newAddData.model._id;
      this.editedAdd.style = this.newAddData.style._id;

      delete this.editedAdd.brands;
      delete this.editedAdd.models;
      delete this.editedAdd.styles;
      delete this.editedAdd.manufacturingYears;
    }
    console.log(this.newAddData);
    this.newAdd.editProduct(this.editedAdd, this.productId).subscribe(res => {
      this.postingAdd = false;

      Swal.fire({
        title: 'تم تعديل الإعلان بنجاح',
        text: 'هل تريد الرجوع إلي إعلاناتي؟',
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'نعم',
        cancelButtonText: 'لا شكرا'
      }).then((result) => {
        if (result.value) {
          this.router.navigate(['/admin/advertisement-management']);
        } else {
          this.router.navigate(['/']);
        }
      });

      console.log(res);
    }, err => {
      console.log(err);
      Swal.fire(
        'حدث خطأ!',
        '',
        'error'
      );
      this.router.navigate(['/']);
      this.postingAdd = false;
    });
  }

  // ==== screens control ==== //
  openFirstScreen() {
    this.resetAll();
    this.closeAllScreens();
    this.firstScreen = true;
  }

  openSecondScreen() {
    // this.resetAddFields(); // if go back
    this.newAddData.typeOfAd = this.category?.name;
    this.getSubCategories();
    this.closeAllScreens();
    this.secondScreen = true;
  }

  openThirdScreen() {
    this.closeAllScreens();
    this.thirdScreen = true;
  }

  openFourthScreen() {
    this.modelValue = this.newAddData?.model;
    this.styleValue = this.newAddData?.style;
    this.getModelsByBrand();
    this.getStylesByModel(this.styleValue);
    console.log(this.brandValue, 'brandValue');
    if (this.category.name === 'سيارات') {
      this.getBrandList();
      this.closeAllScreens();
      this.fourthScreen = true;
    } else if (this.category.name === 'قطع غيار') {
      this.openThirdScreen();
    }
  }

  openFifthScreen() {
    if (this.category.name === 'قطع غيار') {
      this.getBrandAndModelAndStyleList();
    }
    this.closeAllScreens();
    this.fifthScreen = true;
  }

  openSixScreen() {
    if (this.category.name === 'قطع غيار') {
      // this.submitSpare();
    }
    this.closeAllScreens();
    this.sixScreen = true;
  }

  // submitSpare() {
  //   this.newAddData.brands = this.spareBrandList.map(brand => brand._id);
  //   this.newAddData.models = this.spareModelList.map(model => model._id);
  //   this.newAddData.styles = this.spareStyleList.map(style => style._id);
  //   console.log(this.newAddData);
  // }

  openSevenScreen() {
    console.log(this.newAddData);
    this.closeAllScreens();
    this.sevenScreen = true;
  }

  openEightScreen() {
    this.closeAllScreens();
    this.eightScreen = true;
  }

  playLoader() {
    $('.loader').fadeIn();
  }

  stopLoader() {
    $('.loader').fadeOut();
  }

  // ==== resetting ====
  closeAllScreens() {
    this.firstScreen = false;
    this.secondScreen = false;
    this.thirdScreen = false;
    this.fourthScreen = false;
    this.fifthScreen = false;
    this.sixScreen = false;
    this.sevenScreen = false;
    this.eightScreen = false;
  }

  resetAddFields() {
    this.newAddData.carPlace = '';
    this.newAddData.driver = null;
    this.newAddData.installment = null;
    this.newAddData.forSell = null;

  }

  resetAll() {
    this.newAddData = {
      category: {
        _id: '',
        name: '',
        parent: ''
      },
      agent: '',
      brand: {
        name: '',
        _id: ''
      },
      carPlace: '',
      city: '',
      color: '',
      contactNumber: '',
      cylinderCount: null,
      driver: null,
      forSell: null,
      fuelType: '',
      images: [],
      innerColor: '',
      installment: null,
      manufacturingYear: '',
      model: {
        _id: '',
        name: ''
      },
      movementController: '',
      price: null,
      runningKilos: null,
      speedCount: '',
      style: {
        name: '',
        _id: ''
      },
      text: '',
      title: '',
      typeOfAd: '',
      typeOfSpecifications: '',
      warrantyStatus: '',
      brands: [],
      models: [],
      styles: [],
      manufacturingYears: []
    };
  }

  getProductDetailsAndCategoryList() {
    this.playLoader();
    forkJoin([this.newAdd.getListCategories(), this.product.getProdcutDetials(this.productId)]).subscribe(res => {
      this.categoryList = res[0];
      console.log(res[1], 'product');
      this.newAddData = res[1].data.product;
      console.log(this.newAddData, 'my cont');
      this.category.name = res[1].data.product.category.parent.name;
      this.category._id = res[1].data.product.category.parent._id;
      this.imageList.push(...res[1].data.product.images);
      console.log(this.category, 'category');
      this.editedAdd.images.push(...this.imageList.map(x => x._id));

      // spare parts
      this.spareBrandList = this.newAddData.brands;
      this.spareModelList = this.newAddData.models;
      this.spareStyleList = this.newAddData.styles;


      this.openSecondScreen();
    }, err => {
      this.stopLoader();
    });
  }

}
