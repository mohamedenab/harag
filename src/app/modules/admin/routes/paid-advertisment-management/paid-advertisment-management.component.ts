import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {AdvertisingService} from '../../../../services/Advertising/advertising.service';
import {ProductDetails, UserAdvertisments, UserFavorites} from '../../../../models/model';
import {Subscription} from 'rxjs';
import Swal from 'sweetalert2';
import {ProductService} from '../../../../services/products/product.service';
import uikit from 'Uikit';
import {NewAddService} from '../../../../services/new-add.service';
import {SearchService} from '../../../../services/search/search.service';
import {Router} from '@angular/router';
import {ProfileService} from '../../../../services/profile/profile.service';

declare const $: any;

@Component({
  selector: 'app-paid-advertisment-management',
  templateUrl: './paid-advertisment-management.component.html',
  styleUrls: ['./paid-advertisment-management.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-60px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, 0)'
          }))
        ])
      ])
    ])
  ]
})
export class PaidAdvertismentManagementComponent implements OnInit, OnDestroy {
  // variables
  prodSub: Subscription;
  sendRequest = false;
  // array
  advertisingContainer: UserAdvertisments[] = [];
  productEditContainer: ProductDetails = {
    title: '',
    text: '',
    city: '',
    price: null,
    contactNumber: '',
    warrantyStatus: '',
    installment: null,
    carPlace: '',
    driver: null,
    forSell: null,
    runningKilos: null,
    brand: {},
    model: {},
    style: '',
    manufacturingYear: null,
    typeOfSpecifications: '',
    agent: '',
    movementController: '',
    speedCount: '',
    color: '',
    innerColor: '',
    cylinderCount: null,
    fuelType: '',
  };

  constructor(
    public advertising: AdvertisingService,
    private product: ProductService,
    private newAdd: NewAddService,
    private search: SearchService,
    public profile: ProfileService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.getAdvertisingMangeList();
  }

  ngOnDestroy(): void {
    this.prodSub.unsubscribe();
  }

  editAdv(id) {
    this.router.navigate(['/admin/edit-advertisement'], {queryParams: {id}});
  }

  getProductDetails(id) {
    this.productEditContainer = {};
    this.modalLoaderOpen();
    this.product.getProdcutDetials(id).subscribe(res => {
      const {
        title,
        text,
        city,
        price,
        contactNumber,
        warrantyStatus,
        installment,
        carPlace,
        driver,
        forSell,
        runningKilos,
        brand,
        style,
        model,
        manufacturingYear,
        typeOfSpecifications,
        agent,
        movementController,
        speedCount,
        color,
        innerColor,
        cylinderCount,
        fuelType
      } = res.data.product;
      this.productEditContainer = {
        title, text, city, price, contactNumber, warrantyStatus,
        installment,
        carPlace,
        driver,
        forSell,
        runningKilos,
        brand,
        style,
        model,
        manufacturingYear,
        typeOfSpecifications,
        agent,
        movementController,
        speedCount,
        color,
        innerColor,
        cylinderCount,
        fuelType
      };
      console.log(this.productEditContainer);
      this.modalLoaderClose();
    });
  }

  editMyProduct() {
    this.sendRequest = true;
    this.product.editProduct(this.productEditContainer._id, this.productEditContainer).subscribe(res => {
      console.log(res);
      this.getAdvertisingMangeList();
      this.sendRequest = false;
      this.closeModal();
      this.advertising.alertSuccess(`${res.message}`);
    });
  }

  modalLoaderOpen() {
    $('.modal-overlayer').fadeIn();
  }

  modalLoaderClose() {
    $('.modal-overlayer').fadeOut();
  }

  showAdv(id) {
    this.advertising.showAdvertisment(id).subscribe(res => {
    });
  }

  closeModal() {
    uikit.modal('#modal-overflow').hide();
  }

  getBrandsList() {
    this.search.getListBrands().subscribe(res => {
      console.log(res);
    });
  }

  getModelList() {
    this.search.getModelsByBrands({});
  }

  hideAdv(id) {
    this.advertising.hideAdvertisment(id).subscribe(res => {
    });
  }

  activeAdv(id) {
    this.advertising.activeAdvertising(id).subscribe(res => {
      this.advertising.alertSuccess(`${res.message}`);
    }, err => `${err.error.error}`);
  }

  deleteAdv(id) {
    Swal.fire({
      title: 'هل انت متأكد؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, احذف',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.advertising.deleteProduct(id).subscribe(res => {
          this.getAdvertisingMangeList();
        });
        Swal.fire(
          'تم الحذف!',
          '',
          'success'
        );
      }
    });
  }

  getAdvertisingMangeList() {
    this.advertising.showLoader();
    this.prodSub = this.advertising.getAdvMangeList().subscribe(res => {
      this.advertisingContainer = res.data;
      this.advertisingContainer.forEach(el => el.show = el.visible);
      this.advertising.hideLoader();
      console.log(this.advertisingContainer);
    });
  }

}
