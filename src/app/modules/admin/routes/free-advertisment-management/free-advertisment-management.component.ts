import { Component, OnInit } from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-free-advertisment-management',
  templateUrl: './free-advertisment-management.component.html',
  styleUrls: ['./free-advertisment-management.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-60px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, 0)'
          }))
        ])
      ])
    ])
  ]
})
export class FreeAdvertismentManagementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
