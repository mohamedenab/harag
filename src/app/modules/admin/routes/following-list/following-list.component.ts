import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {AdvertisingService} from '../../../../services/Advertising/advertising.service';
import {UserFavorites} from '../../../../models/model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-following-list',
  templateUrl: './following-list.component.html',
  styleUrls: ['./following-list.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-60px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, 0)'
          }))
        ])
      ])
    ])
  ]
})
export class FollowingListComponent implements OnInit, OnDestroy {
  // variables
  favSub: Subscription;
  // array
  favouritesContainer: UserFavorites[] = [];
  constructor(public fav: AdvertisingService) { }

  ngOnInit(): void {
    this.getUserFavourite();
  }

  ngOnDestroy(): void {
    this.favSub.unsubscribe();
  }

  getUserFavourite() {
    this.fav.showLoader();
    this.favSub = this.fav.getMyFavourites().subscribe(res => {
      this.favouritesContainer = res.data;
      this.fav.hideLoader();
      console.log(this.favouritesContainer);
    });
  }
}
