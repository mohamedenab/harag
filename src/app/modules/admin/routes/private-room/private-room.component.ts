import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {SocketService} from '../../../../services/socket.service';
import {GetChatMessages} from '../../../../models/model';
import {ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../../../services/profile/profile.service';
declare const $: any;
@Component({
  selector: 'app-private-room',
  templateUrl: './private-room.component.html',
  styleUrls: ['./private-room.component.scss'],
  animations: [
    trigger('scaleIn', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-50%)'
        }), group([
          animate(250, style({
            opacity: 1
          })), animate(250, style({
            transform: 'translateX(0)'
          }))
        ])
      ])
    ])]
})
export class PrivateRoomComponent implements OnInit, OnDestroy {
  // variables
  messagesContainer: GetChatMessages = {};
  roomId: string;
  token = localStorage.getItem('harag-token');
  messageText = '';
  loading: boolean;

  constructor(public socket: SocketService, private ActiveRouter: ActivatedRoute, public profile: ProfileService) {
    this.getRoomId();
    this.listenToChatMessages();
    this.listenToNewMessage();
  }

  ngOnInit(): void {
    this.getChat();
  }

  ngOnDestroy() {
    this.socket.leaveRoom(this.roomId);
  }

  // when enter the room part //
  getChat() {
    this.loading = true;
    this.socket.getChatMessages(this.roomId).subscribe(res => {
      this.messagesContainer = res.data[0];
      setTimeout(() => {
        this.socket?.getDownWhenEnter();
      }, 100);
      this.loading = false;
      console.log(this.messagesContainer);
    });
  }

  listenToNewMessage() {
    this.socket.listenToNewMessage().subscribe(res => {
     this.messagesContainer.messages.push(res);
      this.getDown();
    });
  }

  getRoomId(): void {
    this.ActiveRouter.params.subscribe(res => {
      this.roomId = res.id;
    });
  }

  listenToChatMessages(): void {
    this.socket.listen('sendMessage').subscribe(res => {
      this.messagesContainer.messages.push(res['data']);
      this.getDown();
      console.log(this.messagesContainer.messages);
    });
  }

  sendMessage(): void {
    if (this.messageText.trim() === '') {
      return;
    } else {
      this.socket.emit('sendMessage', {
          userToken: this.token,
          to: this.profile?.profileContainer?.user?._id === this.messagesContainer?.userOne?._id ? this.messagesContainer?.userTwo?._id : this.messagesContainer?.userOne?._id,
          text: this.messageText,
          chat: this.roomId
      });
      this.messageText = '';
    }
  }

  getDown(): void {
    const scrollDiv = $('.middle-box');
    scrollDiv.animate({scrollTop: scrollDiv.prop('scrollHeight')}, 400);
  }
}

