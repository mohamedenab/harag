import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {Notifications} from "../../../../models/model";
import {AdminService} from "../../../../services/admin/admin.service";
import {Subscription} from "rxjs";
import {AdminProductsService} from '../../../../services/admin-products.service';
import {SocketService} from '../../../../services/socket.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(1.12) translate(-60px, 60px)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1) translate(0, 0)'
          }))
        ])
      ])
    ])
  ]
})
export class NotificationComponent implements OnInit, OnDestroy {

  test  = false;
  notificationSub: Subscription;
  notificationContainer: Notifications = {};
  constructor(public admin: AdminService, private socket: SocketService) { }

  ngOnInit(): void {
    this.socket.emitToNotification();
    this.getNotification();
    setTimeout(() => {
      this.socket.setReadNotification();
      this.socket.emitToNotification();
    }, 6000);
  }
  ngOnDestroy(): void {
  }

  getNotification() {
    this.admin.showLoader();
    this.socket.listenToListNotification().subscribe(res => {
      this.notificationContainer.notifications = res.notifications;
      console.log(res.notifications);
      this.admin.hideLoader();
    });
  }

  readThisNotification(id, index) {
    this.notificationContainer.notifications.splice(index, 1);
    this.admin.readNotification(id).subscribe(res => {
      console.log(res);
    });
  }
}
