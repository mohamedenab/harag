import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MainRoutingModule} from './main-routing.module';
import {MainComponent} from './main.component';
import {HomeComponent} from './routes/home/home.component';
import {CategoryComponent} from './routes/category/category.component';
import {SearchComponent} from './routes/search/search.component';
import {SharedModule} from '../shared/shared.module';
import {NgxUsefulSwiperModule} from 'ngx-useful-swiper';
import {AdvertismentDetailsComponent} from './routes/advertisment-details/advertisment-details.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SparePartsComponent} from './routes/spare-parts/spare-parts.component';
import { SearchbarComponent } from './routes/searchbar/searchbar.component';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import { AboutComponent } from './routes/about/about.component';
import { PrivacyPoliceComponent } from './routes/privacy-police/privacy-police.component';
import { TermsConditionsComponent } from './routes/terms-conditions/terms-conditions.component';

@NgModule({
  declarations: [
    MainComponent,
    HomeComponent,
    CategoryComponent,
    SearchComponent,
    AdvertismentDetailsComponent,
    SparePartsComponent,
    SearchbarComponent,
    AboutComponent,
    PrivacyPoliceComponent,
    TermsConditionsComponent
  ],
    imports: [
        CommonModule,
        MainRoutingModule,
        SharedModule,
        NgxUsefulSwiperModule,
        FormsModule,
        ReactiveFormsModule,
        CarouselModule,
    ]
})
export class MainModule {
}
