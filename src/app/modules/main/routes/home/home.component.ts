import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {ProductService} from '../../../../services/products/product.service';
import {City, HomeProducts} from '../../../../models/model';
import {FormBuilder} from '@angular/forms';
import {Brand, SubCategory} from '../../../../Interfaces/Model';
import {NewAddService} from '../../../../services/new-add.service';
import {Subscription} from 'rxjs';
import {SearchService} from '../../../../services/search/search.service';
import {AuthenticatedService} from '../../../../services/auth-gaurd/authenticated.service';
import {ActivatedRoute, Router} from '@angular/router';
import uikit from 'Uikit';

declare const $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('* => void', [
        style({
          opacity: 1,
          transform: 'translate(0, -28vh)'
        }), group([
          animate(100, style({
            opacity: 0
          })), animate(100, style({
            transform: 'translate(100px, -28vh)'
          }))
        ])
      ]),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translate(-100px, -28vh)'
        }), group([
          animate(100, style({
            opacity: 1
          })), animate(100, style({
            transform: 'translate(0, -28vh)'
          }))
        ])
      ])
    ])
  ]
})
export class HomeComponent implements OnInit {
  // variables
  // array
  homeProcutsContainer: HomeProducts[] = [];
  homeBodyData = {
    categories: ['سيارات وكالة', 'سيارات تأجير', 'سيارات مستعملة', 'سيارات إستيراد']
  };
  bodyShpae = [{img: 'sedan', title: 'سيدان'}, {img: 'hatchback', title: 'هاتشباك'}, {img: 'foorbyfoor', title: 'دفع رباعي'}, {
    img: 'crossover',
    title: 'كروس اوفر'
  }, {
    img: 'sport',
    title: 'رياضية'
  }, {img: 'CONVERTIBLE', title: 'مكشوفه'}, {img: 'classic', title: 'كلاسيك'}, {img: 'pikup', title: 'بيك اب'}];
  activeBody = '';
  recentAds = {};

  constructor(private product: ProductService, public auth: AuthenticatedService, public search: SearchService,
              private router: Router,
  ) {
  }


  ngOnInit(): void {

    this.getAllHomeProducts();
    this.search.getRecentAds(1).subscribe((res) => {
      this.recentAds = {categoryName: 'اخر السيارات المضافه لدى متور اكتور', products: res.data};
    });
    // this.getSubCategories();
  }


  getAllHomeProducts(): void {
    this.product.showLoader();
    this.product.getHomeProcutsData('15', '1', this.homeBodyData).subscribe(res => {
      this.homeProcutsContainer = res.data;
      console.log(res);
      this.product.hideLoader();
    }, err => {
      this.product.hideLoader();
    });
  }

  searchWithmodel(title) {
    this.playLoader();
    this.search.getSearchData(this.search.currentPage, 10, {
      carBody: title, sortBy: 'price',
      sortOrder: 'asc',
    }).subscribe(res => {
      this.search.searchContainer = res.data.products;
      this.search.advCount = res.data.count;
      this.search.calcPages();
      this.search.filterdata = {carBody: title};
      this.stopLoader();
      this.router.navigate(['/search']);
    });
  }

  playLoader() {
    $('.loader').fadeIn();
  }

  stopLoader() {
    $('.loader').fadeOut();
  }

  pay() {
    this.product.checkout().subscribe((res: any) => {
      const script = document.createElement('script');
      script.src = `https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=${res.data}`;
      script.type = 'text/javascript';
      script.async = true;
      script.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(script);
      uikit.modal('#pay').show();
    });

  }
}
