import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Brand, SubCategory} from '../../../../Interfaces/Model';
import {City} from '../../../../models/model';
import {Subscription} from 'rxjs';
import {NewAddService} from '../../../../services/new-add.service';
import {SearchService} from '../../../../services/search/search.service';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';

declare const $, UIkit: any;

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {
  searchGroup = this.fb.group({
    title: '',
    brand: '',
    model: '',
    minPrice: '',
    maxPrice: '',
    place: '',
    startYear: '',
    finishYear: '',
    minRunningKilos: '',
    maxRunningKilos: '',
    carBody: '',
    condition: '',
    movementController: '',
    wheeldrive: '',
    color: '',
    cylinder: '',
    engine: '',
  });
  brandSub: Subscription;
  brandList: Brand[] = [];
  modelList: Brand[] = [];
  styleList: Brand[] = [];
  @Input() relative = false;
  cities: City[] = [];
  bodyShpae = ['هاتشباك', 'سيدان', 'دفع رباعي', 'كروس اوفر', 'رياضية', 'مكشوفه', 'كلاسيك', 'بيك اب'];
  subCategoryList: SubCategory[] = [];
  @Input() filterData;
  innerWidth = 0;

  constructor(private fb: FormBuilder, private newAdd: NewAddService, public search: SearchService, private router: Router) {
  }

  ngOnInit(): void {

    if (this.filterData) {
      this.searchGroup.patchValue(this.filterData);
    }
    this.getBrandList();
    this.getCities();
    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  switch() {
    this.search.advanced = !this.search.advanced;
  }

  years(startYear = 1980) {
    const currentYear = new Date().getFullYear();
    const years = [];
    startYear = startYear || 1980;
    while (startYear <= currentYear) {
      years.unshift(startYear++);
    }
    return years;
  }

  getCities() {
    this.search.getCities().subscribe(res => {
      this.cities = res.data;
    }, err => {
      console.log(err);
    });
  }

  getModelsByBrand(evt) {
    this.search.getModelsByBrands({brandID: evt}).subscribe(res => {
      this.modelList = res.data;
    }, err => {
      console.log(err);
    });

  }

  getBrandList() {
    if (this.brandList.length === 0) {
      this.brandSub = this.newAdd.getListBrands().subscribe(res => {
        this.brandList = res.data;
      }, err => {
        console.log(err);
      });
    }
  }


  getStylesByModel(evt) {
    this.search.getStylesByModels({modelID: evt}).subscribe(res => {
      console.log(res);
      this.styleList = res.data;
    }, err => {
      console.log(err);
    });
  }

  getSearchDataFromDB() {
    UIkit.modal('#modal-search-car').hide();

    if (this.router.url !== '/search') {
      this.playLoader();
    }
    const body = this.searchGroup.value;
    body.sortOrder = this.search.sortOrder;
    body.sortBy = 'price';
    this.search.getSearchData(this.search.currentPage, 10, body).subscribe(res => {
      this.search.searchContainer = res.data.products;
      this.search.advCount = res.data.count;
      this.search.calcPages();
      this.search.filterdata = this.searchGroup.value;
      if (this.router.url !== '/search') {
        this.stopLoader();
        this.router.navigate(['/search']);
      }
    });

  }

  playLoader() {
    $('.loader').fadeIn();
  }

  stopLoader() {
    $('.loader').fadeOut();
  }

  searchdata() {
    // this.search.getSearchData()
  }
}
