import {Component, OnInit} from '@angular/core';
import {SwiperOptions} from 'swiper';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../../services/products/product.service';
import {City, FilterModel, Search, SearchBody, SubCategoryList} from '../../../../models/model';
import {SearchService} from '../../../../services/search/search.service';
import {forkJoin} from 'rxjs';

declare const $: any;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  sendRequest = false;
  currentId;
  content = [
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
    {id: '1'},
  ];
  config: SwiperOptions = {
    pagination: {el: '.swiper-pagination', clickable: true},
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    // freeMode: true,
    spaceBetween: 10,
    keyboard: true,
    scrollbar: {
      el: '.swiper-scrollbar',
      // hide: true
    }
  };
  bodyData = {
    categories: []
  };
  searchBody: SearchBody = {
    sort: '',
    title: '',
    categories: [],
    maxPrice: null,
    minPrice: null,
    startYear: null,
    finishYear: null,
    model: '',
    brand: '',
    style: '',
    place: '',
    maxRunningKilos: null,
    minRunningKilos: null,
    installment: null,
    sortBy: 'price',
    sortOrder: 'asc'
  };
  specCategory: Search[] = [];
  // brands, models and styles..
  brandList: FilterModel[] = [];
  modelsList: FilterModel[] = [];
  stylesList: FilterModel[] = [];
  categoryName = '';
  cities: City[] = [];
  subCategoryList: SubCategoryList[] = [];
  currentPage = 1;
  lastPage = 1;
  advCount = 0;

  years = (startYear = 1980) => {
    const currentYear = new Date().getFullYear();
    const years = [];
    startYear = startYear || 1980;
    while (startYear <= currentYear) {
      years.unshift(startYear++);
    }
    return years;
  };

  constructor(private activated: ActivatedRoute, private product: ProductService,
              private search: SearchService, public router: Router) {
    if (router.url !== '/recent') {
      this.activated.params.subscribe(res => {
        this.currentId = res.id;
        this.searchBody.categories[0] = res.id;
        this.categoryName = res.name;
        this.categoryName = this.categoryName.split('-').join(' ');
        console.log(this.searchBody.categories);
        this.searchByCategory();
      });
    }
  }

  ngOnInit(): void {
    if (this.router.url !== '/recent') {
      this.getBrandsAndCitiesAndSubCategories();
    } else {
      this.product.showLoader();
      this.search.getRecentAds(1).subscribe((res) => {
        this.specCategory = res.data;
        this.product.hideLoader();

      });
    }
  }

  getBrandsAndCitiesAndSubCategories() {
    forkJoin([this.search.getListBrands(), this.search.getCities(), this.search.getListSubCategory()]).subscribe(res => {
      this.brandList = res[0].data;
      this.cities = res[1].data;
      this.subCategoryList = res[2].data;
    });
  }

  searchByCategory() {
    this.product.showLoader();
    this.search.searchByCategory(this.currentPage, 10, {categories: [this.categoryName]}).subscribe(res => {
      console.dir(res);
      this.specCategory = res.data[0].products;
      console.log(this.specCategory);
      this.advCount = res.data[0].count;
      this.calcPages();
      this.product.hideLoader();
    }, err => {
      console.log(err);
    });
  }

  getModlesByBrands(id, index) {
    this.searchBody.brand = id;
    this.brandList.forEach(brand => brand.selected = false);
    this.brandList[index].selected = true;
    this.modelsList = [];
    this.stylesList = [];
    this.search.getModelsByBrands({brandID: id}).subscribe(res => {
      this.modelsList = res.data;
      console.log(this.modelsList);
    });
    this.getSpecificProduct();
  }

  getStylesByModels(id, index) {
    this.searchBody.model = id;
    this.modelsList.forEach(m => m.selected = false);
    this.modelsList[index].selected = true;
    this.stylesList = [];
    this.search.getStylesByModels({modelID: id}).subscribe(res => {
      this.stylesList = res.data;
      console.log(this.stylesList);
    });
    this.getSpecificProduct();
  }

  getSpecificProduct() {
    console.log(this.searchBody);
    this.product.showLoader();
    this.search.getSearchData(this.currentPage, 10, this.searchBody).subscribe(res => {
      this.specCategory = res.data.products;
      this.advCount = res.data.count;
      this.calcPages();
      this.product.hideLoader();
      console.log(res);
    });
  }

  inputChanged() {
    this.getSpecificProduct();
  }

  resetFilter() {
    this.modelsList.forEach(m => m.selected = false);
    this.brandList.forEach(m => m.selected = false);
    this.stylesList.forEach(m => m.selected = false);
    this.searchBody = {
      sort: '',
      title: '',
      categories: [],
      maxPrice: null,
      minPrice: null,
      startYear: null,
      finishYear: null,
      model: '',
      brand: '',
      style: '',
      place: '',
      maxRunningKilos: null,
      minRunningKilos: null,
      installment: null,
      sortBy: 'price',
      sortOrder: 'asc'
    };
    this.searchBody.categories[0] = this.currentId;
    this.searchByCategory();
  }

  filterByDate() {
    this.getSpecificProduct();
  }

  filterByCountry(event) {
    this.searchBody.place = event.target.value;
    this.getSpecificProduct();
  }

  selectStyle(index, id) {
    this.searchBody.style = id;
    this.stylesList.forEach(s => s.selected = false);
    this.stylesList[index].selected = true;
    this.getSpecificProduct();
  }

  previous() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getSpecificProduct();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.lastPage) {
      this.currentPage++;
      this.getSpecificProduct();
      $('html, body').animate({scrollTop: 0}, 'fast');

    }
  }

  first() {
    if (this.currentPage > 1) {
      this.currentPage = 1;
      this.getSpecificProduct();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.lastPage) {
      this.currentPage = this.lastPage;
      this.getSpecificProduct();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  calcPages() {
    if (this.advCount === 0) {
      this.lastPage = 1;
      return;
    }
    this.lastPage = Math.ceil(this.advCount / 10);
  }
}
