import { Component, OnInit } from '@angular/core';
import {HomeProducts} from '../../../../models/model';
import {ProductService} from '../../../../services/products/product.service';
import {animate, group, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-spare-parts',
  templateUrl: './spare-parts.component.html',
  styleUrls: ['./spare-parts.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('* => void', [
        style({
          opacity: 1,
          transform: 'translate(0, -28vh)'
        }), group([
          animate(100, style({
            opacity: 0
          })), animate(100, style({
            transform: 'translate(100px, -28vh)'
          }))
        ])
      ]),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translate(-100px, -28vh)'
        }), group([
          animate(100, style({
            opacity: 1
          })), animate(100, style({
            transform: 'translate(0, -28vh)'
          }))
        ])
      ])
    ])
  ]
})
export class SparePartsComponent implements OnInit {
  homeProcutsContainer: HomeProducts[] = [];
  homeBodyData = {
    categories: ['أنظمة كهرباء', 'أنظمة تبريد محرك', 'أنظمة الدريكسون']
  };
  constructor(private product: ProductService) { }

  ngOnInit(): void {
    this.getAllHomeProducts();
  }


  getAllHomeProducts(): void {
    this.product.showLoader();
    this.product.getHomeProcutsData('5', '1', this.homeBodyData).subscribe(res => {
      this.homeProcutsContainer = res.data;
      console.log(res);
      this.product.hideLoader();
    }, err => {this.product.hideLoader(); });
  }
}
