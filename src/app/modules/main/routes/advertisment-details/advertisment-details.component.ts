import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {ProductService} from '../../../../services/products/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AddComment, Comments, ProductDetails, ProductDetailsParent} from '../../../../models/model';
import {AuthenticatedService} from '../../../../services/auth-gaurd/authenticated.service';
import {SocketService} from '../../../../services/socket.service';
import {AdminProductsService} from '../../../../services/admin-products.service';

declare const $: any;

@Component({
  selector: 'app-advertisment-details',
  templateUrl: './advertisment-details.component.html',
  styleUrls: ['./advertisment-details.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('* => void', [
        style({
          opacity: 1,
          transform: 'translate(0, -28vh)'
        }), group([
          animate(100, style({
            opacity: 0
          })), animate(100, style({
            transform: 'translate(100px, -28vh)'
          }))
        ])
      ]),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translate(-100px, -28vh)'
        }), group([
          animate(100, style({
            opacity: 1
          })), animate(100, style({
            transform: 'translate(0, -28vh)'
          }))
        ])
      ])
    ])
  ]
})
export class AdvertismentDetailsComponent implements OnInit, OnDestroy {
  // variables
  productId;
  currentDate: Date = new Date();
  likeAdv;
  sendCommentVar = false;
  // array
  MONTH_NAMES = [
    'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيه',
    'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'
  ];
  // interfaces
  productDetailsContainer: ProductDetailsParent = {};
  addComment: AddComment = {
    body: ''
  };
  openRoom = false;
  token = localStorage.getItem('harag-token');
  number = '05x xxx xxxx';

  isSpare: boolean;

  constructor(private product: ProductService,
              private router: Router,
              private admin: AdminProductsService,
              private activatedRoute: ActivatedRoute,
              public auth: AuthenticatedService,
              public socket: SocketService) {
    this.activatedRoute.params.subscribe(res => {
      this.productId = res.id;
      this.getProdDetails();
      if (this.token) {
        this.listenToOpeningChat();
      }
    });
  }

  ngOnInit(): void {

  }

  openNewChat(): void {
    this.openRoom = true;
    this.socket.emit('openChat', {
      userToken: this.token,
      userTwo: this.productDetailsContainer?.product?.supplier?._id,
    });
  }

  listenToOpeningChat(): void {
    this.socket.listen('openChat').subscribe(res => {
      this.router.navigateByUrl(`/admin/my-chats/${res}`);
    }, err => this.openRoom = false);
  }

  ngOnDestroy(): void {
  }

  getProdDetails(): void {
    this.product.showLoader();
    this.product.getProdcutDetials(this.productId).subscribe(res => {
      this.productDetailsContainer = res.data;
      this.productDetailsContainer?.product?.comments.reverse();
      this.likeAdv = res.data.product.isFavorited;
      this.isSpare = this.productDetailsContainer?.product?.typeOfAd === 'قطع غيار';
      console.log(this.isSpare);
      console.log(this.productDetailsContainer);
      this.product.hideLoader();
    });
  }

  addCommentToProduct(comment): void {
    this.sendCommentVar = true;
    this.product.addComment(this.productId, this.addComment).subscribe(res => {
      console.log(res);
      this.sendCommentVar = false;
      // this.productDetailsContainer.product.comments.push(res.data[0]);
      this.admin.alertSuccess('تم اضافة التعليق للمراجعة');
      comment.reset();
    }, err => {
      this.sendCommentVar = false;
      console.log(err);
    });
  }

  toggleLove(): void {
    this.likeAdv = !this.likeAdv;
    this.product.giveLikeToAdd(this.productId).subscribe(res => {
      console.log(res);
    });
  }

  getFormattedDate(date, prefomattedDate, hideYear = false) {
    const day = date.getDate();
    const month = this.MONTH_NAMES[date.getMonth()];
    const year = date.getFullYear();
    const hours = date.getHours();
    let minutes = date.getMinutes();

    if (minutes < 10) {
      // Adding leading zero to minutes
      minutes = `0${minutes}`;
    }

    if (prefomattedDate) {
      // Today at 10:20
      // Yesterday at 10:20
      return `${prefomattedDate} عند ${hours}:${minutes}`;
    }

    if (hideYear) {
      // 10. January at 10:20
      return `${day} ${month} عند ${hours}:${minutes}`;
    }

    // 10. January 2017. at 10:20
    return `${day} ${month} ${year} عند ${hours}:${minutes}`;
  }

  timeAgo(dateParam) {
    if (!dateParam) {
      return null;
    }

    const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
    const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
    const today = new Date();
    const yesterday = new Date(+today - DAY_IN_MS);
    const seconds = Math.round((+today - date) / 1000);
    const minutes = Math.round(seconds / 60);
    const isToday = today.toDateString() === date.toDateString();
    const isYesterday = yesterday.toDateString() === date.toDateString();
    const isThisYear = today.getFullYear() === date.getFullYear();


    if (seconds < 5) {
      return 'الان';
    } else if (seconds < 60) {
      return `منذ ${seconds} ثوان`;
    } else if (seconds < 90) {
      return 'منذ دقيقة تقريبا';
    } else if (minutes < 60) {
      return `منذ ${minutes} دقيقة`;
    } else if (isToday) {
      return this.getFormattedDate(date, 'اليوم'); // Today at 10:20
    } else if (isYesterday) {
      return this.getFormattedDate(date, 'امس'); // Yesterday at 10:20
    } else if (isThisYear) {
      return this.getFormattedDate(date, false, true); // 10. January at 10:20
    }

    return this.getFormattedDate(date, false, false); // 10. January 2017. at 10:20
  }

  showNumer() {
    this.product.showLoader();

    this.product.showNumber(this.productId).subscribe((res: any) => {
      this.number = res.data.contactNumber;
      this.product.hideLoader();
      // window.open(`tel:${res.data.contactNumber}`);
      document.location.href = `tel:${res.data.contactNumber}`;
    });
  }

  showWhatsapp() {
    this.product.showLoader();
    this.product.showWhatsapp(this.productId).subscribe((res: any) => {
      this.number = res.data.contactNumber;
      this.product.hideLoader();
      window.open(`https://api.whatsapp.com/send?phone=${res.data.contactNumber}`, '_blank');

    });
  }

}
