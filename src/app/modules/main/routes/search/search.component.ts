import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {City, FilterModel, Search, SearchBody, SubCategoryList} from '../../../../models/model';
import {ActivatedRoute, Router} from '@angular/router';
import {SearchService} from '../../../../services/search/search.service';
import {ProductService} from '../../../../services/products/product.service';
import {NgForm} from '@angular/forms';
import {SubCategory} from '../../../../Interfaces/Model';
import {forkJoin} from 'rxjs';

declare const $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('* => void', [
        style({
          opacity: 1,
          transform: 'translate(0, -28vh)'
        }), group([
          animate(100, style({
            opacity: 0
          })), animate(100, style({
            transform: 'translate(100px, -28vh)'
          }))
        ])
      ]),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translate(-100px, -28vh)'
        }), group([
          animate(100, style({
            opacity: 1
          })), animate(100, style({
            transform: 'translate(0, -28vh)'
          }))
        ])
      ])
    ])
  ]
})
export class SearchComponent implements OnInit, OnDestroy {
  // variables
  sendRequest = false;
  queryText = '';
  subcategoryList: SubCategoryList[] = [];
  currentPage = 1;
  // arraies
  listBrandsContainer: FilterModel[] = [];
  listCategoryContainer: FilterModel[] = [];
  modelByBrandContainer: FilterModel[] = [];
  styleByModelContainer: FilterModel[] = [];
  // interfaces
  view = 'grid';
  searchBody: SearchBody = {
    sort: '',
    brand: '',
    categories: [],
    minPrice: null,
    maxPrice: null,
    maxRunningKilos: null,
    minRunningKilos: null,
    model: '',
    place: '',
    startYear: null,
    finishYear: null,
    style: '',
    title: ''
  };
  cities: City[] = [];

  constructor(private activeatedRoute: ActivatedRoute,
              public search: SearchService,
              private product: ProductService,
              private router: Router) {
    this.activeatedRoute.queryParams.subscribe(res => {
      this.queryText = res.searchText;
      this.searchBody.title = res.searchText;
    });
  }


  ngOnInit(): void {
    this.product.hideLoader();
    this.search.advanced = true;
    // this.getCategoryBrands();
  }


  trackByFn(index, item) {
    return index;
  }

  previous() {
    if (this.currentPage > 1) {
      this.search.currentPage--;
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.search.lastPage) {
      this.search.currentPage++;
      $('html, body').animate({scrollTop: 0}, 'fast');

    }
  }

  first() {
    if (this.currentPage > 1) {
      this.search.currentPage = 1;
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.search.lastPage) {
      this.search.currentPage = this.search.lastPage;
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  changeSearch(order) {
    if (this.router.url !== '/search') {
      this.playLoader();
    }
    this.search.sortOrder = order;
    const body = this.search.filterdata;
    body['sortOrder'] = order;
    body['sortBy'] = 'price';
    this.search.searchContainer = [];
    this.search.getSearchData(this.search.currentPage, 10, body).subscribe(res => {
      this.search.searchContainer = res.data.products;
      this.search.advCount = res.data.count;
      this.search.calcPages();
      if (this.router.url !== '/search') {
        this.stopLoader();
        this.router.navigate(['/search']);
      }
    });
  }

  playLoader() {
    $('.loader').fadeIn();
  }

  stopLoader() {
    $('.loader').fadeOut();
  }

  ngOnDestroy() {
    this.search.advanced = false;
  }
}
