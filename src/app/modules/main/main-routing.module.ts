import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {MainComponent} from './main.component';
import {HomeComponent} from './routes/home/home.component';
import {CategoryComponent} from './routes/category/category.component';
import {SearchComponent} from './routes/search/search.component';
import {AdvertismentDetailsComponent} from './routes/advertisment-details/advertisment-details.component';
import {SparePartsComponent} from './routes/spare-parts/spare-parts.component';
import {AboutComponent} from './routes/about/about.component';
import {PrivacyPoliceComponent} from './routes/privacy-police/privacy-police.component';
import { TermsConditionsComponent } from './routes/terms-conditions/terms-conditions.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        redirectTo: 'cars',
        pathMatch: 'full'
      },
      {
        path: 'cars',
        component: HomeComponent
      },
      {
        path: 'spare-part',
        component: SparePartsComponent
      },
      {
        path: 'category/:id/:name',
        component: CategoryComponent
      },
      {
        path: 'recent',
        component: CategoryComponent
      },
      {
        path: 'search',
        component: SearchComponent
      },
      {
        path: 'products/:id/:title',
        component: AdvertismentDetailsComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'privacy-police',
        component: PrivacyPoliceComponent
      },
      {
        path: 'terms-condition',
        component: TermsConditionsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
