import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NavbarComponent} from '../../components/navbar/navbar.component';
import {FooterComponent} from '../../components/footer/footer.component';
import {NgxUsefulSwiperModule} from 'ngx-useful-swiper';
import {MainRoutingModule} from '../main/main-routing.module';
import {CategorySliderComponent} from '../../components/category-slider/category-slider.component';
import {AdvertismentCardComponent} from '../../components/advertisment-card/advertisment-card.component';
import {AdminSidebarComponent} from '../../components/admin-sidebar/admin-sidebar.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TooltipModule} from 'ngx-bootstrap/tooltip';


@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    CategorySliderComponent,
    AdvertismentCardComponent,
    AdminSidebarComponent
  ],
  exports: [
    NavbarComponent,
    CategorySliderComponent,
    AdvertismentCardComponent,
    AdminSidebarComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxUsefulSwiperModule,
    MainRoutingModule, TooltipModule.forRoot()
  ]
})
export class SharedModule {
}
