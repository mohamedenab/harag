import { Component, OnInit } from '@angular/core';
import {AdminStatisticsService} from '../../../../services/admin-statistics.service';
import {Statistics} from '../../../../models/model';

@Component({
  selector: 'app-admin-statistics',
  templateUrl: './admin-statistics.component.html',
  styleUrls: ['./admin-statistics.component.scss']
})
export class AdminStatisticsComponent implements OnInit {

  statisticsData: Statistics = {
    usersCount: null,
    subUsersCount: null,
    productsCount: null
  };
  loading = false;

  constructor(private statistics: AdminStatisticsService) { }

  ngOnInit(): void {
    this.getStatistics();
  }

  getStatistics() {
    this.loading = true;
    this.statistics.getStatistics().subscribe(res => {
      this.statisticsData = res.data;
      this.loading = false;
      console.log(res);
    }, err => {
      console.log(err);
    });
  }


}
