import {Component, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {AdminProductsService} from '../../../../services/admin-products.service';
import {ProductList} from '../../../../models/model';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-advertisement-approval',
  templateUrl: './advertisement-approval.component.html',
  styleUrls: ['./advertisement-approval.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.8)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class AdvertisementApprovalComponent implements OnInit {
  // variables
  currentPage = 1;
  sendRequest = false;
  lastPage = 1;
  productsCount = 0;
  isLoading = false;
  searchText = '';
  // interfaces;
  listProductsContainer: ProductList = {};

  constructor(private admin: AdminProductsService, private router: Router) {
  }

  ngOnInit(): void {
    this.getProductList();
  }

  getProductList() {
    this.isLoading = true;
    this.admin.getProductList(this.currentPage, 5).subscribe(res => {
      this.listProductsContainer = res.data;
      this.productsCount = res.data.count;
      this.calcPages();
      this.isLoading = false;
      console.log(res, 'res');
    }, err => {
      this.isLoading = false;
    });
  }

  approveProduct(id) {
    this.isLoading = true;
    this.sendRequest = true;
    this.admin.approveProduct(id).subscribe(res => {
      this.sendRequest = false;
      this.isLoading = false;
      this.getProductList();
      this.admin.alertSuccess('تم قبول هذا الاعلان');
    }, err => {
      this.sendRequest = false;
      this.isLoading = false;
      this.admin.alertDanger('لم يتم قبول هذا الاعلان');
    });
  }

  editAdv(id) {
    this.router.navigate(['/admin/edit-advertisement'], {queryParams: {id}});
  }


  deleteProduct(id) {
    Swal.fire({
      title: 'هل انت متاكد ؟',
      text: 'لن يكون هذا الاعلان متاح!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم متأكد',
      cancelButtonText: 'لست متأكد'
    }).then((result) => {
      if (result.value) {
        this.sendRequest = true;
        this.admin.deleteProduct(id).subscribe(res => {
          this.sendRequest = false;
          this.getProductList();
          Swal.fire(
            'تم حذف هذا الاعلان',
            'هذا الاعلان لم يعد متاح',
            'success'
          );
        });
      }
    });
  }


  previous() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.lastPage) {
      this.currentPage++;
      this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');

    }
  }

  first() {
    if (this.currentPage > 1) {
      this.currentPage = 1;
      this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.lastPage) {
      this.currentPage = this.lastPage;
      this.getProductList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  calcPages() {
    if (this.productsCount === 0) {
      this.lastPage = 1;
      return;
    }
    this.lastPage = Math.ceil(this.productsCount / 5);
  }

  search() {
    this.listProductsContainer.products.length = 0;
    this.isLoading = true;
    if (this.searchText.length > 0) {
      this.admin.search(this.searchText, 1, 5).subscribe(res => {
        this.listProductsContainer = res.data;
        this.productsCount = res.data.count;
        console.log(res.data);
        this.calcPages();
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
      });
    } else {
      this.getProductList();
    }
    this.isLoading = false;
  }

}
