import { Component, OnInit } from '@angular/core';
import { animate, group, style, transition, trigger } from '@angular/animations';
import { AdminNewModelStyleService } from '../../../../services/admin-new-model-style.service';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { SearchService } from '../../../../services/search/search.service';
import { Brand } from '../../../../Interfaces/Model';

declare const $: any;

@Component({
  selector: 'app-new-model',
  templateUrl: './new-model.component.html',
  styleUrls: ['./new-model.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.8)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class NewModelComponent implements OnInit {

  brand = true;
  model = false;
  style = false;
  isLoading = false;
  brandList: Brand[] = [];
  modelList: Brand[] = [];

  newBrand = {
    name: ''
  };
  newModel = {
    name: '',
    brandID: ''
  };
  newStyle = {
    name: '',
    modelID: ''
  };


  constructor(private service: AdminNewModelStyleService, private search: SearchService) {

  }

  ngOnInit(): void {
  }

  addNewBrand(form: NgForm) {
    this.isLoading = true;
    this.service.addNewBrand(this.newBrand).subscribe(res => {
      Swal.fire(
        'تم إضافة الصانع بنجاح!',
        '',
        'success'
      );
      console.log(res);
      form.reset();
      this.isLoading = false;
    }, err => {
      console.log(err);
      this.isLoading = false;
      Swal.fire(
        err.error.message,
        '',
        'error'
      );
    });
  }

  addNewModel(form: NgForm) {
    this.isLoading = true;
    this.service.addNewModel(this.newModel).subscribe(res => {
      Swal.fire(
        'تم إضافة الموديل بنجاح!',
        '',
        'success'
      );
      console.log(res);
      form.reset();
      this.isLoading = false;
    }, err => {
      console.log(err);
      this.isLoading = false;
      Swal.fire(
        err.error.message,
        '',
        'error'
      );
    });
  }

  addNewStyle(form: NgForm) {
    this.isLoading = true;
    this.service.addNewStyle(this.newStyle).subscribe(res => {
      Swal.fire(
        'تم إضافة الطراز بنجاح!',
        '',
        'success'
      );
      console.log(res);
      form.reset();
      this.isLoading = false;
    }, err => {
      console.log(err);
      this.isLoading = false;
      Swal.fire(
        err.error.message,
        '',
        'error'
      );
    });
  }

  getListBrands() {
    $('.overlay').fadeIn(50);
    this.search.getListBrands().subscribe(res => {
      this.brandList = res.data;
      console.log(res);
      $('.overlay').fadeOut(50);
    }, err => {
      console.log(err);
      $('.overlay').fadeOut(50);
    });
  }

  getModelList() {
    $('.overlay').fadeIn(50);
    this.search.getListModels().subscribe(res => {
      console.log(res);
      this.modelList = res.data;
      $('.overlay').fadeOut(50);
    }, err => {
      console.log(err);
      $('.overlay').fadeOut(50);
    });
  }

  closeAll() {
    this.brand = false;
    this.model = false;
    this.style = false;
  }

  openBrand() {
    if (!this.brand) {
      this.closeAll();
      this.brand = true;
    }
  }

  openModel() {
    if (!this.model) {
      this.closeAll();
      this.getListBrands();
      this.model = true;
    }
  }

  openStyle() {
    if (!this.style) {
      this.closeAll();
      this.getModelList();
      this.style = true;
    }
  }

}
