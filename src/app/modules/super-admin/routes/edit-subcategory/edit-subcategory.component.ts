import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../../../../services/category.service';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import uikit from 'Uikit';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-subcategory',
  templateUrl: './edit-subcategory.component.html',
  styleUrls: ['./edit-subcategory.component.scss'],
  animations: [trigger('fadeHide', [
    transition('void => *', [
      style({
        opacity: 0,
        transform: 'scale(0.8)'
      }), group([
        animate(120, style({
          opacity: 1
        })), animate(100, style({
          transform: ' scale(1)'
        }))
      ])
    ])
  ])]
})
export class EditSubCategoryComponent implements OnInit {
  categories = [];
  isLoading = true;
  loadingReq = false;
  category = new FormControl(['', [Validators.required]]);
  subCategories = [];
  categoryForm: FormGroup;
  loadingOp = false;
  editMode = false;
  editId = '';
  Toast = Swal.mixin({
    toast: true,
    position: 'top-left',
    showConfirmButton: false,
    timer: 2500,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });

  constructor(private categoryService: CategoryService, private fb: FormBuilder) {
    this.categoryForm = this.fb.group({
      categoryName: '',
      input: this.fb.array([])

    });
  }

  ngOnInit(): void {
    this.categoryService.getCategoryList().subscribe(res => {
      this.categories = res.data;
      this.isLoading = false;
      console.log(res);
    });
    this.category.valueChanges.subscribe((value) => {
      console.log(value);
      this.categoryService.getSubCategoryList(value).subscribe((res) => {
        console.log(res);
        this.subCategories = res.data;
      });
    });
  }

  addInput(value, index) {
    this.loadingOp = true;
    (this.categoryForm.get('input') as FormArray).push(this.newInput(value));
    if (index > -1) {
      value.options.forEach((option) => {
        this.addoption(option, index);
      });
    }
    this.loadingOp = false;
  }

  newInput(value): FormGroup {
    if (value.name === undefined) {
      return this.fb.group({
        name: ['', Validators.required],
        field: ['', [Validators.pattern(/^[a-z0-9-]+$/), Validators.required]],
        options: this.fb.array([])

      });
    } else {
      return this.fb.group({
        name: [value.name, Validators.required],
        field: [value.field, [Validators.pattern(/^[a-z0-9-]+$/), Validators.required]],
        options: this.fb.array([])

      });
    }
  }

  newoption(Value): FormGroup {
    if (Value.name === undefined) {
      return this.fb.group({
        name: ['', Validators.required],
        value: ['', [Validators.pattern(/^[a-z0-9-]+$/), Validators.required]],
      });
    } else {
      return this.fb.group({
        name: [Value.name, Validators.required],
        value: [Value.value, [Validators.pattern(/^[a-z0-9-]+$/), Validators.required]],
      });
    }
  }

  deleteoptionSubCategory(i: number) {
    (this.categoryForm.get('inputs') as FormArray).removeAt(i);

  }

  deleteoption(i, j) {
    (((this.categoryForm.controls.input as FormArray)
      .controls[i] as FormGroup).controls.options as FormArray).removeAt(j);
  }

  get inputs() {
    return (this.categoryForm.controls.input as FormArray);
  }

  addoption(value, index) {
    (((this.categoryForm.controls.input as FormArray)
      .controls[index] as FormGroup).controls.options as FormArray).push(this.newoption(value));


  }

  edit(option) {
    this.editMode = true;
    this.loadingOp = true;
    this.editId = option.parent;
    this.categoryForm.reset();
    (this.categoryForm.get('input') as FormArray).clear();
    this.categoryForm.patchValue({
      categoryName: option.name,
    });
    if (option.inputs.length > 0) {
      option.inputs.forEach((input, index) => {
        this.addInput(input, index);
      });
    } else {
      this.loadingOp = false;

    }

  }

  updateSubCategory() {
    this.categoryService.updateSubCategory(this.categoryForm.value, this.editId).subscribe((res) => {
      console.log(res);

    });
  }

  addSubCategory() {
    const obj = {
      parent: this.category.value,
      categoryName: this.categoryForm.value.categoryName
    };

    this.categoryService.addSubCategory(obj).subscribe((res: any) => {
      const obj2 = {input: this.categoryForm.value.input[0]};
      this.categoryService.updateSubCategory(obj2, res.data._id).subscribe((response) => {
        console.log(res);
        uikit.modal('#subCategory-modal').hide();
        this.Toast.fire({
          icon: 'success',
          title: 'تم انشاء الفئة الفرعية بنجاح'
        });
      });
    }, (err) => {
      this.Toast.fire({
        icon: 'error',
        title: err.message
      });
    });

  }

  newSubcategory() {
    this.editMode = false;
    this.loadingOp = true;
    this.categoryForm.reset();
    (this.categoryForm.get('input') as FormArray).clear();
    this.categoryForm.patchValue({
      subCategoryName: '',
    });
    this.addInput({}, -1);
    this.loadingOp = false;

  }
}
