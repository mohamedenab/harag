import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, group, style, transition, trigger} from '@angular/animations';
import {AdminUsersService} from '../../../../services/admin-users.service';
import {User} from '../../../../Interfaces/Model';
import {Subscription} from 'rxjs';
import Swal from 'sweetalert2';
import {ProfileService} from '../../../../services/profile/profile.service';
import {Register} from '../../../../models/model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import uikit from 'Uikit';

declare const $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [
    trigger('fadeHide', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'scale(0.8)'
        }), group([
          animate(120, style({
            opacity: 1
          })), animate(100, style({
            transform: ' scale(1)'
          }))
        ])
      ])
    ])
  ]
})
export class UsersComponent implements OnInit, OnDestroy {

  userList: User[] = [];
  userListTemp: User[] = [];
  adminList: User[] = [];
  moderatorList: User[] = [];
  currentPage = 1;
  userSub: Subscription;
  userCount = 0;
  lastPage = 0;
  isLoading: boolean;
  searchText = '';
  isSearching = false;
  adminTab = false;
  userTab = true;
  moderatorsTab = false;
  registerData: Register = {
    firstName: '',
    lastName: '',
    password: '',
    phone: '',
  };
  registerForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(4)])
  });
  sendRequest = false;
  Toast = Swal.mixin({
    toast: true,
    position: 'top-left',
    showConfirmButton: false,
    timer: 2500,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });

  constructor(private users: AdminUsersService, public profile: ProfileService) {
  }

  ngOnInit(): void {
    this.getUserList();
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

  getUserList() {
    this.adminList.length = 0;
    this.moderatorList.length = 0;
    this.isLoading = true;
    this.userSub = this.users.getListUsers(this.currentPage, 10).subscribe(res => {
      console.log(res);
      this.userList = res.data.users;
      this.userListTemp = res.data.users;
      this.userCount = res.data.count;
      this.isLoading = false;
      this.calcPages();
    }, err => {
      console.log(err);
      this.isLoading = false;
    });
  }

  getAdminList() {
    this.userList.length = 0;
    this.moderatorList.length = 0;
    this.isLoading = true;
    this.userSub = this.users.getAdminList().subscribe(res => {
      console.log(res);
      this.adminList = res.data;
      console.log(this.adminList);
      this.isLoading = false;
    }, err => {
      console.log(err);
      this.isLoading = false;
    });
  }

  getModeratorList() {
    this.userList.length = 0;
    this.adminList.length = 0;
    this.isLoading = true;
    this.userSub = this.users.getModeratorList().subscribe(res => {
      console.log(res);
      this.moderatorList = res.data;
      console.log(this.moderatorList);
      this.isLoading = false;
    }, err => {
      console.log(err);
      this.isLoading = false;
    });
  }

  deleteUser(userId, index) {

    Swal.fire({
      title: 'هل انت متأكد من حذف هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, احذف',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.deleteUser(userId).subscribe(res => {
          this.getUserList();
          Swal.fire(
            'تم الحذف!',
            '',
            'success'
          );
        });
      }
    });


  }

  addUser() {
    this.sendRequest = true;
    this.registerData.firstName = this.registerForm.controls.firstName.value;
    this.registerData.lastName = this.registerForm.controls.lastName.value;
    this.registerData.phone = this.registerForm.controls.phone.value;
    this.registerData.password = this.registerForm.controls.password.value;
    this.users.addUser(this.registerData).subscribe((res) => {
      uikit.modal('#user-modal').hide();
      this.alertSuccess('تم إنشاء حساب جديد بنجاح, اهلا بك!');

    });
  }
  alertSuccess(message: string) {
    this.Toast.fire({
      icon: 'success',
      title: message
    });
  }
  suspendUser(userId, index) {

    Swal.fire({
      title: 'هل انت متأكد من تعطيل هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم، عطل',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.suspendUser(userId).subscribe(res => {
          this.userList[index].suspended = true;
          console.log(res);
          Swal.fire(
            'تم تعطيل الحساب!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });

  }

  suspendModerator(userId, index) {

    Swal.fire({
      title: 'هل انت متأكد من تعطيل هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم، عطل',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.suspendUser(userId).subscribe(res => {
          this.moderatorList[index].suspended = true;
          console.log(res);
          Swal.fire(
            'تم تعطيل الحساب!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });

  }

  unsuspendUser(userId, index) {
    Swal.fire({
      title: 'هل انت متأكد من تفعيل هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم، فَعل',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.unsuspendUser(userId).subscribe(res => {
          this.userList[index].suspended = false;
          console.log(res);
          Swal.fire(
            'تم تفعيل الحساب!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });
  }

  unsuspendModerator(userId, index) {
    Swal.fire({
      title: 'هل انت متأكد من تفعيل هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم، فَعل',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.unsuspendUser(userId).subscribe(res => {
          this.moderatorList[index].suspended = false;
          console.log(res);
          Swal.fire(
            'تم تفعيل الحساب!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });
  }

  deleteAdmin(userId, index) {

    Swal.fire({
      title: 'هل انت متأكد من حذف هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, احذف',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.deleteUser(userId).subscribe(res => {
          this.getAdminList();
          Swal.fire(
            'تم الحذف!',
            '',
            'success'
          );
        });
      }
    });


  }

  deleteModerator(userId, index) {

    Swal.fire({
      title: 'هل انت متأكد من حذف هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, احذف',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.deleteUser(userId).subscribe(res => {
          this.getModeratorList();
          Swal.fire(
            'تم الحذف!',
            '',
            'success'
          );
        });
      }
    });


  }

  suspendAdmin(userId, index) {

    Swal.fire({
      title: 'هل انت متأكد من تعطيل هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم، عطل',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.suspendUser(userId).subscribe(res => {
          this.adminList[index].suspended = true;
          console.log(res);
          Swal.fire(
            'تم تعطيل الحساب!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });

  }

  unsuspendAdmin(userId, index) {
    Swal.fire({
      title: 'هل انت متأكد من تفعيل هذا الحساب؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم، فَعل',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.unsuspendUser(userId).subscribe(res => {
          this.adminList[index].suspended = false;
          console.log(res);
          Swal.fire(
            'تم تفعيل الحساب!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });
  }

  makeAdmin(userId) {

    Swal.fire({
      title: 'هل انت متأكد من تنصيب هذا المستخدم مسئولا؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, تنصيب',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.makeAdmin(userId).subscribe(res => {
          this.getUserList();
          Swal.fire(
            'تم تنصيب المستخدم مسئولا!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });

  }

  makeModerator(userId) {

    Swal.fire({
      title: 'هل انت متأكد من تنصيب هذا المستخدم مشرفا؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, تنصيب',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.makeModerator(userId).subscribe(res => {
          this.getUserList();
          Swal.fire(
            'تم تنصيب المستخدم مشرفا!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });

  }

  removeAdmin(userId) {

    Swal.fire({
      title: 'هل انت متأكد من الغاء تنصيب هذا المستخدم مسئولا؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, الغاء',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.removeAdmin(userId).subscribe(res => {
          this.getAdminList();
          Swal.fire(
            'تم الغاء تنصيب المستخدم مسئولا!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });

  }

  removeModerator(userId) {

    Swal.fire({
      title: 'هل انت متأكد من الغاء تنصيب هذا المستخدم مشرفا؟',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'نعم, ازالة',
      cancelButtonText: 'لا'
    }).then((result) => {
      if (result.value) {
        this.users.removeModerator(userId).subscribe(res => {
          this.getModeratorList();
          Swal.fire(
            'تم الغاء تنصيب المستخدم مشرفا!',
            '',
            'success'
          );
        }, err => {
          console.log(err);
        });
      }
    });

  }

  search() {
    this.isLoading = true;
    this.isSearching = true;
    if (this.searchText.length > 0) {
      this.users.search(this.searchText).subscribe(res => {
        this.userList = res.data.users;
        this.isLoading = false;
      });
    } else {
      this.getUserList();
    }
  }

  previous() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getUserList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  next() {
    if (this.currentPage < this.lastPage) {
      this.currentPage++;
      this.getUserList();
      $('html, body').animate({scrollTop: 0}, 'fast');

    }
  }

  first() {
    if (this.currentPage > 1) {
      this.currentPage = 1;
      this.getUserList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  last() {
    if (this.currentPage < this.lastPage) {
      this.currentPage = this.lastPage;
      this.getUserList();
      $('html, body').animate({scrollTop: 0}, 'fast');
    }
  }

  calcPages() {
    if (this.userCount === 0) {
      this.lastPage = 1;
      return;
    }
    this.lastPage = Math.ceil(this.userCount / 10);
  }

  closeAll() {
    this.adminTab = false;
    this.userTab = false;
    this.moderatorsTab = false;
  }

  openUsers() {
    if (!this.userTab) {
      this.getUserList();
      this.closeAll();
      this.userTab = true;
    }
  }

  openAdmin() {
    if (!this.adminTab) {
      this.getAdminList();
      this.closeAll();
      this.adminTab = true;
    }
  }

  openModerators() {
    if (!this.moderatorsTab) {
      this.getModeratorList();
      this.closeAll();
      this.moderatorsTab = true;
    }
  }

  trackByFn(index, item) {
    return index;
  }

}
