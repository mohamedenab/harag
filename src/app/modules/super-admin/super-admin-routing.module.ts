import {NewModelComponent} from './routes/new-model/new-model.component';
import {UsersComponent} from './routes/users/users.component';
import {AdvertisementApprovalComponent} from './routes/advertisement-approval/advertisement-approval.component';
import {CommentsApprovalComponent} from './routes/comments-approval/comments-approval.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SuperAdminComponent} from './super-admin.component';
import {AdminModeratorGuard} from '../../services/auth-gaurd/admin-moderator.guard';
import {AdminStatisticsComponent} from './routes/admin-statistics/admin-statistics.component';
import {AdminGuard} from '../../services/auth-gaurd/admin.guard';
import {EditSubCategoryComponent} from './routes/edit-subcategory/edit-subcategory.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: SuperAdminComponent,
    canActivate: [AdminModeratorGuard],
    children: [
      {
        path: '',
        redirectTo: 'comments', pathMatch: 'full'
      },
      {
        path: 'comments',
        component: CommentsApprovalComponent
      },
      {
        path: 'advertisements',
        component: AdvertisementApprovalComponent
      }, {
        path: 'edit-subCategory',
        component: EditSubCategoryComponent
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'new-model-style',
        component: NewModelComponent
      },
      {
        path: 'statistics',
        component: AdminStatisticsComponent,
        canActivate: [AdminGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperAdminRoutingModule {
}
