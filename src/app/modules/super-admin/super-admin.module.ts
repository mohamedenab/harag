import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SuperAdminRoutingModule} from './super-admin-routing.module';
import {SuperAdminComponent} from './super-admin.component';
import {NewModelComponent} from './routes/new-model/new-model.component';
import {AdvertisementApprovalComponent} from './routes/advertisement-approval/advertisement-approval.component';
import {CommentsApprovalComponent} from './routes/comments-approval/comments-approval.component';
import {UsersComponent} from './routes/users/users.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminStatisticsComponent} from './routes/admin-statistics/admin-statistics.component';
import {EditSubCategoryComponent} from './routes/edit-subcategory/edit-subcategory.component';


@NgModule({
  declarations: [
    SuperAdminComponent,
    NewModelComponent,
    AdvertisementApprovalComponent,
    CommentsApprovalComponent,
    UsersComponent,
    AdminStatisticsComponent,
    EditSubCategoryComponent
  ],
  imports: [
    CommonModule,
    SuperAdminRoutingModule,
    SharedModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class SuperAdminModule {
}
