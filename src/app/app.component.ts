import {Component, OnInit} from '@angular/core';
import {ProfileService} from './services/profile/profile.service';
import {SocketService} from './services/socket.service';
import {Notifications, NotificationsData} from './models/model';
import {AdminService} from './services/admin/admin.service';
import {AdminProductsService} from './services/admin-products.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {GoogleTagManagerService} from 'angular-google-tag-manager';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Harag';
  token = localStorage.getItem('harag-token');

  constructor(private gtmService: GoogleTagManagerService, private activeroute: ActivatedRoute,
              public profile: ProfileService, private socket: SocketService, private alert: AdminProductsService, private router: Router) {
  }

  ngOnInit() {
    if (this.token) {
      this.socket.emitSocketWhenConnect();
    }
    this.router.events.forEach(item => {
      if (item instanceof NavigationEnd) {
        const gtmTag = {
          event: 'page',
          pageName: item.url
        };
        console.log(gtmTag);

        this.gtmService.pushTag(gtmTag);
      }
    });
    this.activeroute.queryParams.subscribe(params => {
      if (params['token']) {
        localStorage.setItem('harag-token', params['token']);
        this.profile.getProfile(params['token']);
        this.router.navigate(['/']);
        // localStorage.setItem('token', queryParam['token'])
      }
    });
  }
}
