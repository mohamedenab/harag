import {Component, Input, OnInit} from '@angular/core';
import {SwiperOptions} from 'swiper';
import {HomeProducts} from '../../models/model';

@Component({
  selector: 'app-category-slider',
  templateUrl: './category-slider.component.html',
  styleUrls: ['./category-slider.component.scss']
})
export class CategorySliderComponent implements OnInit {

  config: SwiperOptions = {
    pagination: {el: '.swiper-pagination', clickable: true},
    slidesPerView: 5,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 10,
    scrollbar: {
      el: '.swiper-scrollbar',
      hide: true
    },
    breakpoints: {
      1200: {
        slidesPerView: 5,
        slidesPerGroup: 5
      },
      992: {
        slidesPerView: 4,
      },
      768: {
        slidesPerView: 3,
      },
      600: {
        slidesPerView: 2.5,
      },
      450: {
        slidesPerView: 2,
      },
      320: {
        slidesPerView: 1.5,
      }
    }
  };
  // input data
  @Input() categorySlider: HomeProducts;
  @Input() isRecent = false;

  constructor() {
  }

  ngOnInit(): void {
    if (window.innerWidth <= 992) {
      this.config.freeMode = true;

    }
  }

}
