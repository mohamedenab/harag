import {Component, Input, OnInit} from '@angular/core';
import {Test} from '../../Interfaces/Model';
import {Search} from '../../models/model';

@Component({
  selector: 'app-advertisment-card',
  templateUrl: './advertisment-card.component.html',
  styleUrls: ['./advertisment-card.component.scss']
})
export class AdvertismentCardComponent implements OnInit {

  @Input() searchData: Search;
  @Input() grid: boolean;
  MONTH_NAMES = [
    'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيه',
    'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

  getFormattedDate(date, prefomattedDate, hideYear = false) {
    const day = date.getDate();
    const month = this.MONTH_NAMES[date.getMonth()];
    const year = date.getFullYear();
    const hours = date.getHours();
    let minutes = date.getMinutes();

    if (minutes < 10) {
      // Adding leading zero to minutes
      minutes = `0${minutes}`;
    }

    if (prefomattedDate) {
      // Today at 10:20
      // Yesterday at 10:20
      return `${prefomattedDate} عند ${hours}:${minutes}`;
    }

    if (hideYear) {
      // 10. January at 10:20
      return `${day} ${month} عند ${hours}:${minutes}`;
    }

    // 10. January 2017. at 10:20
    return `${day} ${month} ${year} عند ${hours}:${minutes}`;
  }

  timeAgo(dateParam) {
    if (!dateParam) {
      return null;
    }

    const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
    const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
    const today = new Date();
    const yesterday = new Date(+today - DAY_IN_MS);
    const seconds = Math.round((+today - date) / 1000);
    const minutes = Math.round(seconds / 60);
    const isToday = today.toDateString() === date.toDateString();
    const isYesterday = yesterday.toDateString() === date.toDateString();
    const isThisYear = today.getFullYear() === date.getFullYear();


    if (seconds < 5) {
      return 'الان';
    } else if (seconds < 60) {
      return `منذ ${seconds} ثوان`;
    } else if (seconds < 90) {
      return 'منذ دقيقة تقريبا';
    } else if (minutes < 60) {
      return `منذ ${minutes} دقيقة`;
    } else if (isToday) {
      return this.getFormattedDate(date, 'اليوم'); // Today at 10:20
    } else if (isYesterday) {
      return this.getFormattedDate(date, 'امس'); // Yesterday at 10:20
    } else if (isThisYear) {
      return this.getFormattedDate(date, false, true); // 10. January at 10:20
    }

    return this.getFormattedDate(date, false, false); // 10. January 2017. at 10:20
  }
}
