import {Component, HostListener, OnInit} from '@angular/core';
import {AuthenticatedService} from '../../services/auth-gaurd/authenticated.service';
import {FormGroup, FormControl, Validators, FormGroupDirective, NgForm, FormBuilder} from '@angular/forms';
import Swal from 'sweetalert2';
import {Login, NotificationsData, Register} from '../../models/model';
import {SignUpInService} from '../../services/sign-up-in.service';
import uikit from 'Uikit';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {ProfileService} from '../../services/profile/profile.service';
import {SocketService} from '../../services/socket.service';
import {AdminProductsService} from '../../services/admin-products.service';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment.prod';

import {ErrorStateMatcher} from '@angular/material/core';

declare const $: any;

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  // variables
  sendRequest = false;
  // arraies
  activateToken = false;
  environment = environment;
  forgetPassword = false;
  countries = [
    {
      code: 'AD',
      name: 'أندورا',
      dialCode: '+376'
    },
    {
      code: 'AE',
      name: 'الامارات العربية المتحدة',
      dialCode: '+971'
    },
    {
      code: 'AF',
      name: 'أفغانستان',
      dialCode: '+93'
    },
    {
      code: 'AG',
      name: 'أنتيجوا وبربودا',
      dialCode: '+1'
    },
    {
      code: 'AI',
      name: 'أنجويلا',
      dialCode: '+1'
    },
    {
      code: 'AL',
      name: 'ألبانيا',
      dialCode: '+355'
    },
    {
      code: 'AM',
      name: 'أرمينيا',
      dialCode: '+374'
    },
    {
      code: 'AO',
      name: 'أنجولا',
      dialCode: '+244'
    },
    {
      code: 'AQ',
      name: 'القطب الجنوبي',
      dialCode: '+672'
    },
    {
      code: 'AR',
      name: 'الأرجنتين',
      dialCode: '+54'
    },
    {
      code: 'AS',
      name: 'ساموا الأمريكية',
      dialCode: '+1'
    },
    {
      code: 'AT',
      name: 'النمسا',
      dialCode: '+43'
    },
    {
      code: 'AU',
      name: 'أستراليا',
      dialCode: '+61'
    },
    {
      code: 'AW',
      name: 'آروبا',
      dialCode: '+297'
    },
    {
      code: 'AX',
      name: 'جزر أولان',
      dialCode: '+358'
    },
    {
      code: 'AZ',
      name: 'أذربيجان',
      dialCode: '+994'
    },
    {
      code: 'BA',
      name: 'البوسنة والهرسك',
      dialCode: '+387'
    },
    {
      code: 'BB',
      name: 'بربادوس',
      dialCode: '+1'
    },
    {
      code: 'BD',
      name: 'بنجلاديش',
      dialCode: '+880'
    },
    {
      code: 'BE',
      name: 'بلجيكا',
      dialCode: '+32'
    },
    {
      code: 'BF',
      name: 'بوركينا فاسو',
      dialCode: '+226'
    },
    {
      code: 'BG',
      name: 'بلغاريا',
      dialCode: '+359'
    },
    {
      code: 'BH',
      name: 'البحرين',
      dialCode: '+973'
    },
    {
      code: 'BI',
      name: 'بوروندي',
      dialCode: '+257'
    },
    {
      code: 'BJ',
      name: 'بنين',
      dialCode: '+229'
    },
    {
      code: 'BL',
      name: 'سان بارتيلمي',
      dialCode: '+590'
    },
    {
      code: 'BM',
      name: 'برمودا',
      dialCode: '+1'
    },
    {
      code: 'BN',
      name: 'بروناي',
      dialCode: '+673'
    },
    {
      code: 'BO',
      name: 'بوليفيا',
      dialCode: '+591'
    },
    {
      code: 'BQ',
      name: 'بونير',
      dialCode: '+599'
    },
    {
      code: 'BR',
      name: 'البرازيل',
      dialCode: '+55'
    },
    {
      code: 'BS',
      name: 'الباهاما',
      dialCode: '+1'
    },
    {
      code: 'BT',
      name: 'بوتان',
      dialCode: '+975'
    },
    {
      code: 'BV',
      name: 'جزيرة بوفيه',
      dialCode: '+47'
    },
    {
      code: 'BW',
      name: 'بتسوانا',
      dialCode: '+267'
    },
    {
      code: 'BY',
      name: 'روسيا البيضاء',
      dialCode: '+375'
    },
    {
      code: 'BZ',
      name: 'بليز',
      dialCode: '+501'
    },
    {
      code: 'CA',
      name: 'كندا',
      dialCode: '+1'
    },
    {
      code: 'CC',
      name: 'جزر كوكوس',
      dialCode: '+61'
    },
    {
      code: 'CD',
      name: 'جمهورية الكونغو الديمقراطية',
      dialCode: '+243'
    },
    {
      code: 'CF',
      name: 'جمهورية افريقيا الوسطى',
      dialCode: '+236'
    },
    {
      code: 'CG',
      name: 'الكونغو - برازافيل',
      dialCode: '+242'
    },
    {
      code: 'CH',
      name: 'سويسرا',
      dialCode: '+41'
    },
    {
      code: 'CI',
      name: 'ساحل العاج',
      dialCode: '+225'
    },
    {
      code: 'CK',
      name: 'جزر كوك',
      dialCode: '+682'
    },
    {
      code: 'CL',
      name: 'شيلي',
      dialCode: '+56'
    },
    {
      code: 'CM',
      name: 'الكاميرون',
      dialCode: '+237'
    },
    {
      code: 'CN',
      name: 'الصين',
      dialCode: '+86'
    },
    {
      code: 'CO',
      name: 'كولومبيا',
      dialCode: '+57'
    },
    {
      code: 'CR',
      name: 'كوستاريكا',
      dialCode: '+506'
    },
    {
      code: 'CU',
      name: 'كوبا',
      dialCode: '+53'
    },
    {
      code: 'CV',
      name: 'الرأس الأخضر',
      dialCode: '+238'
    },
    {
      code: 'CW',
      name: 'كوراساو',
      dialCode: '+599'
    },
    {
      code: 'CX',
      name: 'جزيرة الكريسماس',
      dialCode: '+61'
    },
    {
      code: 'CY',
      name: 'قبرص',
      dialCode: '+357'
    },
    {
      code: 'CZ',
      name: 'جمهورية التشيك',
      dialCode: '+420'
    },
    {
      code: 'DE',
      name: 'ألمانيا',
      dialCode: '+49'
    },
    {
      code: 'DJ',
      name: 'جيبوتي',
      dialCode: '+253'
    },
    {
      code: 'DK',
      name: 'الدانمرك',
      dialCode: '+45'
    },
    {
      code: 'DM',
      name: 'دومينيكا',
      dialCode: '+1'
    },
    {
      code: 'DO',
      name: 'جمهورية الدومينيك',
      dialCode: '+1'
    },
    {
      code: 'DZ',
      name: 'الجزائر',
      dialCode: '+213'
    },
    {
      code: 'EC',
      name: 'الاكوادور',
      dialCode: '+593'
    },
    {
      code: 'EE',
      name: 'استونيا',
      dialCode: '+372'
    },
    {
      code: 'EG',
      name: 'مصر',
      dialCode: '+20'
    },
    {
      code: 'EH',
      name: 'الصحراء الغربية',
      dialCode: '+212'
    },
    {
      code: 'ER',
      name: 'اريتريا',
      dialCode: '+291'
    },
    {
      code: 'ES',
      name: 'أسبانيا',
      dialCode: '+34'
    },
    {
      code: 'ET',
      name: 'اثيوبيا',
      dialCode: '+251'
    },
    {
      code: 'FI',
      name: 'فنلندا',
      dialCode: '+358'
    },
    {
      code: 'FJ',
      name: 'فيجي',
      dialCode: '+679'
    },
    {
      code: 'FK',
      name: 'جزر فوكلاند',
      dialCode: '+500'
    },
    {
      code: 'FM',
      name: 'ميكرونيزيا',
      dialCode: '+691'
    },
    {
      code: 'FO',
      name: 'جزر فارو',
      dialCode: '+298'
    },
    {
      code: 'FR',
      name: 'فرنسا',
      dialCode: '+33'
    },
    {
      code: 'GA',
      name: 'الجابون',
      dialCode: '+241'
    },
    {
      code: 'GB',
      name: 'المملكة المتحدة',
      dialCode: '+44'
    },
    {
      code: 'GD',
      name: 'جرينادا',
      dialCode: '+1'
    },
    {
      code: 'GE',
      name: 'جورجيا',
      dialCode: '+995'
    },
    {
      code: 'GF',
      name: 'غويانا',
      dialCode: '+594'
    },
    {
      code: 'GG',
      name: 'غيرنزي',
      dialCode: '+44'
    },
    {
      code: 'GH',
      name: 'غانا',
      dialCode: '+233'
    },
    {
      code: 'GI',
      name: 'جبل طارق',
      dialCode: '+350'
    },
    {
      code: 'GL',
      name: 'جرينلاند',
      dialCode: '+299'
    },
    {
      code: 'GM',
      name: 'غامبيا',
      dialCode: '+220'
    },
    {
      code: 'GN',
      name: 'غينيا',
      dialCode: '+224'
    },
    {
      code: 'GP',
      name: 'جوادلوب',
      dialCode: '+590'
    },
    {
      code: 'GQ',
      name: 'غينيا الاستوائية',
      dialCode: '+240'
    },
    {
      code: 'GR',
      name: 'اليونان',
      dialCode: '+30'
    },
    {
      code: 'GS',
      name: 'جورجيا الجنوبية وجزر ساندويتش الجنوبية',
      dialCode: '+500'
    },
    {
      code: 'GT',
      name: 'جواتيمالا',
      dialCode: '+502'
    },
    {
      code: 'GU',
      name: 'جوام',
      dialCode: '+1'
    },
    {
      code: 'GW',
      name: 'غينيا بيساو',
      dialCode: '+245'
    },
    {
      code: 'GY',
      name: 'غيانا',
      dialCode: '+595'
    },
    {
      code: 'HK',
      name: 'هونج كونج الصينية',
      dialCode: '+852'
    },
    {
      code: 'HM',
      name: 'جزيرة هيرد وماكدونالد',
      dialCode: ''
    },
    {
      code: 'HN',
      name: 'هندوراس',
      dialCode: '+504'
    },
    {
      code: 'HR',
      name: 'كرواتيا',
      dialCode: '+385'
    },
    {
      code: 'HT',
      name: 'هايتي',
      dialCode: '+509'
    },
    {
      code: 'HU',
      name: 'المجر',
      dialCode: '+36'
    },
    {
      code: 'ID',
      name: 'اندونيسيا',
      dialCode: '+62'
    },
    {
      code: 'IE',
      name: 'أيرلندا',
      dialCode: '+353'
    },
    {
      code: 'IL',
      name: 'اسرائيل',
      dialCode: '+972'
    },
    {
      code: 'IM',
      name: 'جزيرة مان',
      dialCode: '+44'
    },
    {
      code: 'IN',
      name: 'الهند',
      dialCode: '+91'
    },
    {
      code: 'IO',
      name: 'المحيط الهندي البريطاني',
      dialCode: '+246'
    },
    {
      code: 'IQ',
      name: 'العراق',
      dialCode: '+964'
    },
    {
      code: 'IR',
      name: 'ايران',
      dialCode: '+98'
    },
    {
      code: 'IS',
      name: 'أيسلندا',
      dialCode: '+354'
    },
    {
      code: 'IT',
      name: 'ايطاليا',
      dialCode: '+39'
    },
    {
      code: 'JE',
      name: 'جيرسي',
      dialCode: '+44'
    },
    {
      code: 'JM',
      name: 'جامايكا',
      dialCode: '+1'
    },
    {
      code: 'JO',
      name: 'الأردن',
      dialCode: '+962'
    },
    {
      code: 'JP',
      name: 'اليابان',
      dialCode: '+81'
    },
    {
      code: 'KE',
      name: 'كينيا',
      dialCode: '+254'
    },
    {
      code: 'KG',
      name: 'قرغيزستان',
      dialCode: '+996'
    },
    {
      code: 'KH',
      name: 'كمبوديا',
      dialCode: '+855'
    },
    {
      code: 'KI',
      name: 'كيريباتي',
      dialCode: '+686'
    },
    {
      code: 'KM',
      name: 'جزر القمر',
      dialCode: '+269'
    },
    {
      code: 'KN',
      name: 'سانت كيتس ونيفيس',
      dialCode: '+1'
    },
    {
      code: 'KP',
      name: 'كوريا الشمالية',
      dialCode: '+850'
    },
    {
      code: 'KR',
      name: 'كوريا الجنوبية',
      dialCode: '+82'
    },
    {
      code: 'KW',
      name: 'الكويت',
      dialCode: '+965'
    },
    {
      code: 'KY',
      name: 'جزر الكايمن',
      dialCode: '+345'
    },
    {
      code: 'KZ',
      name: 'كازاخستان',
      dialCode: '+7'
    },
    {
      code: 'LA',
      name: 'لاوس',
      dialCode: '+856'
    },
    {
      code: 'LB',
      name: 'لبنان',
      dialCode: '+961'
    },
    {
      code: 'LC',
      name: 'سانت لوسيا',
      dialCode: '+1'
    },
    {
      code: 'LI',
      name: 'ليختنشتاين',
      dialCode: '+423'
    },
    {
      code: 'LK',
      name: 'سريلانكا',
      dialCode: '+94'
    },
    {
      code: 'LR',
      name: 'ليبيريا',
      dialCode: '+231'
    },
    {
      code: 'LS',
      name: 'ليسوتو',
      dialCode: '+266'
    },
    {
      code: 'LT',
      name: 'ليتوانيا',
      dialCode: '+370'
    },
    {
      code: 'LU',
      name: 'لوكسمبورج',
      dialCode: '+352'
    },
    {
      code: 'LV',
      name: 'لاتفيا',
      dialCode: '+371'
    },
    {
      code: 'LY',
      name: 'ليبيا',
      dialCode: '+218'
    },
    {
      code: 'MA',
      name: 'المغرب',
      dialCode: '+212'
    },
    {
      code: 'MC',
      name: 'موناكو',
      dialCode: '+377'
    },
    {
      code: 'MD',
      name: 'مولدافيا',
      dialCode: '+373'
    },
    {
      code: 'ME',
      name: 'الجبل الأسود',
      dialCode: '+382'
    },
    {
      code: 'MF',
      name: 'سانت مارتين',
      dialCode: '+590'
    },
    {
      code: 'MG',
      name: 'مدغشقر',
      dialCode: '+261'
    },
    {
      code: 'MH',
      name: 'جزر المارشال',
      dialCode: '+692'
    },
    {
      code: 'MK',
      name: 'مقدونيا',
      dialCode: '+389'
    },
    {
      code: 'ML',
      name: 'مالي',
      dialCode: '+223'
    },
    {
      code: 'MM',
      name: 'ميانمار',
      dialCode: '+95'
    },
    {
      code: 'MN',
      name: 'منغوليا',
      dialCode: '+976'
    },
    {
      code: 'MO',
      name: 'ماكاو الصينية',
      dialCode: '+853'
    },
    {
      code: 'MP',
      name: 'جزر ماريانا الشمالية',
      dialCode: '+1'
    },
    {
      code: 'MQ',
      name: 'مارتينيك',
      dialCode: '+596'
    },
    {
      code: 'MR',
      name: 'موريتانيا',
      dialCode: '+222'
    },
    {
      code: 'MS',
      name: 'مونتسرات',
      dialCode: '+1'
    },
    {
      code: 'MT',
      name: 'مالطا',
      dialCode: '+356'
    },
    {
      code: 'MU',
      name: 'موريشيوس',
      dialCode: '+230'
    },
    {
      code: 'MV',
      name: 'جزر الملديف',
      dialCode: '+960'
    },
    {
      code: 'MW',
      name: 'ملاوي',
      dialCode: '+265'
    },
    {
      code: 'MX',
      name: 'المكسيك',
      dialCode: '+52'
    },
    {
      code: 'MY',
      name: 'ماليزيا',
      dialCode: '+60'
    },
    {
      code: 'MZ',
      name: 'موزمبيق',
      dialCode: '+258'
    },
    {
      code: 'NA',
      name: 'ناميبيا',
      dialCode: '+264'
    },
    {
      code: 'NC',
      name: 'كاليدونيا الجديدة',
      dialCode: '+687'
    },
    {
      code: 'NE',
      name: 'النيجر',
      dialCode: '+227'
    },
    {
      code: 'NF',
      name: 'جزيرة نورفوك',
      dialCode: '+672'
    },
    {
      code: 'NG',
      name: 'نيجيريا',
      dialCode: '+234'
    },
    {
      code: 'NI',
      name: 'نيكاراجوا',
      dialCode: '+505'
    },
    {
      code: 'NL',
      name: 'هولندا',
      dialCode: '+31'
    },
    {
      code: 'NO',
      name: 'النرويج',
      dialCode: '+47'
    },
    {
      code: 'NP',
      name: 'نيبال',
      dialCode: '+977'
    },
    {
      code: 'NR',
      name: 'نورو',
      dialCode: '+674'
    },
    {
      code: 'NU',
      name: 'نيوي',
      dialCode: '+683'
    },
    {
      code: 'NZ',
      name: 'نيوزيلاندا',
      dialCode: '+64'
    },
    {
      code: 'OM',
      name: 'عمان',
      dialCode: '+968'
    },
    {
      code: 'PA',
      name: 'بنما',
      dialCode: '+507'
    },
    {
      code: 'PE',
      name: 'بيرو',
      dialCode: '+51'
    },
    {
      code: 'PF',
      name: 'بولينيزيا الفرنسية',
      dialCode: '+689'
    },
    {
      code: 'PG',
      name: 'بابوا غينيا الجديدة',
      dialCode: '+675'
    },
    {
      code: 'PH',
      name: 'الفيلبين',
      dialCode: '+63'
    },
    {
      code: 'PK',
      name: 'باكستان',
      dialCode: '+92'
    },
    {
      code: 'PL',
      name: 'بولندا',
      dialCode: '+48'
    },
    {
      code: 'PM',
      name: 'سانت بيير وميكولون',
      dialCode: '+508'
    },
    {
      code: 'PN',
      name: 'بتكايرن',
      dialCode: '+872'
    },
    {
      code: 'PR',
      name: 'بورتوريكو',
      dialCode: '+1'
    },
    {
      code: 'PS',
      name: 'فلسطين',
      dialCode: '+970'
    },
    {
      code: 'PT',
      name: 'البرتغال',
      dialCode: '+351'
    },
    {
      code: 'PW',
      name: 'بالاو',
      dialCode: '+680'
    },
    {
      code: 'PY',
      name: 'باراجواي',
      dialCode: '+595'
    },
    {
      code: 'QA',
      name: 'قطر',
      dialCode: '+974'
    },
    {
      code: 'RE',
      name: 'روينيون',
      dialCode: '+262'
    },
    {
      code: 'RO',
      name: 'رومانيا',
      dialCode: '+40'
    },
    {
      code: 'RS',
      name: 'صربيا',
      dialCode: '+381'
    },
    {
      code: 'RU',
      name: 'روسيا',
      dialCode: '+7'
    },
    {
      code: 'RW',
      name: 'رواندا',
      dialCode: '+250'
    },
    {
      code: 'SA',
      name: 'المملكة العربية السعودية',
      dialCode: '+966'
    },
    {
      code: 'SB',
      name: 'جزر سليمان',
      dialCode: '+677'
    },
    {
      code: 'SC',
      name: 'سيشل',
      dialCode: '+248'
    },
    {
      code: 'SD',
      name: 'السودان',
      dialCode: '+249'
    },
    {
      code: 'SE',
      name: 'السويد',
      dialCode: '+46'
    },
    {
      code: 'SG',
      name: 'سنغافورة',
      dialCode: '+65'
    },
    {
      code: 'SH',
      name: 'سانت هيلنا',
      dialCode: '+290'
    },
    {
      code: 'SI',
      name: 'سلوفينيا',
      dialCode: '+386'
    },
    {
      code: 'SJ',
      name: 'سفالبارد وجان مايان',
      dialCode: '+47'
    },
    {
      code: 'SK',
      name: 'سلوفاكيا',
      dialCode: '+421'
    },
    {
      code: 'SL',
      name: 'سيراليون',
      dialCode: '+232'
    },
    {
      code: 'SM',
      name: 'سان مارينو',
      dialCode: '+378'
    },
    {
      code: 'SN',
      name: 'السنغال',
      dialCode: '+221'
    },
    {
      code: 'SO',
      name: 'الصومال',
      dialCode: '+252'
    },
    {
      code: 'SR',
      name: 'سورينام',
      dialCode: '+597'
    },
    {
      code: 'SS',
      name: 'جنوب السودان',
      dialCode: '+211'
    },
    {
      code: 'ST',
      name: 'ساو تومي وبرينسيبي',
      dialCode: '+239'
    },
    {
      code: 'SV',
      name: 'السلفادور',
      dialCode: '+503'
    },
    {
      code: 'SX',
      name: 'سينت مارتن',
      dialCode: '+1'
    },
    {
      code: 'SY',
      name: 'سوريا',
      dialCode: '+963'
    },
    {
      code: 'SZ',
      name: 'سوازيلاند',
      dialCode: '+268'
    },
    {
      code: 'TC',
      name: 'جزر الترك وجايكوس',
      dialCode: '+1'
    },
    {
      code: 'TD',
      name: 'تشاد',
      dialCode: '+235'
    },
    {
      code: 'TF',
      name: 'المقاطعات الجنوبية الفرنسية',
      dialCode: '+262'
    },
    {
      code: 'TG',
      name: 'توجو',
      dialCode: '+228'
    },
    {
      code: 'TH',
      name: 'تايلند',
      dialCode: '+66'
    },
    {
      code: 'TJ',
      name: 'طاجكستان',
      dialCode: '+992'
    },
    {
      code: 'TK',
      name: 'توكيلو',
      dialCode: '+690'
    },
    {
      code: 'TL',
      name: 'تيمور الشرقية',
      dialCode: '+670'
    },
    {
      code: 'TM',
      name: 'تركمانستان',
      dialCode: '+993'
    },
    {
      code: 'TN',
      name: 'تونس',
      dialCode: '+216'
    },
    {
      code: 'TO',
      name: 'تونجا',
      dialCode: '+676'
    },
    {
      code: 'TR',
      name: 'تركيا',
      dialCode: '+90'
    },
    {
      code: 'TT',
      name: 'ترينيداد وتوباغو',
      dialCode: '+1'
    },
    {
      code: 'TV',
      name: 'توفالو',
      dialCode: '+688'
    },
    {
      code: 'TW',
      name: 'تايوان',
      dialCode: '+886'
    },
    {
      code: 'TZ',
      name: 'تانزانيا',
      dialCode: '+255'
    },
    {
      code: 'UA',
      name: 'أوكرانيا',
      dialCode: '+380'
    },
    {
      code: 'UG',
      name: 'أوغندا',
      dialCode: '+256'
    },
    {
      code: 'UM',
      name: 'جزر الولايات المتحدة البعيدة الصغيرة',
      dialCode: ''
    },
    {
      code: 'US',
      name: 'الولايات المتحدة الأمريكية',
      dialCode: '+1'
    },
    {
      code: 'UY',
      name: 'أورجواي',
      dialCode: '+598'
    },
    {
      code: 'UZ',
      name: 'أوزبكستان',
      dialCode: '+998'
    },
    {
      code: 'VA',
      name: 'الفاتيكان',
      dialCode: '+379'
    },
    {
      code: 'VC',
      name: 'سانت فنسنت وغرنادين',
      dialCode: '+1'
    },
    {
      code: 'VE',
      name: 'فنزويلا',
      dialCode: '+58'
    },
    {
      code: 'VG',
      name: 'جزر فرجين البريطانية',
      dialCode: '+1'
    },
    {
      code: 'VI',
      name: 'جزر فرجين الأمريكية',
      dialCode: '+1'
    },
    {
      code: 'VN',
      name: 'فيتنام',
      dialCode: '+84'
    },
    {
      code: 'VU',
      name: 'فانواتو',
      dialCode: '+678'
    },
    {
      code: 'WF',
      name: 'جزر والس وفوتونا',
      dialCode: '+681'
    },
    {
      code: 'WS',
      name: 'ساموا',
      dialCode: '+685'
    },
    {
      code: 'XK',
      name: 'كوسوفو',
      dialCode: '+383'
    },
    {
      code: 'YE',
      name: 'اليمن',
      dialCode: '+967'
    },
    {
      code: 'YT',
      name: 'مايوت',
      dialCode: '+262'
    },
    {
      code: 'ZA',
      name: 'جمهورية جنوب افريقيا',
      dialCode: '+27'
    },
    {
      code: 'ZM',
      name: 'زامبيا',
      dialCode: '+260'
    },
    {
      code: 'ZW',
      name: 'زيمبابوي',
      dialCode: '+263'
    }
  ];
  open = false;
  searchText = '';
  matcher = new MyErrorStateMatcher();

  // interfaces
  loginData: Login = {
    phone: '',
    password: '',
  };
  registerData: Register = {
    firstName: '',
    lastName: '',
    password: '',
    phone: '',
  };
  // sweet alert
  Toast = Swal.mixin({
    toast: true,
    position: 'top-left',
    showConfirmButton: false,
    timer: 2500,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });
  token = '';
  newMessages = false;
  newNotification = false;
  // reactive forms part
  loginForm = new FormGroup({
    userName: new FormControl('', [Validators.required]),
    userPassword: new FormControl('', [Validators.required, Validators.minLength(4)])
  });
  forgetForm = this.fb.group({
    phone: new FormControl('', [Validators.required]),
    code: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(4)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(4)])
  }, {validator: this.checkPasswords});

  notificationContainer: NotificationsData = {
    read: null,
    _id: '',
    createdAt: '',
    from: {
      _id: '',
      fullName: '',
      profilePicture: {
        path: ''
      }
    },
    link: '',
    message: ''
  };

  registerForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(4)])
  });

  constructor(public auth: AuthenticatedService,
              private login: SignUpInService,
              private socket: SocketService,
              public router: Router, private fb: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private alert: AdminProductsService,
              public profile: ProfileService) {
  }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      this.socket.listenToChatNotifications().subscribe((res) => {
        if (res.success) {
          this.newMessages = true;
          this.alert.alertInfo('لديك رسالة جديدة');
          return;
        }
        this.newMessages = false;
      });
      this.checkChat();
      this.checkNotification();
      this.readNotification();


      this.socket.listenToAllNotifications().subscribe(res => {
        this.notificationContainer = res.notification;
        console.log(res);
        this.newNotification = true;
        this.socket.emitToNotification();
        this.alert.alertInfo(this.notificationContainer?.from?.fullName.concat(' ', this.notificationContainer?.message));
      });
    }
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {notSame: true};
  }


  checkChat() {
    this.socket.checkChat().subscribe(res => {
      if (res.data > 0) {
        this.newMessages = true;
        return;
      }
      this.newMessages = false;
    });
  }

  readNotification() {
    this.socket.readNotification().subscribe(res => {
      this.newNotification = !res.success;
      console.log(this.newNotification, 'notification');
    });
  }

  checkNotification() {
    this.socket.checkNotification().subscribe(res => {
      if (res.data > 0) {
        this.newNotification = true;
        return;
      }
      this.newNotification = false;
    });
  }

  // login user
  loginThisUser() {
    this.sendRequest = true;
    this.loginData.phone = this.loginForm.controls.userName.value;
    this.loginData.password = this.loginForm.controls.userPassword.value;
    console.log(this.loginData);

    this.login.loginUser(this.loginData).subscribe(res => {
      this.auth.saveToken(res.data.refreshToken);
      this.loginForm.reset();
      this.sendRequest = false;
      this.closeModal();
      window.location.reload();
      console.log(res);
      localStorage.setItem('h-u-id', res.data.user._id);
      localStorage.setItem('h-u-n', res.data.user.fullName);
    }, err => {
      this.sendRequest = false;
      this.alertDanger(err.error.message);
    });

  }

  // register user
  registerAccount() {
    this.sendRequest = true;
    this.registerData.firstName = this.registerForm.controls.firstName.value;
    this.registerData.lastName = this.registerForm.controls.lastName.value;
    this.registerData.phone = this.registerForm.controls.phone.value;
    this.registerData.password = this.registerForm.controls.password.value;
    this.login.registerUser(this.registerData).subscribe(res => {
      this.sendRequest = false;
      this.registerForm.reset();
      this.activateToken = true;
    }, err => {
      this.sendRequest = false;
      this.alertDanger(err.message);
    });
    console.log(this.registerData);
  }

  activateUser(verify?) {
    this.login.activate(this.token).subscribe((res: any) => {
      if (res.success) {
        if (!verify) {
          this.loginData.phone = this.registerData.phone;
          this.loginData.password = this.registerData.password;

          this.alertSuccess('تم إنشاء حساب جديد بنجاح, اهلا بك!');

          this.login.loginUser(this.loginData).subscribe(_res => {
            this.auth.saveToken(_res.data.refreshToken);
            this.loginForm.reset();
            this.sendRequest = false;
            window.location.reload();
            uikit.modal('#register-modal').hide();
            localStorage.setItem('h-u-id', _res.data.user._id);
            localStorage.setItem('h-u-n', _res.data.user.fullName);
          }, err => {
            this.sendRequest = false;
            this.alertDanger(err.error.message);
          });
        } else {
          this.alertSuccess('تم تأكيد الحساب بنجاح');
          this.router.navigate(['/admin/new-advertisement']);
          uikit.modal('#verify-modal').hide();
        }
      } else {
        this.alertDanger(res.message);

      }

    }, error => {
      this.alertDanger(error.message);

    });
  }

  toggleSidebar() {
    if (this.open) {
      // close
      $('.sidebar').css({
        transform: 'translateX(270px)'
      });
      setTimeout(() => {
        $('.sidebar-wrapper').fadeOut(200);
        $('.sidebar').fadeOut(50);
      }, 100);
      this.open = !this.open;
    } else {
      // open
      $('.sidebar').fadeIn(50);
      $('.sidebar-wrapper').fadeIn(200);
      setTimeout(() => {
        $('.sidebar').css({
          transform: 'translateX(0)'
        });
      }, 100);
      this.open = !this.open;
    }
  }

  searchFocus(input: HTMLInputElement) {
    setTimeout(() => {
      input.focus();
    }, 50);
  }


  // search start
  searchInDb() {
    this.router.navigate(['/search'], {queryParams: {searchText: this.searchText}});
  }

  // search end

  alertSuccess(message: string) {
    this.Toast.fire({
      icon: 'success',
      title: message
    });
  }

  alertDanger(message: string) {
    this.Toast.fire({
      icon: 'error',
      title: message
    });
  }

  closeModal() {
    uikit.modal('#login-modal').hide();
  }

  openRegModal() {
    this.closeModal();
    uikit.modal('#register-modal').show();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    console.log(this.router.url);
    if (this.router.url === '/cars') {
      const element = document.querySelector('.navbar');
      if (window.pageYOffset > element.clientHeight) {
        element.classList.add('active');
      } else {
        element.classList.remove('active');
      }
    }
  }

  forgetPasswordcode() {
    this.login.resetpassword(this.forgetForm.value.phone).subscribe((res) => {
      this.forgetPassword = true;

    }, error => {
      this.alertDanger(error.error.message);

    });
  }

  sendCode() {
    this.login.sendCode().subscribe((res) => {
    });
  }

  resetPassword() {
    this.login.confirmresetpassword(this.forgetForm.value.password, this.forgetForm.value.code).subscribe((res) => {
      uikit.modal('#forget-modal').hide();
      this.alertSuccess('تم إعادة تعين كلمة السر بنجاح, اهلا بك!');

    });
  }
}
