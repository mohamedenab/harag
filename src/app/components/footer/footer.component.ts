import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../services/blog/article.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  blog;

  constructor(private article: ArticleService) {
  }

  ngOnInit(): void {
    this.article.getHomeblogs().subscribe(res => {
      // this.blogContainer = res.data;
      this.blog = res.data.latestBlogs[0];
    });
  }

}
