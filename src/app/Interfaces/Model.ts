export interface Test {
  name?: string;
}

export interface Advertisement {
  category?: string;
  title?: string;
  cover?: string;
  text?: string;
  city?: string;
  typeOfAd?: string;
  price?: number;
  carBody?: string;
  contactNumber?: string;
  warrantyStatus?: string;
  installment?: boolean;
  carPlace?: string;
  driver?: boolean;
  forSell?: boolean;
  runningKilos?: number;
  images?: any;
  brand?: string;
  model?: string;
  style?: string;
  manufacturingYear?: string;
  typeOfSpecifications?: string;
  agent?: string;
  movementController?: string;
  speedCount?: string;
  color?: string;
  innerColor?: string;
  cylinderCount?: number;
  fuelType?: string;
  brands?: string[];
  models?: string[];
  styles?: string[];
  manufacturingYears?: string[];
}

export interface GivenAdvertisement {
  category?: {
    _id: string,
    name: string,
    parent: string
  };
  title?: string;
  text?: string;
  city?: string;
  typeOfAd?: string;
  price?: number;
  contactNumber?: string;
  warrantyStatus?: string;
  installment?: boolean;
  carPlace?: string;
  driver?: boolean;
  forSell?: boolean;
  runningKilos?: number;
  images?: {
    path: '',
    name: ''
  }[];
  brand?: Brand;
  model?: Brand;
  style?: Brand;
  manufacturingYear?: string;
  typeOfSpecifications?: string;
  agent?: string;
  movementController?: string;
  speedCount?: string;
  color?: string;
  innerColor?: string;
  cylinderCount?: number;
  fuelType?: string;
  brands?: Brand[];
  models?: Brand[];
  styles?: Brand[];
  manufacturingYears?: string[];
}

export interface SubCategory {
  name?: string;
  _id?: string;
  inputs?: Inputs[];
  ancestors?: Brand[];
}

export interface UploadedImage {
  _id?: string;
  path?: string;
  name?: string;
}

export interface SubCategoryResponse {
  data?: SubCategory[];
}

export interface Inputs {
  options?: {
    name?: string;
    value?: string
  }[];
  field?: string;
  name?: string;
}

export interface Brand {
  _id: string;
  name?: string;
  selected?: boolean;
}

export interface User {
  _id?: string;
  subscribed?: boolean;
  email?: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
  gender?: string;
  birthDate: string;
  image?: string;
  suspended?: boolean;
  productsCount?: number;
  role?: string;
}
